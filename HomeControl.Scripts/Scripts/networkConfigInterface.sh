
#!/bin/bash
con="${1}"

if [[ -z "$con" ]]; then
  echo "Please provide an interface name as an argument."
  exit 1
fi

while true; do
  conns=$(nmcli con show "$con" | grep "GENERAL.STATE:" | grep "activated")
  if [[ -z "$conns" ]]; then
    echo "$con is not active; scanning...."
    nmcli device wifi rescan
    # nmcli device wifi list
    nmcli -f SSID dev wifi
    nmcli device wifi connect SSID-Name # password wireless-password
  fi
  sleep 5
done