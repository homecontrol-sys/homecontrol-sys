!/bin/bash

# set static LAN IP in  /etc/network/interfaces
echo -e 'auto eth0\nallow-hotplug eth0\niface eth0 inet static\n   address 10.0.0.1\n   netmask 255.255.255.0\n' >> /etc/network/interfaces

#set wlan config in /etc/network/interfaces
echo -e 'auto wlan0\nallow-hotplug wlan0\niface wlan0 inet manual\n   wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf\n' >> /etc/network/interfaces

#write wlan config in etc/wpa_supplicant/wpa_supplicant.conf
echo -e 'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\ncountry=DE\n\nnetwork={\n   ssid="INTERN"\n   scan_ssid=1\n   psk="password"\n   key_mgmt=WPA-PSK\n}\n' >> /etc/wpa_supplicant/wpa_supplicant.conf


# restart network manager 
service network-manager restart
sleep 15
#network manager WIFI connect
nmcli device wifi connect "INTERN" password "password" hidden on