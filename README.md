# homecontrol-sys-api (.NET Core 5.0 REST API)

## ENVIRONMENT

### PRODUCTION

**Raspbery Pi**

- Install Raspberry Pi Imager (https://www.raspberrypi.com/software/)
- Install Raspbian OS on Raspberry SD card
- Create homecontrol user
- Connect to network
- Install Docker
  - `sudo apt-get update`
  - `sudo apt-get upgrade`
  - `curl -sSL https://get.docker.com | sh`
  - Change mode `sudo chmod 666 /var/run/docker.sock`
  - Add homecontrol user to docker group `sudo usermod -aG docker homecontrol`
- Install docker compose  
`sudo pip3 install docker-compose`
- enable SSH  
  `sudo systemctl enable ssh`
- start SSH  
  `sudo systemctl start ssh`
- Install winSCP, connect to raspberry via ssh and copy the project files, create
- pull latest version from staging branch

### DEVELOPMENT

**HTTPS SSL Certificates**:

src: https://gist.github.com/dahlsailrunner/679e6dec5fd769f30bce90447ae80081

**To suport custom DNS names**:

- For Windows edit host file entry: C:\Windows\System32\drivers\etc\hosts  
  Add Line

      127.0.0.1       api-local.homecontrol.com
      127.0.0.1       www-local.homecontrol.com

APP is running in Docker Container Environment(Software Virtualization Layer):
within Linux containers,  
REST API depends on listed containers:

Run app environment:

- Rebuild and Start docker image with changed source  
  `docker-compose -f .\docker-compose.debug.yml up -d --build`
- Start Debug mode/ development  
  `docker-compose -f .\docker-compose.debug.yml up -d`

Localhost/Docker environment:

- **database** - container with running PostgreSQL database

  - _localhost url_: http://api-local.homecontrol.com:5433 | http://localhost:5433
  - _localhost port_: 5433
  - _docker_url:_ http://database:5432
  - _docker_port:_ 5432
  - _postgres user :_ admin
  - _postgres password:_ admin
  - _API database:_ homecontrol-db

  Drop all database active sessions

  > SELECT pg_terminate_backend(pg_stat_activity.pid)
  > FROM pg_stat_activity
  > WHERE pg_stat_activity.datname = 'homecontrol_db'
  > AND pid <> pg_backend_pid();
  > AND pid <> pg_backend_pid();

- **database-admin** - container which provides PgAdmin tool relational database IDE for database administration

  - _localhost url_: http://api-local.homecontrol.com:5050 | http://localhost:5050
  - _localhost port_: 5050
  - _docker_url:_ http://database-admin:80
  - _docker_port:_ 80
  - _Email Address / Username:_ admin@homecontrol-sys.com
  - _Password:_ admin

- **logging** - seq logging server, container for development environment purpose for error logging

  - _localhost url_: http://api-local.homecontrol.com:5341 | http://localhost:5341
  - _localhost port_: 5050
  - _docker_url:_ http://logging:80
  - _docker_port:_ 80
  - _Email Address / Username:_ admin@homecontrol-sys.com
  - _Password:_ admin

- **api** - homecontrol api, container with REST API endpoints

  - _localhost url_: http://api-local.homecontrol.com:5001 | http://localhost:5001
  - _localhost https url_: https://api-local.homecontrol.com:44395/swagger/index.html
  - _localhost port_: 5001
  - _docker_url:_ http://api:5000
  - _docker port:_ 5000

- **web** - homecontrol web, container with UI Web aplication
  - _localhost url_: http://www-local.homecontrol.com:3001 | http://localhost:3001
  - _localhost https url_: https://www-local.homecontrol.com:44395
  - _localhost port_: 3001
  - _docker_url:_ http://web:3000
  - _docker port:_ 3000

### TESTING

**Swagger API test, localhost uri:**

- http://localhost:5001/swagger/index.html
- http://api-local.homecontrol.com:5001/swagger/index.html

**Seq logs, localhost uri**: http://localhost:5341/#/events

### MIGRATIONS

_Migrations are handled automatically by running docker container_

- EF CLI Migrations  
   `dotnet tool update --global dotnet-ef`

- Create migration  
  `dotnet ef migrations add InitialMigration -s .\HomeControl.API\ --project .\HomeControl.DAL\`

- Update database  
  `dotnet ef database update --project .\HomeControl.API\`

### ERRORS

- Development api run Error:  
   This localhost page can’t be foundNo webpage was found for the web address: https://localhost:5001/
  HTTP ERROR 404  
  `dotnet dev-certs https --trust`

## HANDLE API.Project.Common NuGet

- Apply package changes and pack new version

  `cd API.Project.Common`

  `nuget pack`

- List nuget remote repositories

  `nuget sources`

- Add NuGet package remote

  `nuget source Add -Name gitlab -Source "https://gitlab.com/api/v4/projects/31434276/packages/nuget/index.json" -UserName andjelo -Password glpat-QW7hLK-bW8VBapVEJWL-`

- Enable gitlab remote repository

  ` dotnet nuget enable source gitlab`

- Specify project where NuGet package is

  `nuget spec .\API.Project.Common.csproj`

- Push NuGet package to remote
  `nuget push .\API.Project.Common.1.0.0.nupkg -source gitlab`

- Clean

  `nuget disable source gitlab`

  `nuget locals all -clear`

  `nuget locals -clear`

## REST API ENDPOINTS

### **Bussines Logic**

### Security:

- Identity (IdentityController)  
  `/identity`
  - Login  
    `/identity/login`  
    **Methods:**  
    POST `/identity/login`
    - PARAMS: **_usernameOrEmail (string)_**, **_password (string)_**
  - Logout  
    `/identity/logout`
  - Register  
    `/identity/register`

### **Resources:**

- **System** (SystemController)
  `/system`  
   **Methods:**  
   POST `/system`  
   _Crates new System resource_
  - PARAMS: **_categoryId (string)_**, **_name (string)_**, **_description(string | null)_**
- **SystemCategory** (SystemCategoryController)
  `/system-category`  
   **Methods:**

  - GET `/system-category`  
    _Gets all awailable SystemCategory resources_

    - PARAMS: none
    - RESPONSE:  
      _array of SystemCategory objects_
      ```js
          [
            {
              "id": (guid),
              "abrv": (string),
              "name": (string),
              "description": (string)
            }, ...
          ]
      ```

  - GET `/system-category`  
    _Gets all awailable SystemCategory resources_

    - PARAMS: **_id (guid)_**
    - RESPONSE:  
      _SystemCategory object_
      ```js
      [
        {
          id: guid,
          abrv: string,
          name: string,
          description: string,
        },
      ];
      ```

- Device
- Element

## DATA

## DATA SEEDS

- USER
- SYSTEM
