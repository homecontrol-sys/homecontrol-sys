using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.Domain
{

    public class SwitchDomain : LookupBaseModel, ISwitchDomain
    {

        #region Properties
    
        public string Alias { get; set; }
        public int Dilay { get; set; }
        
        public ICollection<ElementDomain> Elements { get; set; }
        public ICollection<SwitchStateDomain> SwitchStates { get; set; }

        #endregion

    }
    
}
