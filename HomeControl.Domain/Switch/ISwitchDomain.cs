using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.Domain
{

    public interface ISwitchDomain : ILookupBaseModel
    {

        #region Properties
    
        string Alias { get; set; }
        int Dilay { get; set; }
        
        ICollection<ElementDomain> Elements { get; set; }
        ICollection<SwitchStateDomain> SwitchStates { get; set; }

        #endregion

    }
    
}
