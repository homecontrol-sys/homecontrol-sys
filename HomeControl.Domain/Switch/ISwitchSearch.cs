namespace HomeControl.Domain
{

    public interface ISwitchSearch
    {
        
        #region Properties

        string Name { get; set; }

        #endregion
    
    }
    
}