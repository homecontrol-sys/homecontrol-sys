using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public class PinConnectionDomain : IPinConnectionDomain
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsTested { get; set; }
        public bool IsValid { get; set; }

        public Guid StartPinId { get; set; }
        public Guid EndPinId { get; set; }
        public ICollection<PinDomain> Pins { get; set; }

        #endregion

    }

}
