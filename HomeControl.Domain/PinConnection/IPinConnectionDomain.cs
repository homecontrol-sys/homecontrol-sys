using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface IPinConnectionDomain
    {
        
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        bool IsTested { get; set; }
        bool IsValid { get; set; }

        Guid StartPinId { get; set; }
        Guid EndPinId { get; set; }
        ICollection<PinDomain> Pins { get; set; }

        #endregion
    
    }
    
}
