namespace HomeControl.Domain
{

    public interface IPinConnectionSearch
    {
        
        #region Properties

        string Name { get; set; }

        #endregion
    
    }
    
}
