using System;

namespace HomeControl.Domain
{

    public class SystemDeviceDomain : ISystemDeviceDomain
    {

        #region Properties

        public Guid Id { get; set; }

        public Guid SystemId { get; set; }
        public SystemDomain System { get; set; }

        public Guid DeviceId { get; set; }
        public DeviceDomain Device { get; set; }

        #endregion

    }

}
