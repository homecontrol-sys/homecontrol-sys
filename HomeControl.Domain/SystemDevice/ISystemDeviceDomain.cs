using System;

namespace HomeControl.Domain
{

    public interface ISystemDeviceDomain
    {
        
        #region Properties

        Guid Id { get; set; }

        Guid SystemId { get; set; }
        SystemDomain System { get; set; }

        Guid DeviceId { get; set; }
        DeviceDomain Device { get; set; }

        #endregion
    
    }
    
}