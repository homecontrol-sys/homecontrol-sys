namespace HomeControl.Domain
{

    public interface IModuleSearch
    {
        
        #region Properties

        string Name { get; set; }
        string Description { get; set; }

        #endregion
    
    }
    
}