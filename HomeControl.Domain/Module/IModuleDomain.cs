using System;
using System.Collections.Generic;
using HomeControl.Domain._Common;

namespace HomeControl.Domain
{

    public interface IModuleDomain
    {
        
        #region Properties
        
        Guid Id { get; set; }
        string Name { get; set; }

        Guid DeviceId { get; set; }
        DeviceDomain Device { get; set; }
        ICollection<ElementDomain> Elements { get; set; }
        ICollection<PinDomain> Pins { get; set; }

        #endregion
    
    }

}
