using System;
using System.Collections.Generic;
using HomeControl.Domain._Common;

namespace HomeControl.Domain
{

    public class ModuleDomain : IModuleDomain
    {
        
        #region Properties
        
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Guid DeviceId { get; set; }
        public DeviceDomain Device { get; set; }
        public ICollection<ElementDomain> Elements { get; set; }
        public ICollection<PinDomain> Pins { get; set; }

        #endregion
    
    }

}
