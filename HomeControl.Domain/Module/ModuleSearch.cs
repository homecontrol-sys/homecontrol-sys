namespace HomeControl.Domain
{

    public class ModuleSearch : IModuleSearch
    {
        
        #region Properties

        public string Name { get; set; }
        public string Description { get; set; }

        #endregion
    
    }
    
}