using System;

namespace HomeControl.Domain._Common
{

    public interface IDomainBaseModel
    {
        
        #region Properties

        #region Identificator

        Guid Id { get; set; }

        #endregion

        #region Jurnalism

        DateTime DateCreated { get; set; } 
        Guid CreatedByUserId { get; set; }
        DateTime DateUpdated { get; set; }
        Guid UpdatedByUserId { get; set; }
        
        string Action { get; set; }
        
        #endregion
        
        #endregion
    
    }

}