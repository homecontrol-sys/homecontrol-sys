using System;

namespace HomeControl.Domain._Common
{

    public class DomainBaseModel : IDomainBaseModel
    {
        
        #region Properties

        #region Identificator

        public Guid Id { get; set; }

        #endregion

        #region Jurnalism

        public DateTime DateCreated { get; set; } 
        public Guid CreatedByUserId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByUserId { get; set; }
        public string Action { get; set; }
        
        #endregion
        
        #endregion
    
    }

}