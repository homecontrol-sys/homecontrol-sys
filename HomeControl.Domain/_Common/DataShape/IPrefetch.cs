using System.Collections.Generic;

namespace HomeControl.Domain._Common.DataShape
{

    public interface IPrefetch
    {

        IEnumerable<string> Prefetch { get; set; }
    
    }

}