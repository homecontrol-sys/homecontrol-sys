using System.Collections;

namespace HomeControl.Domain._Common.DataShape
{

    public interface IResourceParams<TFilterResource, TSearchResource> 
    : IPrefetch, IFilter<TFilterResource>, ISearch<TSearchResource>
    {

    }

}