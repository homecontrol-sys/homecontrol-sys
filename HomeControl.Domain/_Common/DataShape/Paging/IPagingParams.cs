namespace HomeControl.Domain._Common.DataShape
{

    public interface IPagingParams 
    {
        
        #region Properties

        int PageNumber { get; set; }
        int PageSize { get; set; }

        #endregion

    }

}