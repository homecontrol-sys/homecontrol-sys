using System;
using System.Collections.Generic;

namespace HomeControl.Domain._Common.DataShape
{

    public interface IFilter<TResource>
    {

        TResource Filter {get; set;}
        // IEnumerable<Guid> Ids { get; set; }
    
    }

}