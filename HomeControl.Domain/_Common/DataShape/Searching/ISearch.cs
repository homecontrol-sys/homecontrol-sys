
namespace HomeControl.Domain._Common.DataShape
{

    public interface ISearch<TResource>
    {
        TResource SearchBy { get; set; }
    
    }

}