namespace HomeControl.Domain._Common.DataShape
{

    public class SearchParams : ISearchParams 
    {

        #region Properties

        public string SearchQuery { get; set; }

        #endregion

    }

}