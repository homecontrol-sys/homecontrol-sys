namespace HomeControl.Domain._Common.DataShape
{
    public interface ISearchParams 
    {
        
        #region Properties

        string SearchQuery { get; set; }

        #endregion

    }

}
