using System;

namespace HomeControl.Domain
{

    public class PinDomain : IPinDomain
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Number { get; set; }

        public Guid PinTypeId { get; set; }
        public PinTypeDomain PinType { get; set; }
        public Guid? PinConnectionId { get; set; }
        public PinConnectionDomain PinConnection { get; set; }
        public Guid? DeviceId { get; set; }
        public DeviceDomain Device { get; set; }
        public Guid? ModuleId { get; set; }
        public ModuleDomain Module { get; set; }
        public Guid? ElementId { get; set; }
        public ElementDomain Element { get; set; }

        #endregion

    }

}
