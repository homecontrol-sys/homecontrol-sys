using System;

namespace HomeControl.Domain
{

    public interface IPinDomain
    {
        
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        int Number { get; set; }

        Guid PinTypeId { get; set; }
        PinTypeDomain PinType { get; set; }
        Guid? PinConnectionId { get; set; }
        PinConnectionDomain PinConnection { get; set; }
        Guid? DeviceId { get; set; }
        DeviceDomain Device { get; set; }
        Guid? ModuleId { get; set; }
        ModuleDomain Module { get; set; }
        Guid? ElementId { get; set; }
        ElementDomain Element { get; set; }

        #endregion
    
    }
    
}