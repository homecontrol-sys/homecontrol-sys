using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public class DeviceDomain : IDeviceDomain
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<SystemDeviceDomain> SystemDevices { get; set; }
        public ICollection<ModuleDomain> Modules { get; set; }
        public ICollection<PinDomain> Pins { get; set; }

        #endregion

    }

}
