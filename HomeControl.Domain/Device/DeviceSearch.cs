namespace HomeControl.Domain
{

    public class DeviceSearch : IDeviceSearch
    {
        
        #region Properties

        public string Name { get; set; }

        #endregion
    
    }
    
}