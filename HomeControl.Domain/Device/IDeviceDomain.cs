using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface IDeviceDomain
    {
        
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }

        ICollection<SystemDeviceDomain> SystemDevices { get; set; }
        ICollection<ModuleDomain> Modules { get; set; }
        ICollection<PinDomain> Pins { get; set; }

        #endregion
    
    }
    
}