using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface IDeviceFilter
    {
        
        #region Properties

        IEnumerable<Guid> Ids { get; set; }
        string Name { get; set; }
        IEnumerable<Guid> SystemIds { get; set; }

        #endregion
    
    }
    
}