namespace HomeControl.Domain
{

    public interface IDeviceSearch
    {
        
        #region Properties

        string Name { get; set; }

        #endregion
    
    }
    
}