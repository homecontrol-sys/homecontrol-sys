using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public class DeviceFilter : IDeviceFilter
    {
        
        #region Properties

        public IEnumerable<Guid> Ids { get; set; }
        public string Name { get; set; }
        public IEnumerable<Guid> SystemIds { get; set; }

        #endregion
    
    }
    
}