using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface IPinTypeDomain
    {
        
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }

        ICollection<PinDomain> Pins { get; set; }

        #endregion
    
    }
    
}
