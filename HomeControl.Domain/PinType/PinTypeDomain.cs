using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public class PinTypeDomain : IPinTypeDomain
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<PinDomain> Pins { get; set; }

        #endregion

    }

}
