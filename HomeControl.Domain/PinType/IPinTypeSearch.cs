namespace HomeControl.Domain
{

    public interface IPinTypeSearch
    {
        
        #region Properties

        string Name { get; set; }

        #endregion
    
    }
    
}