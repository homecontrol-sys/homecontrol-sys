namespace HomeControl.Domain.Identity
{
    public class UserClaimDomain : IUserClaimDomain
    {

        #region Properties

        public UserDomain User { get; set; }

        public bool CanAccessLogs { get; set; }
        public bool CanReadElements { get; set; }
        public bool CanReadDevices { get; set; }
        public bool CanDeleteElements { get; set; }
        public bool CanDeleteDevices { get; set; }

        #endregion

    }
}
