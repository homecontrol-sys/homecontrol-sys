namespace HomeControl.Domain.Identity
{
    public interface IRoleClaimDomain
    {

        #region Properties

        RoleDomain Role { get; set; }

        #endregion

    }
}
