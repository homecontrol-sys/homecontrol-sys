using System;

namespace HomeControl.Domain.Identity
{
    public class RoleClaimDomain : IRoleClaimDomain
    {

        #region Properties

        public RoleDomain Role { get; set; }
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public Guid CreatedByRoleId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByRoleId { get; set; }
        public string Action { get; set; }

        #endregion
    }
}
