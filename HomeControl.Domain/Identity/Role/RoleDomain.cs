using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.Domain.Identity
{
    public class RoleDomain : IdentityRole<Guid>, IRoleDomain
    {

        #region Properties

        public ICollection<UserRoleDomain> UserRoles { get; set; }
        public ICollection<RoleClaimDomain> RoleClaims { get; set; }
        public DateTime DateCreated { get; set; }
        public Guid CreatedByRoleId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByRoleId { get; set; }
        public string Action { get; set; }


        #endregion
    }
}
