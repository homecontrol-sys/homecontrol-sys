using System.Collections.Generic;

namespace HomeControl.Domain.Identity
{
    public interface IRoleDomain
    {

        #region Properties
 
        ICollection<UserRoleDomain> UserRoles { get; set; }
        ICollection<RoleClaimDomain> RoleClaims { get; set; }

        #endregion

    }
}
