using System;
using System.Collections.Generic;

namespace HomeControl.Domain.Identity
{
    
    public interface IUserDomain
    {
        
        #region Properties
 
        string FirstName { get; set; }
        string LastName { get; set; }
        string PasswordSalt { get; set; }
        DateTime PasswordModifiedDate { get; set; }
        DateTime? LastLoginDate { get; set; }
        short Status { get; set; }

        #endregion

        #region Navigation properties

        IEnumerable<UserRoleDomain> UserRoles { get; set; } 
        IEnumerable<UserClaimDomain> UserClaims { get; set; }

        #endregion

    }

}
