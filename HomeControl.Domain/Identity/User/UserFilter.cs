namespace HomeControl.Domain.Identity
{
    public class UserFilter
    {

        #region Properties

        public string UserName { get; set; }
        public string EMail { get; set; }

        #endregion


        #region Composed

        public string UserNameOrEMail { get; set; }
        public string Password { get; set; }

        #endregion

    }

}
