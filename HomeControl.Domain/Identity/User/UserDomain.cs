using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.Domain.Identity
{
    public class UserDomain : IdentityUser<Guid>, IUserDomain
    {
        
        #region Properties
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime PasswordModifiedDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public short Status { get; set; }

        #endregion

        #region Navigation properties

        public IEnumerable<UserRoleDomain> UserRoles { get; set; } 
        public IEnumerable<UserClaimDomain> UserClaims { get; set; } 

        #endregion

    }

}
