namespace HomeControl.Domain.Identity
{
    public interface IUserFilter
    {

        #region Properties

        string UserName { get; set; }
        string EMail { get; set; }

        #endregion


        #region To Resolve

        string UserNameOrEMail { get; set; }

        string Password { get; set; }

        #endregion

    }

}
