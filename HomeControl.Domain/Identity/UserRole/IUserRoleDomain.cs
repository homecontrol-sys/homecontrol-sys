namespace HomeControl.Domain.Identity
{

    public interface IUserRoleDomain
    {
        
        #region Properties 

        UserDomain User { get; set; }
        RoleDomain Role { get; set; }

        #endregion 

    }
}
