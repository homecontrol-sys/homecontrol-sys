namespace HomeControl.Domain.Identity
{
    public class UserRoleDomain : IUserRoleDomain
    {
        
        #region Properties

        public UserDomain User { get; set; }
        public RoleDomain Role { get; set; }

        #endregion 

    }
}
