using System;
using Microsoft.Extensions.Configuration;

namespace HomeControl.Domain.Identity
{
    public class JwtSettings : IJwtSettings
    {
        
        #region Properties

        public string Key { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int MinutesToExpiration { get; set; }
         
        #endregion

        #region Constructor

        public JwtSettings(IConfigurationSection jwtTokenConfig)
        {
            Key = jwtTokenConfig["key"];
            Issuer = jwtTokenConfig["issuer"];
            Audience = jwtTokenConfig["audience"];
            MinutesToExpiration = Convert.ToInt32(jwtTokenConfig["minutestoexpiration"]);
        }

        #endregion

    }

}