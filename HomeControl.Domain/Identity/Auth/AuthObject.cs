using System.Collections.Generic;

namespace HomeControl.Domain.Identity
{
    public class AuthObject : IAuthObject
    {
        
        #region Properties

        public string UserNameOrEMail { get; set; }
        public string Password { get; set; }
        public bool IsEMailConfirmed { get; set; }
        public string BearerToken { get; set; }
        public bool IsAuthenticated {get; set; }

        public UserRoleDomain userRole { get; set; }
        public ICollection<UserClaimDomain> userClaims { get; set; }
         
        #endregion

    }

}
