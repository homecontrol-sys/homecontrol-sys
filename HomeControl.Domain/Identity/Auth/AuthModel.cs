namespace HomeControl.Domain.Identity
{
    public class AuthModel : IAuthModel
    {
        
        #region Properties

        public string UserNameOrEMail { get; set; }
        public string Password { get; set; }
         
        #endregion

    }

}
