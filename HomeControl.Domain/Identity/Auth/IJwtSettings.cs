namespace HomeControl.Domain.Identity
{
    public interface IJwtSettings
    {
        
        #region Propertie

        string Key { get; set; }
        string Issuer { get; set; }
        string Audience { get; set; }
        int MinutesToExpiration { get; set; }
         
        #endregion

    }

}