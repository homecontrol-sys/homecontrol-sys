using System.Collections.Generic;

namespace HomeControl.Domain.Identity
{
    public interface IAuthObject
    {
        
        #region Properties

        string UserNameOrEMail { get; set; }
        string Password { get; set; }
        bool IsEMailConfirmed { get; set; }
        string BearerToken { get; set; }
        bool IsAuthenticated {get; set; }

        UserRoleDomain userRole { get; set; }
        ICollection<UserClaimDomain> userClaims { get; set; }
         
        #endregion

    }

}
