namespace HomeControl.Domain.Identity
{
    public interface IAuthModel
    {
        
        #region Properties

        string UserNameOrEMail { get; set; }
        string Password { get; set; }
         
        #endregion

    }

}
