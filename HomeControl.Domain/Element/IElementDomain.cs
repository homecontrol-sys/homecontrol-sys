using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.Domain
{

    public interface IElementDomain : ILookupBaseModel
    {
        
        #region Properties
    
        string Alias { get; set; }

        Guid SystemId { get; set; }
        SystemDomain System { get; set; }
        Guid DeviceId { get; set; }
        DeviceDomain Device { get; set; }
        Guid ModuleId { get; set; }
        ModuleDomain Module { get; set; }
        Guid SwitchId { get; set; }
        SwitchDomain Switch { get; set;}
        Guid SwitchStateId { get; set; }
        SwitchStateDomain SwitchState { get; set; }
        ICollection<PinDomain> Pins { get; set; }

        #endregion
    
    }

}
