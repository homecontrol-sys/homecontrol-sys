using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public class ElementFilter : IElementFilter
    {
        
        #region Properties

        public IEnumerable<Guid> Ids { get; set; }
        public string Name { get; set; }
        public bool SystemElements { get; set; }
        public bool DeviceElements { get; set; }
        public IEnumerable<Guid> SystemElementIds { get; set; }
        public IEnumerable<Guid> DeviceElementIds { get; set; }

        #endregion

    }
    
}