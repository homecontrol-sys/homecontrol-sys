namespace HomeControl.Domain
{

    public interface IElementSearch
    {
        
        #region Properties

        string Name { get; set; }
        bool SystemName { get; set; }
        bool DeviceName { get; set; }

        #endregion
    
    }
    
}