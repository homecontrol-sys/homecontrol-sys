using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface IElementFilter
    {
        
        #region Properties

        IEnumerable<Guid> Ids { get; set; }
        string Name { get; set; }
        bool SystemElements { get; set; }
        bool DeviceElements { get; set; }
        IEnumerable<Guid> SystemElementIds { get; set; }
        IEnumerable<Guid> DeviceElementIds { get; set; }

        #endregion
    
    }
    
}