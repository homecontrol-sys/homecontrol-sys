using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.Domain
{

    public class ElementDomain : LookupBaseModel, IElementDomain
    {

        #region Properties

        public string Alias { get; set; }

        public Guid SystemId { get; set; }
        public SystemDomain System { get; set; }
        public Guid DeviceId { get; set; }
        public DeviceDomain Device { get; set; }
        public Guid ModuleId { get; set; }
        public ModuleDomain Module { get; set; }
        public Guid SwitchId { get; set; }
        public SwitchDomain Switch { get; set;}
        public Guid SwitchStateId { get; set; }
        public SwitchStateDomain SwitchState { get; set; }
        public ICollection<PinDomain> Pins { get; set; }

        #endregion

    }
    
}
