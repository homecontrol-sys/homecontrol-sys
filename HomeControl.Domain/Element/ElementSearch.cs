namespace HomeControl.Domain
{

    public class ElementSearch : IElementSearch
    {
        
        #region Properties

        public string Name { get; set; }
        public bool SystemName { get; set; }
        public bool DeviceName { get; set; }

        #endregion
    
    }
    
}