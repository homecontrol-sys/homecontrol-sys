namespace HomeControl.Domain
{

    public interface IMainSystemConfiguration
    {
        
        #region Properties

        string Name { get; set; }
        string SSID { get; set; }
        string Password { get; set; }

        #endregion
    
    }

}