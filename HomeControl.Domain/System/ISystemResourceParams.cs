using HomeControl.Domain._Common.DataShape;

namespace HomeControl.Domain
{

    public interface ISystemResourceParams : IResourceParams<SystemFilter, SystemSearch>
    {
        
        #region Properties

        #endregion
    
    }
    
}