using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface ISystemDomain
    {
        
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        bool IsMain { get; set; }

        Guid? NetworkConfigurationId { get; set; }
        NetworkConfigurationDomain NetworkConfiguration { get; set; }
        ICollection<ElementDomain> Elements { get; set; }
        ICollection<SystemDeviceDomain> SystemDevices { get; set; }

        #endregion
    
    }

}