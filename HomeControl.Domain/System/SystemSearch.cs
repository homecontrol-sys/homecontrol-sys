namespace HomeControl.Domain
{

    public class SystemSearch : ISystemSearch
    {
        
        #region Properties

        public string Name { get; set; }
        public string Description { get; set; }

        #endregion
    
    }
    
}