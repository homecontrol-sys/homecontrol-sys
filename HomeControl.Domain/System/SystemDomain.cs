using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public class SystemDomain : ISystemDomain
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsMain { get; set; }
        
        public Guid? NetworkConfigurationId { get; set; }
        public NetworkConfigurationDomain NetworkConfiguration { get; set; }
        public ICollection<ElementDomain> Elements { get; set; }
        public ICollection<SystemDeviceDomain> SystemDevices { get; set; }

        #endregion
    
    }

}
