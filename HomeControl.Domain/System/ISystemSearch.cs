namespace HomeControl.Domain
{

    public interface ISystemSearch
    {
        
        #region Properties

        string Name { get; set; }
        string Description { get; set; }

        #endregion
    
    }
    
}