using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface ISystemFilter
    {
        
        #region Properties

        IEnumerable<Guid> Ids { get; set; }
        bool? IsMain { get; set; }
        string Name { get; set; }

        #endregion
    
    }
    
}