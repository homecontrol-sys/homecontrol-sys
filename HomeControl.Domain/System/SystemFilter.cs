using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public class SystemFilter : ISystemFilter
    {
        
        #region Properties

        public IEnumerable<Guid> Ids { get; set; }
        public bool? IsMain { get; set; }
        public string Name { get; set; }
        
        #endregion
    
    }
    
}