namespace HomeControl.Domain
{

    public class MainSystemConfiguration : IMainSystemConfiguration
    {

        #region Properties

        public string Name { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }

        #endregion
    
    }

}
