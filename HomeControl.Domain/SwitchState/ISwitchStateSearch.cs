namespace HomeControl.Domain
{

    public interface ISwitchStateSearch
    {
        
        #region Properties

        string Name { get; set; }

        #endregion
    
    }
    
}