using System;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.Domain
{

    public class SwitchStateDomain : LookupBaseModel, ISwitchStateDomain
    {

        #region Properties
        
        public int Value { get; set; }
        public string Alias { get; set; }

        public Guid SwitchId { get; set; }
        public SwitchDomain Switch { get; set; }
        public Guid ElementId { get; set; }
        public ElementDomain Element { get; set; }

        #endregion

    }
    
}
