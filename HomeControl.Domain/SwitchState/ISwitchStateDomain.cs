using System;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.Domain
{

    public interface ISwitchStateDomain : ILookupBaseModel
    {

        #region Properties
    
        int Value { get; set; }
        string Alias { get; set; }

        Guid SwitchId { get; set; }
        SwitchDomain Switch { get; set; }
        Guid ElementId { get; set; }
        ElementDomain Element { get; set; }

        #endregion

    }
    
}
