using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface ISwitchStateFilter
    {
        
        #region Properties

        IEnumerable<Guid> Ids { get; set; }
        string Name { get; set; }

        #endregion
    
    }
    
}