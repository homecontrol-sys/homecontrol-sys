using System;

namespace HomeControl.Domain
{

    public class NetworkConfigurationDomain : INetworkConfigurationDomain
    {

        #region Properties
    
        public Guid Id { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }
        public bool IsConnectionVerified { get; set; }

        public SystemDomain System { get; set; }

        #endregion

    }
    
}
