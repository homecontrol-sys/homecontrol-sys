namespace HomeControl.Domain
{

    public class NetworkConfigurationSearch : INetworkConfigurationSearch
    {
        
        #region Properties

        public string SSID { get; set; }

        #endregion
    
    }
    
}