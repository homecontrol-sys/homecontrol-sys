using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface INetworkConfigurationFilter
    {
        
        #region Properties

        IEnumerable<Guid> Ids { get; set; }
        string SSID { get; set; }

        #endregion
    
    }
    
}