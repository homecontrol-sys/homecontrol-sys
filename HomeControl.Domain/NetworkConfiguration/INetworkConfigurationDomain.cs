using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public interface INetworkConfigurationDomain
    {
        
        #region Properties
    
        Guid Id { get; set; }
        string SSID { get; set; }
        string Password { get; set; }
        bool IsConnectionVerified { get; set; }

        SystemDomain System { get; set; }

        #endregion
    
    }

}
