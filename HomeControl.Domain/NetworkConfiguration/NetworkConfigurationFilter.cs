using System;
using System.Collections.Generic;

namespace HomeControl.Domain
{

    public class NetworkConfigurationFilter : INetworkConfigurationFilter
    {
        
        #region Properties

        public IEnumerable<Guid> Ids { get; set; }
        public string SSID { get; set; }

        #endregion

    }
    
}