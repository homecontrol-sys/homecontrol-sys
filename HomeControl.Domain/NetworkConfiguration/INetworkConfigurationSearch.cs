namespace HomeControl.Domain
{

    public interface INetworkConfigurationSearch
    {
        
        #region Properties

        string SSID { get; set; }

        #endregion
    
    }
    
}