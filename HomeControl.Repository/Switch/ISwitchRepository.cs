using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface ISwitchRepository : IRepositoryBase
    {

        Task<IEnumerable<SwitchEntity>> Find(IEnumerable<string> prefetch = null, SwitchFilter filter = null, SwitchSearch search = null);
        Task<bool> SwitchStateExistsOnSwitch(Guid switchId, Guid switchStateId);
        
    }

}