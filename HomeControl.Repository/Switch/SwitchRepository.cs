using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository._Extension;
using HomeControl.Repository._Utils;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository
{

    public class SwitchRepository : RepositoryBase, ISwitchRepository
    {
       
        #region Constructor

        public SwitchRepository(HomeControlDbContext dbContext) : base(dbContext)
        {

        }

        #endregion

        #region Methods

        #region Read
        
        public async Task<IEnumerable<SwitchEntity>> Find(IEnumerable<string> prefetch = null, SwitchFilter filter = null, SwitchSearch search = null)
        {
            var query = base.CreateQuery<SwitchEntity>();

            query = ApplyPrefetch(query, prefetch);
            query = ApplyFilter(query, filter);
            query = ApplySearch(query, search);
            
            return await query.AsNoTracking().ToArrayAsync();
        }

        #endregion

        #region PRIVATE Utils

        private IQueryable<SwitchEntity> ApplyPrefetch(IQueryable<SwitchEntity> query, IEnumerable<string> prefetch)
        {

            if (prefetch == null || prefetch.Count() == 0) return query;
            else
            {
                return query
                    .If(prefetch.Contains(It<SwitchEntity>.NameOf(s => s.SwitchStates)), q => q.Include(s => s.SwitchStates));
            }
        }

        private IQueryable<SwitchEntity> ApplyFilter(IQueryable<SwitchEntity> query, SwitchFilter filter)
        {

            if (filter == null) return query;
            else
            {
                if (filter.Ids != null && filter.Ids.Any())
                {
                    query = query.Where(e => filter.Ids.Contains(e.Id));
                }
                if (!string.IsNullOrWhiteSpace(filter.Name))
                {
                    var name = filter.Name.Trim();
                    query = query.Where(e => filter.Name.Contains(name));
                }

                return query;
            }        
        }

        private IQueryable<SwitchEntity> ApplySearch(IQueryable<SwitchEntity> query, SwitchSearch search)
        {
            if (search == null) return query;
            else 
            {
                if (!string.IsNullOrWhiteSpace(search.Name))
                {
                    var searchQuery = search.Name.Trim();
                    query = query.Where(e => search.Name.Contains(searchQuery));
                }

                return query;
            }
        }

        #endregion

        public async Task<bool> SwitchStateExistsOnSwitch(Guid switchId, Guid switchStateId)
        {
            var query = base.CreateQuery<SwitchEntity>();

            return await query.Where(s => s.Id == switchId).Select(ss => ss.SwitchStates.Where(ss => ss.Id == switchStateId)).AnyAsync();
        }

        #endregion

    }
}