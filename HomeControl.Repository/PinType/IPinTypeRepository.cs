using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface IPinTypeRepository : IRepositoryBase
    {
        
        Task<IEnumerable<PinTypeEntity>> Find(IEnumerable<string> prefetch = null, PinTypeFilter filter = null, PinTypeSearch search = null);

    }

}