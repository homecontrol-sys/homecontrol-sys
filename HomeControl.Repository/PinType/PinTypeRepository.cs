using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository
{
    public class PinTypeRepository : RepositoryBase, IPinTypeRepository
    {
       
        #region Fields

        #endregion


        #region Constructor
        
        public PinTypeRepository(HomeControlDbContext dbContext) : base(dbContext)
        {
        }

        #endregion

        #region Read
        
        public async Task<IEnumerable<PinTypeEntity>> Find(IEnumerable<string> prefetch = null, PinTypeFilter filter = null, PinTypeSearch search = null)
        {
            var query = base.CreateQuery<PinTypeEntity>();

            query = ApplyPrefetch(query, prefetch);
            query = ApplyFilter(query, filter);
            query = ApplySearch(query, search);
            
            return await query.ToArrayAsync();
        }

        #endregion

        #region PRIVATE Utils

        private IQueryable<PinTypeEntity> ApplyPrefetch(IQueryable<PinTypeEntity> query, IEnumerable<string> prefetch)
        {

            if (prefetch == null || prefetch.Count() == 0) return query;
            else
            {
                // if (prefetch.Contains(It<PinTypeEntity>.NameOf(s => s.Elements)))
                // {
                //     query = query.Include(x => x.Elements);
                // }

                return query;
            }
        }

        private IQueryable<PinTypeEntity> ApplyFilter(IQueryable<PinTypeEntity> query, PinTypeFilter filter)
        {

            if (filter == null) return query;
            else
            {
                if (filter.Ids != null && filter.Ids.Any())
                {
                    query = query.Where(e => filter.Ids.Contains(e.Id));
                }
                if (!string.IsNullOrWhiteSpace(filter.Name))
                {
                    var name = filter.Name.Trim();
                    query = query.Where(e => filter.Name.Contains(name));
                }

                return query;
            }        
        }

        private IQueryable<PinTypeEntity> ApplySearch(IQueryable<PinTypeEntity> query, PinTypeSearch search)
        {
            if (search == null) return query;
            else 
            {
                if (!string.IsNullOrWhiteSpace(search.Name))
                {
                    var searchQuery = search.Name.Trim();
                    query = query.Where(e => search.Name.Contains(searchQuery));
                }

                return query;
            }
        }

        #endregion

    }

}