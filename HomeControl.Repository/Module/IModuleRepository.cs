using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface IModuleRepository : IRepositoryBase
    {
        
        Task<IEnumerable<ModuleEntity>> Find(IEnumerable<string> prefetch = null, ModuleFilter filter = null, ModuleSearch search = null);

    }

}