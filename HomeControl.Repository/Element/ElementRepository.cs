using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository._Extension;
using HomeControl.Repository._Utils;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository
{

    public class ElementRepository : RepositoryBase, IElementRepository
    {
       
        #region Constructor

        public ElementRepository(HomeControlDbContext dbContext) : base(dbContext)
        {

        }

        #endregion

        #region Read
        
        public async Task<IEnumerable<ElementEntity>> Find(IEnumerable<string> prefetch = null, ElementFilter filter = null, ElementSearch search = null)
        {
            var query = base.CreateQuery<ElementEntity>();

            query = ApplyPrefetch(query, prefetch);
            query = ApplyFilter(query, filter);
            query = ApplySearch(query, search);
            
            return await query.ToArrayAsync();
        }

        #endregion

        #region PRIVATE Utils

        private IQueryable<ElementEntity> ApplyPrefetch(IQueryable<ElementEntity> query, IEnumerable<string> prefetch)
        {

            if (prefetch == null || prefetch.Count() == 0) return query;
            else
            {
                // if (prefetch.Contains(It<ElementEntity>.NameOf(s => s.Switch)))
                // {
                //     query = query.Include(x => x.Switch);
                // }
                return query
                    .If(prefetch.Contains(It<ElementEntity>.NameOf(s => s.Switch)), q => q.Include(s => s.Switch));
            }
        }

        private IQueryable<ElementEntity> ApplyFilter(IQueryable<ElementEntity> query, ElementFilter filter)
        {

            if (filter == null) return query;
            else
            {
                if (filter.Ids != null && filter.Ids.Any())
                {
                    query = query.Where(e => filter.Ids.Contains(e.Id));
                }
                if (!string.IsNullOrWhiteSpace(filter.Name))
                {
                    var name = filter.Name.Trim();
                    query = query.Where(e => filter.Name.Contains(name));
                }

                return query;
            }        
        }

        private IQueryable<ElementEntity> ApplySearch(IQueryable<ElementEntity> query, ElementSearch search)
        {
            if (search == null) return query;
            else 
            {
                if (!string.IsNullOrWhiteSpace(search.Name))
                {
                    var searchQuery = search.Name.Trim();
                    query = query.Where(e => search.Name.Contains(searchQuery));
                }

                return query;
            }
        }

        #endregion

    }
}