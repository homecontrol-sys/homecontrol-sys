using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface IElementRepository : IRepositoryBase
    {
        
        Task<IEnumerable<ElementEntity>> Find(IEnumerable<string> prefetch = null, ElementFilter filter = null, ElementSearch search = null);

    }

}