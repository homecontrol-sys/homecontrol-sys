using Microsoft.Extensions.DependencyInjection;

namespace HomeControl.Repository
{

    public static class RepositoryLayerProvider
    {
        public static IServiceCollection ProvideRepositoryLayer(this IServiceCollection services)
        {
            
            services.ProvideRepositoryIdentityLayer();        
            services.AddScoped<IDeviceRepository, DeviceRepository>();
            services.AddScoped<IElementRepository, ElementRepository>();
            services.AddScoped<ISwitchRepository, SwitchRepository>();
            services.AddScoped<ISwitchStateRepository, SwitchStateRepository>();
            services.AddScoped<IModuleRepository, ModuleRepository>();
            services.AddScoped<INetworkConfigurationRepository, NetworkConfigurationRepository>();
            services.AddScoped<IPinRepository, PinRepository>();
            services.AddScoped<IPinConnectionRepository, PinConnectionRepository>();
            services.AddScoped<IPinTypeRepository, PinTypeRepository>();
            services.AddScoped<ISystemRepository, SystemRepository>();
            
            return services;
        }
    }

}