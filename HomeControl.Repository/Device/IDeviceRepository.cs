using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface IDeviceRepository : IRepositoryBase
    {      

        Task<IEnumerable<DeviceEntity>> Find(IEnumerable<string> prefetch = null, DeviceFilter filter = null, DeviceSearch search = null);
     
    }

}