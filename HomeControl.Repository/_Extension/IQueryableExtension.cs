using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Query;

namespace HomeControl.Repository._Extension
{
    public static class IQueryableExtension
    { 
        public static IQueryable<T> If<T>(
            this IQueryable<T> source,
            bool condition,
            Func<IQueryable<T>, IQueryable<T>> transform
        )
        { 
            return condition ? transform(source) : source;
        }

        public static IQueryable<T> If<T, P>(
            this IIncludableQueryable<T, P> source,
            bool condition,
            Func<IIncludableQueryable<T, P>, IQueryable<T>> transform
        )
            where T : class
        {
            return condition ? transform(source) : source;
        }

        public static IQueryable<T> If<T, P>(
            this IIncludableQueryable<T, ICollection<P>> source,
            bool condition,
            Func<IIncludableQueryable<T, ICollection<P>>, IQueryable<T>> transform
        )
            where T : class
        {
            return condition ? transform(source) : source;
        }

    }
}
