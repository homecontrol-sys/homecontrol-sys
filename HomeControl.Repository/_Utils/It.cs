using System;
using System.Linq.Expressions;

namespace HomeControl.Repository._Utils
{

    public class It<T>
    {

        public static string NameOf<TValue>(Expression<Func<T, TValue>> memberAccess)
        {
            return ((MemberExpression)memberAccess.Body).Member.Name;
        }
        
    }

}