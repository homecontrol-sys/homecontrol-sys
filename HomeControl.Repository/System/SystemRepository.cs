using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository._Utils;
using Microsoft.EntityFrameworkCore;
using HomeControl.Repository._Extension;
using Microsoft.EntityFrameworkCore.Query;

namespace HomeControl.Repository
{
    public class SystemRepository : RepositoryBase, ISystemRepository
    {
       
        #region Fields

        #endregion

        #region Constructor
        
        public SystemRepository(HomeControlDbContext dbContext) : base(dbContext)
        {
        }

        #endregion

        #region Read
        
        public async Task<IEnumerable<SystemEntity>>Find(IEnumerable<string> prefetch = null, SystemFilter filter = null, SystemSearch search = null)
        {
            var query = base.CreateQuery<SystemEntity>();

            query = ApplyPrefetch(query, prefetch);
            query = ApplyFilter(query, filter);
            query = ApplySearch(query, search);
            
            return await query.AsNoTracking().ToArrayAsync();
        }

        #endregion

        #region PRIVATE Utils

        private IQueryable<SystemEntity> ApplyPrefetch(IQueryable<SystemEntity> query, IEnumerable<string> prefetch)
        {

            if (prefetch == null || prefetch.Count() == 0) return query;
            else
            {
                return query
                    .If(prefetch.Contains(It<SystemEntity>.NameOf(s => s.Elements)), q => q.Include(s => s.Elements) 
                        .If(prefetch.Contains(It<ElementEntity>.NameOf(e => e.Switch)), sq  => sq.ThenInclude(s => s.Switch)
                            .If(prefetch.Contains(It<SwitchEntity>.NameOf(e => e.SwitchStates)), 
                            ssq => ssq.ThenInclude(s => s.SwitchStates.OrderBy(ss => ss.SortOrder)))
                        )
                    )
                    .If(prefetch.Contains(It<SystemEntity>.NameOf(s => s.Elements)), q => q.Include(s => s.Elements) 
                        .If(prefetch.Contains(It<ElementEntity>.NameOf(e => e.SwitchState)), sq  => sq.ThenInclude(s => s.SwitchState))
                    );
            }
        }

        private IQueryable<SystemEntity> ApplyFilter(IQueryable<SystemEntity> query, SystemFilter filter)
        {

            if (filter == null) return query;
            else
            {
                if (filter.Ids != null && filter.Ids.Any())
                {
                    query = query.Where(e => filter.Ids.Contains(e.Id));
                }
                if (filter.IsMain != null)
                {
                    query = query.Where(e => e.IsMain == filter.IsMain);
                }
                if (!string.IsNullOrWhiteSpace(filter.Name))
                {
                    var name = filter.Name.Trim();
                    query = query.Where(e => filter.Name.Contains(name));
                }

                return query;
            }
        }

        private IQueryable<SystemEntity> ApplySearch(IQueryable<SystemEntity> query, SystemSearch search)
        {
            if (search == null) return query;
            else 
            {
                if (!string.IsNullOrWhiteSpace(search.Name))
                {
                    var searchQuery = search.Name.Trim();
                    query = query.Where(e => search.Name.Contains(searchQuery));
                }
                if (!string.IsNullOrWhiteSpace(search.Description))
                {
                    var searchQuery = search.Description.Trim();
                    query = query.Where(e => search.Description.Contains(searchQuery));
                }

                return query;
            }
        }

        #endregion

    }

}