using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface ISystemRepository : IRepositoryBase
    {

        Task<IEnumerable<SystemEntity>> Find(IEnumerable<string> prefetch = null, SystemFilter filter = null, SystemSearch search = null);
        
    }

}