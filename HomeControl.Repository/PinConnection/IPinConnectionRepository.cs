using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface IPinConnectionRepository : IRepositoryBase
    {
        
        Task<IEnumerable<PinConnectionEntity>> Find(IEnumerable<string> prefetch = null, PinConnectionFilter filter = null, PinConnectionSearch search = null);

    }

}