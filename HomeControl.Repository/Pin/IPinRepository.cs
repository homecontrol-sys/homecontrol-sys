using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface IPinRepository : IRepositoryBase
    {
        
        Task<IEnumerable<PinEntity>> Find(IEnumerable<string> prefetch = null, PinFilter filter = null, PinSearch search = null);

    }

}