using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository
{
    public class PinRepository : RepositoryBase, IPinRepository
    {
       
        #region Fields

        #endregion


        #region Constructor
        
        public PinRepository(HomeControlDbContext dbContext) : base(dbContext)
        {
        }

        #endregion

        #region Read
        
        public async Task<IEnumerable<PinEntity>> Find(IEnumerable<string> prefetch = null, PinFilter filter = null, PinSearch search = null)
        {
            var query = base.CreateQuery<PinEntity>();

            query = ApplyPrefetch(query, prefetch);
            query = ApplyFilter(query, filter);
            query = ApplySearch(query, search);
            
            return await query.ToArrayAsync();
        }

        #endregion

        #region PRIVATE Utils

        private IQueryable<PinEntity> ApplyPrefetch(IQueryable<PinEntity> query, IEnumerable<string> prefetch)
        {

            if (prefetch == null || prefetch.Count() == 0) return query;
            else
            {
                // if (prefetch.Contains(It<PinEntity>.NameOf(s => s.Elements)))
                // {
                //     query = query.Include(x => x.Elements);
                // }

                return query;
            }
        }

        private IQueryable<PinEntity> ApplyFilter(IQueryable<PinEntity> query, PinFilter filter)
        {

            if (filter == null) return query;
            else
            {
                if (filter.Ids != null && filter.Ids.Any())
                {
                    query = query.Where(e => filter.Ids.Contains(e.Id));
                }
                if (!string.IsNullOrWhiteSpace(filter.Name))
                {
                    var name = filter.Name.Trim();
                    query = query.Where(e => filter.Name.Contains(name));
                }

                return query;
            }        
        }

        private IQueryable<PinEntity> ApplySearch(IQueryable<PinEntity> query, PinSearch search)
        {
            if (search == null) return query;
            else 
            {
                if (!string.IsNullOrWhiteSpace(search.Name))
                {
                    var searchQuery = search.Name.Trim();
                    query = query.Where(e => search.Name.Contains(searchQuery));
                }

                return query;
            }
        }

        #endregion

    }

}