using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository
{

    public class NetworkConfigurationRepository : RepositoryBase, INetworkConfigurationRepository
    {
       
        #region Constructor

        public NetworkConfigurationRepository(HomeControlDbContext dbContext) : base(dbContext)
        {

        }

        #endregion

        #region Read
        
        public async Task<IEnumerable<NetworkConfigurationEntity>> Find(
            IEnumerable<string> prefetch = null, NetworkConfigurationFilter filter = null, NetworkConfigurationSearch search = null)
        {
            var query = base.CreateQuery<NetworkConfigurationEntity>();

            query = ApplyPrefetch(query, prefetch);
            query = ApplyFilter(query, filter);
            query = ApplySearch(query, search);
            
            return await query.ToArrayAsync();
        }

        #endregion

        #region PRIVATE Utils

        private IQueryable<NetworkConfigurationEntity> ApplyPrefetch(IQueryable<NetworkConfigurationEntity> query, IEnumerable<string> prefetch)
        {

            if (prefetch == null || prefetch.Count() == 0) return query;
            else
            {
                // if (prefetch.Contains(It<ModuleEntity>.NameOf(s => s.NetworkConfigurations)))
                // {
                //     query = query.Include(x => x.NetworkConfigurations);
                // }

                return query;
            }
        }

        private IQueryable<NetworkConfigurationEntity> ApplyFilter(IQueryable<NetworkConfigurationEntity> query, NetworkConfigurationFilter filter)
        {

            if (filter == null) return query;
            else
            {
                if (filter.Ids != null && filter.Ids.Any())
                {
                    query = query.Where(e => filter.Ids.Contains(e.Id));
                }
                if (!string.IsNullOrWhiteSpace(filter.SSID))
                {
                    var ssid = filter.SSID.Trim();
                    query = query.Where(e => filter.SSID.Contains(ssid));
                }

                return query;
            }        
        }

        private IQueryable<NetworkConfigurationEntity> ApplySearch(IQueryable<NetworkConfigurationEntity> query, NetworkConfigurationSearch search)
        {
            if (search == null) return query;
            else 
            {
                if (!string.IsNullOrWhiteSpace(search.SSID))
                {
                    var searchQuery = search.SSID.Trim();
                    query = query.Where(e => search.SSID.Contains(searchQuery));
                }

                return query;
            }
        }

        #endregion

    }
}