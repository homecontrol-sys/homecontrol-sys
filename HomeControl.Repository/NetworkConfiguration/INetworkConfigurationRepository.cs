using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface INetworkConfigurationRepository : IRepositoryBase
    {
        
        Task<IEnumerable<NetworkConfigurationEntity>> Find(
            IEnumerable<string> prefetch = null, NetworkConfigurationFilter filter = null, NetworkConfigurationSearch search = null);

    }

}