using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Repository
{

    public interface ISwitchStateRepository : IRepositoryBase
    {

        Task<IEnumerable<SwitchStateEntity>> Find(IEnumerable<string> prefetch = null, SwitchStateFilter filter = null, SwitchStateSearch search = null);

    }

}