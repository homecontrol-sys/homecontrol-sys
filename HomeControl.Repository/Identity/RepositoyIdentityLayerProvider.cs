using HomeControl.DAL;
using HomeControl.Repository.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace HomeControl.Repository
{

    public static class RepositoyIdentityLayerProvider
    {
        public static IServiceCollection ProvideRepositoryIdentityLayer(this IServiceCollection services)
        {

            #region Role
            
            services.AddScoped<IRoleRepository, RoleRepository<HomeControlDbContext>>();
            services.AddScoped<IRoleClaimRepository, RoleClaimRepository<HomeControlDbContext>>();
            
            #endregion

            #region User

            services.AddScoped<IUserRepository, UserRepository<HomeControlDbContext>>();
            services.AddScoped<IUserClaimRepository, UserClaimRepository<HomeControlDbContext>>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository<HomeControlDbContext>>();
            services.AddScoped<IUserTokenRepository, UserTokenRepository<HomeControlDbContext>>();

            #endregion

            return services;
        }
    }

}