using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity.Identity;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository.Identity
{
    public class UserClaimRepository<TDbContext> : RepositoryBase, IUserClaimRepository
    where TDbContext : DbContext
    {

        
        #region Constructor

        public UserClaimRepository(TDbContext dbContext) : base(dbContext)
        {
            
        }

        #endregion

        #region Methods

        // public async Task<UserClaimEntity> ReadUserClaim(Guid userId, Guid claimId)
        // {
        //     var entity = await base._table.SingleAsync(uc => uc.UserId == userId && uc.Id == claimId);

        //     return entity;
        // }

        // public async Task<IEnumerable<UserClaimEntity>> ReadUserClaims(Guid userId)
        // { 
        //     var entities = await base._table.Where(uc => uc.UserId == userId).ToListAsync();

        //     return entities;
        // }

        #endregion

    }

}
