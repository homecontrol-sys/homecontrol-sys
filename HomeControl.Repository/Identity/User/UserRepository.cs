using System;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using API.Project.Common.Modules.Security._Extension;
using HomeControl.DAL.Entity.Identity;
using HomeControl.Domain.Identity;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository.Identity
{
    public class UserRepository<TDbContext> : RepositoryBase, IUserRepository
    where TDbContext : DbContext
    {

        #region Constructor

        public UserRepository(TDbContext dbContext) : base(dbContext)
        {

        }

        #endregion

        #region Methods

        #region CRUD


        #endregion

        public async Task<UserEntity> FindByUserNameOrEmail(string userName, string eMail, string password)
        {
            UserEntity userEntity = null;
            if(password == string.Empty || password == null)
            {
                return userEntity;
            }
            else{
                var passwordHash = password.SHA512Hash();
                if (eMail != string.Empty && eMail != null)
                {
                    // userEntity = await base._table.SingleOrDefaultAsync(u => 
                    //     u.PasswordHash == passwordHash && u.Email == eMail
                    // );
                }
                else if (userName != string.Empty && userName != null)
                {
                    // userEntity = await base._table.SingleOrDefaultAsync(u => 
                    //     u.PasswordHash == passwordHash && u.UserName == userName
                    // );
                }

                return userEntity;
            }
        }

        public async Task<UserEntity> FindUser(UserFilter filter)
        {
            UserEntity userEntity = new UserEntity();
            if((filter.UserName != null || filter.EMail != null) && filter.Password != null)
            {
                var passwordHash = filter.Password.SHA512Hash();
                if (filter.EMail != string.Empty && filter.EMail != null)
                {
                    // userEntity = await base._table.SingleOrDefaultAsync(u => 
                    //     u.PasswordHash == passwordHash && u.Email == filter.EMail
                    // );
                }
                else if (filter.UserName != string.Empty && filter.UserName != null)
                {
                    // userEntity = await base._table.SingleOrDefaultAsync(u => 
                    //     u.PasswordHash == passwordHash && u.UserName == filter.UserName
                    // );
                }
            }
            else
            {
                userEntity = null;
            }
            return userEntity;
        }

    }

    #endregion
}