using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity.Identity;
using HomeControl.Domain.Identity;

namespace HomeControl.Repository
{

    public interface IUserRepository : IRepositoryBase
    {

        #region Methods

        Task<UserEntity> FindByUserNameOrEmail(string userName, string eMail, string password);
        Task<UserEntity> FindUser(UserFilter filter);

        #endregion

    }

}
