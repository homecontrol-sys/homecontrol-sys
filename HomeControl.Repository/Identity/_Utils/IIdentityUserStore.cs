using HomeControl.DAL.Entity.Identity;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.Repository.Identity._Utils
{
    public interface IIdentityUserStore : IUserStore<UserEntity>,
    IUserPasswordStore<UserEntity>, IUserEmailStore<UserEntity>,
    IUserSecurityStampStore<UserEntity>
    {

    }

}