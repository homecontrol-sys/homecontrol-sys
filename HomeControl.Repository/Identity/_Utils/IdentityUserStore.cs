using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using HomeControl.DAL;
using HomeControl.DAL.Entity.Identity;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.Repository.Identity._Utils
{
    public class IdentityUserStore : UserStoreBase<UserEntity, Guid, UserClaimEntity, UserLoginEntity, UserTokenEntity>,
    IIdentityUserStore
    {

        #region Fields

        private readonly IHomeControlDbContext _dbContext;
        private bool _disposed;

        #endregion

        #region Constructor

        public IdentityUserStore(IHomeControlDbContext dbContext, IdentityErrorDescriber describer) : base(describer)
        {
            _dbContext = dbContext;
        }

        #endregion

        #region Methods

        #region User

        public override IQueryable<UserEntity> Users => throw new NotImplementedException();

        public override Task<IdentityResult> CreateAsync(UserEntity user, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task<IdentityResult> UpdateAsync(UserEntity user, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task<IdentityResult> DeleteAsync(UserEntity user, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        protected override Task<UserEntity> FindUserAsync(Guid userId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task<UserEntity> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task<UserEntity> FindByIdAsync(string userId, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task<UserEntity> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Login

        public override Task AddLoginAsync(UserEntity user, UserLoginInfo login, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task<IList<UserLoginInfo>> GetLoginsAsync(UserEntity user, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task RemoveLoginAsync(UserEntity user, string loginProvider, string providerKey, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        protected override Task RemoveUserTokenAsync(UserTokenEntity token)
        {
            throw new NotImplementedException();
        }

        protected override Task<UserLoginEntity> FindUserLoginAsync(Guid userId, string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override Task<UserLoginEntity> FindUserLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Claim

        public override Task<IList<Claim>> GetClaimsAsync(UserEntity user, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
       
        public override Task AddClaimsAsync(UserEntity user, IEnumerable<Claim> claims, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task<IList<UserEntity>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task RemoveClaimsAsync(UserEntity user, IEnumerable<Claim> claims, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public override Task ReplaceClaimAsync(UserEntity user, Claim claim, Claim newClaim, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Token

        protected override Task AddUserTokenAsync(UserTokenEntity token)
        {
            throw new NotImplementedException();
        }

        protected override Task<UserTokenEntity> FindTokenAsync(UserEntity user, string loginProvider, string name, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion

        #endregion
        
    }

}