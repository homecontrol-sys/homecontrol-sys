using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity.Identity;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository.Identity
{
    public class UserTokenRepository<TDbContext> : RepositoryBase, IUserTokenRepository
    where TDbContext : DbContext
    {
     
        #region Constructor

        public UserTokenRepository(TDbContext dbContext) : base(dbContext)
        {
            
        }

        #endregion

    }

}
