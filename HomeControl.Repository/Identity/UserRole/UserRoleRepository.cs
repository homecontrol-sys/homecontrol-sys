using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity.Identity;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository.Identity
{
    public class UserRoleRepository<TDbContext> : RepositoryBase, IUserRoleRepository
    where TDbContext : DbContext
    {

        
        #region Constructor

        public UserRoleRepository(TDbContext dbContext) : base(dbContext)
        {
            
        }

        #endregion

        #region Methods

        // public async Task<IEnumerable<UserRoleEntity>> ReadUserRoles(Guid userId)
        // { 
        //     var entities = await _table.Where(e => e.UserId == userId).ToListAsync();

        //     return entities;
        // }

        #endregion

    }

}
