using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity.Identity;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository.Identity
{
    public class RoleClaimRepository<TDbContext> : RepositoryBase, IRoleClaimRepository
    where TDbContext : DbContext
    {
        
        #region Constructor

        public RoleClaimRepository(TDbContext dbContext) : base(dbContext)
        {
            
        }

        #endregion

    }

}
