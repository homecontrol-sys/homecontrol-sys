using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using HomeControl.DAL.Entity.Identity;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.Repository.Identity
{
    public class RoleRepository<TDbContext> : RepositoryBase, IRoleRepository
    where TDbContext : DbContext
    {

        
        #region Constructor

        public RoleRepository(TDbContext dbContext) : base(dbContext)
        {
            
        }

        #endregion

    }

}