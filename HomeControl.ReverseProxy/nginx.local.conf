## learn more about nginx reverse proxy configuration here:
## https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
worker_processes 1;
events { worker_connections 1024; }

http {
    sendfile on;
    large_client_header_buffers 4 32k;

    upstream api {
        server api:5000;
    }

    upstream web {
        server web:3000;
    }
    
    server {        
        listen 44395 ssl;
        ssl_certificate /etc/ssl/certs/api-local.homecontrol.com.crt;
        ssl_certificate_key /etc/ssl/private/api-local.homecontrol.com.key;
        
        server_name api-local.homecontrol.com;        
        
        location / {
            proxy_pass         http://api;
            proxy_redirect     off;
            proxy_http_version 1.1;
            proxy_cache_bypass $http_upgrade;
            proxy_set_header   Upgrade $http_upgrade;
            proxy_set_header   Connection keep-alive;
            proxy_set_header   Host $host:44395;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Proto $scheme;
            proxy_set_header   X-Forwarded-Host $server_name:44395;
            proxy_buffer_size           128k;
            proxy_buffers               4 256k;
            proxy_busy_buffers_size     256k;
        }
    }

    server {        
        listen 44395 ssl;
        ssl_certificate /etc/ssl/certs/www-local.homecontrol.com.crt;
        ssl_certificate_key /etc/ssl/private/www-local.homecontrol.com.key;
        
        server_name www-local.homecontrol.com;
        
        location / {
            proxy_pass         http://web;
            proxy_redirect     off;
            proxy_http_version 1.1;
            proxy_cache_bypass $http_upgrade;
            proxy_set_header   Upgrade $http_upgrade;
            proxy_set_header   Connection keep-alive;
            proxy_set_header   Host $host:44395;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Proto $scheme;
            proxy_set_header   X-Forwarded-Host $server_name:44395;
            proxy_buffer_size           128k;
            proxy_buffers               4 256k;
            proxy_busy_buffers_size     256k;
        }
    }
}