FROM nginx

COPY HomeControl.ReverseProxy/nginx.local.conf /etc/nginx/nginx.conf

#Trust certificates
COPY HomeControl.ReverseProxy/api/api-local.crt /etc/ssl/certs/api-local.homecontrol.com.crt
COPY HomeControl.ReverseProxy/api/api-local.key /etc/ssl/private/api-local.homecontrol.com.key

COPY HomeControl.ReverseProxy/www/www-local.crt /etc/ssl/certs/www-local.homecontrol.com.crt
COPY HomeControl.ReverseProxy/www/www-local.key /etc/ssl/private/www-local.homecontrol.com.key
