export const API_URL = process.env.REACT_APP_API_URL;
//export const API_URL = "http://api-local.homecontrol.com:44395";

export async function handleResponse(response: any) {
  if (response.ok) return response.json();
  if (response.status === 400) {
    const error = await response.text();
    throw new Error(error);
  }

  throw new Error("Network response not OK.");
}

export function handleError(error: any) {
  console.error("API call failed. " + error);
  throw error;
}
