import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import { Module } from "../../Models";
import { API_URL } from "../../Utils";

const MODULE_RESOURCE_API_URL = "/module";

export const getModulesAsync = createAsyncThunk<Module[]>("module/getModules", async () => {
  const response = await axios.get(`${API_URL}${MODULE_RESOURCE_API_URL}`);
  return response.data;
});
