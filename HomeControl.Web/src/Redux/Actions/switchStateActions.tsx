import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import { SwitchState } from "../../Models";
import { API_URL } from "../../Utils";

const ELEMENT_STATE_RESOURCE_API_URL = "/state-switch-state";

export const getSwitchStatesAsync = createAsyncThunk<SwitchState[]>(
  "switchStates/getSwitchStates",
  async () => {
    const response = await axios.get(`${API_URL}${ELEMENT_STATE_RESOURCE_API_URL}`);
    return response.data;
  }
);
