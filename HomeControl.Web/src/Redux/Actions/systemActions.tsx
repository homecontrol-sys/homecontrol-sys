import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import { ReadSystemRequestParams, System } from "../../Models";
import { API_URL } from "../../Utils";

const config = {
  headers: { "Access-Control-Allow-Origin": "*" },
};
const SYSTEM_RESOURCE_API_URL = "/system";

export const getSystemStateAsync = createAsyncThunk<System[]>(
  "systems/getSystemState",
  async () => {
    const response = await axios.get(`${API_URL}${SYSTEM_RESOURCE_API_URL}/state`);
    return response.data;
  }
);

export const getSystemsAsync = createAsyncThunk<System[]>("systems/getSystems", async () => {
  const response = await axios.get(`${API_URL}${SYSTEM_RESOURCE_API_URL}`);
  return response.data;
});

export const getMainSystemAsync = createAsyncThunk<System[]>("systems/getMainSystem", async () => {
  const readSystemRequestParams: ReadSystemRequestParams = { filter: { isMain: true } };

  const response = await axios.get(`${API_URL}${SYSTEM_RESOURCE_API_URL}`, {
    params: { ...readSystemRequestParams },
  });
  return response.data;
});

export const isMainSystemConfiguredAsync = createAsyncThunk<boolean[]>(
  "systems/isMainSystemConfigured",
  async () => {
    const response = await axios.get(`${API_URL}${SYSTEM_RESOURCE_API_URL}/is-main-configured`);
    return response.data;
  }
);

export const getOtherSystemsAsync = createAsyncThunk<System[]>(
  "systems/getOtherSystems",
  async () => {
    const response = await axios.get(`${API_URL}${SYSTEM_RESOURCE_API_URL}`);
    return response.data;
  }
);
