import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import { Element, SwitchElementStateRequest } from "../../Models";
import { API_URL } from "../../Utils";

const ELEMENT_RESOURCE_API_URL = "/element";

export const getElementsAsync = createAsyncThunk<Element[]>("elements/getElements", async () => {
  const response = await axios.get(`${API_URL}${ELEMENT_RESOURCE_API_URL}`);
  return response.data;
});

export const triggerSwitchChangeAsync = createAsyncThunk(
  "command/triggerSwitchChange",
  async ({ elementId, newElementSwitchStateId }: SwitchElementStateRequest) => {
    const request = {
      elementId: elementId,
      newElementSwitchStateId: newElementSwitchStateId,
    };

    const response = await axios.post(`${API_URL}${ELEMENT_RESOURCE_API_URL}/switch-state`, {
      ...request,
    });
    return response.data;
  }
);
