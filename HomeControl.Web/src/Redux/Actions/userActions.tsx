import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import { User } from "../../Models";
import { API_URL } from "../../Utils";

const USER_RESOURCE_API_URL = "/user";

export const getUsersAsync = createAsyncThunk<User[]>(
  "users/getUsers",
  async () => {
    const response = await axios.get(`${API_URL}${USER_RESOURCE_API_URL}`);
    return response.data;
  }
);
