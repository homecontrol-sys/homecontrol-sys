import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import { Switch } from "../../Models";
import { API_URL } from "../../Utils";

const ELEMENT_STATE_SWITCH_RESOURCE_API_URL = "/switch";

export const getSwitchesAsync = createAsyncThunk<Switch[]>("switches/getSwitches", async () => {
  const response = await axios.get(`${API_URL}${ELEMENT_STATE_SWITCH_RESOURCE_API_URL}`);
  return response.data;
});
