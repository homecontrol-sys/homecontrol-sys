import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

import { Device } from "../../Models";
import { API_URL } from "../../Utils";

const DEVICE_RESOURCE_API_URL = "/device";

export const getDevicesAsync = createAsyncThunk<Device[]>("devices/getDevices", async () => {
  const response = await axios.get(`${API_URL}${DEVICE_RESOURCE_API_URL}`);
  return response.data;
});
