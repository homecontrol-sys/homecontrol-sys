import { configureStore } from "@reduxjs/toolkit";

import {
  appSlice,
  devicesSlice,
  elementsSlice,
  switchStatesSlice,
  switchesSlice,
  modulesSlice,
  systemsSlice,
  usersSlice,
} from "./Slices";

export const store = configureStore({
  reducer: {
    app: appSlice.reducer,
    devices: devicesSlice.reducer,
    elements: elementsSlice.reducer,
    switchStates: switchStatesSlice.reducer,
    switches: switchesSlice.reducer,
    modules: modulesSlice.reducer,
    users: usersSlice.reducer,
    systems: systemsSlice.reducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
