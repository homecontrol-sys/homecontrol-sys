import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { Element } from "../../Models";
import { getSwitchStatesAsync } from "../Actions";

export interface SwitchStatesState {
  isLoading: boolean;
  switchStates: Element[];
}

const initialState: SwitchStatesState = {
  isLoading: false,
  switchStates: [],
};

export const switchStatesSlice = createSlice({
  name: "switchStates",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [getSwitchStatesAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getSwitchStatesAsync.fulfilled.type]: (state, action: PayloadAction<Element[]>) => {
      state.isLoading = false;
      state.switchStates = action.payload;
    },
    [getSwitchStatesAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
  },
});
