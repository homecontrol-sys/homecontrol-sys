import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { Device } from "../../Models";
import { getDevicesAsync } from "../Actions";

export interface DevicesState {
  isLoading: boolean;
  devices: Device[];
}

const initialState: DevicesState = {
  isLoading: false,
  devices: [],
};

export const devicesSlice = createSlice({
  name: "devices",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [getDevicesAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getDevicesAsync.fulfilled.type]: (state, action: PayloadAction<Device[]>) => {
      state.isLoading = false;
      state.devices = action.payload;
    },
    [getDevicesAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
  },
});
