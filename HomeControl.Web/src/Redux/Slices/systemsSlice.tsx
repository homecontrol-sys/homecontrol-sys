import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { System } from "../../Models";
import {
  getMainSystemAsync,
  isMainSystemConfiguredAsync,
  getOtherSystemsAsync,
  getSystemStateAsync,
  getSystemsAsync,
} from "../Actions";

export interface SystemsState {
  isLoading: boolean;
  systems: System[];
  mainSystem: System | null;
  isSystemMainConfigured: boolean | null;
  otherSystems: System[];
}

const initialState: SystemsState = {
  isLoading: false,
  systems: [],
  mainSystem: null,
  isSystemMainConfigured: null,
  otherSystems: [],
};

export const systemsSlice = createSlice({
  name: "system",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    //all systems
    [getSystemStateAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getSystemStateAsync.fulfilled.type]: (state, action: PayloadAction<System[]>) => {
      state.isLoading = false;
      state.systems = action.payload;
    },
    [getSystemStateAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
    [getSystemsAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getSystemsAsync.fulfilled.type]: (state, action: PayloadAction<System[]>) => {
      state.isLoading = false;
      state.systems = action.payload;
    },
    [getSystemsAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
    //main system
    [getMainSystemAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getMainSystemAsync.fulfilled.type]: (state, action: PayloadAction<System>) => {
      state.isLoading = false;
      state.mainSystem = action.payload[0];
    },
    [getMainSystemAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
    [isMainSystemConfiguredAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [isMainSystemConfiguredAsync.fulfilled.type]: (state, action: PayloadAction<boolean>) => {
      state.isLoading = false;
      state.isSystemMainConfigured = action.payload;
    },
    [isMainSystemConfiguredAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
    //other systems
    [getOtherSystemsAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getOtherSystemsAsync.fulfilled.type]: (state, action: PayloadAction<System[]>) => {
      state.isLoading = false;
      state.systems = action.payload;
    },
    [getOtherSystemsAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
  },
});
