// DUCKS pattern
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ThemeMode } from "../../Models";

export interface AppState {
  showDialog: boolean;
  showSpinner: boolean;
  themeMode: ThemeMode;
}

const initialState: AppState = {
  showDialog: false,
  showSpinner: false,
  themeMode: ThemeMode.DARK,
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    setShowDialog(state, action: PayloadAction<boolean>) {
      state.showDialog = action.payload;
    },
    setShowSpinner(state, action: PayloadAction<boolean>) {
      state.showSpinner = action.payload;
    },
  },
});

export const { setShowDialog, setShowSpinner } = appSlice.actions;
