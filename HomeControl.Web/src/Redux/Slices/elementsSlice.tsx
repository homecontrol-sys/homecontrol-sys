import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { Element } from "../../Models";
import { getElementsAsync } from "../Actions";

export interface ElementsState {
  isLoading: boolean;
  elements: Element[];
}

const initialState: ElementsState = {
  isLoading: false,
  elements: [],
};

export const elementsSlice = createSlice({
  name: "elements",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [getElementsAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getElementsAsync.fulfilled.type]: (state, action: PayloadAction<Element[]>) => {
      state.isLoading = false;
      state.elements = action.payload;
    },
    [getElementsAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
  },
});
