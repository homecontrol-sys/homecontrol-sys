import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { StateSwitch } from "../../Models";
import { getSwitchesAsync } from "../Actions";

export interface SwitchesState {
  isLoading: boolean;
  switches: StateSwitch[];
}

const initialState: SwitchesState = {
  isLoading: false,
  switches: [],
};

export const switchesSlice = createSlice({
  name: "switches",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [getSwitchesAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getSwitchesAsync.fulfilled.type]: (state, action: PayloadAction<StateSwitch[]>) => {
      state.isLoading = false;
      state.switches = action.payload;
    },
    [getSwitchesAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
  },
});
