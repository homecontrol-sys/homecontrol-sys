import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { Module } from "../../Models";
import { getModulesAsync } from "../Actions";

export interface ModulesState {
  isLoading: boolean;
  modules: Module[];
}

const initialState: ModulesState = {
  isLoading: false,
  modules: [],
};

export const modulesSlice = createSlice({
  name: "modules",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [getModulesAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getModulesAsync.fulfilled.type]: (state, action: PayloadAction<Module[]>) => {
      state.isLoading = false;
      state.modules = action.payload;
    },
    [getModulesAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
  },
});
