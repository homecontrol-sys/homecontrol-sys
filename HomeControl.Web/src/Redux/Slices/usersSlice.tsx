import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { User } from "../../Models";
import { getUsersAsync } from "../Actions";

export interface UsersState {
  isLoading: boolean;
  users: User[];
}

const initialState: UsersState = {
  isLoading: false,
  users: [],
};

export const usersSlice = createSlice({
  name: "users",
  initialState: initialState,
  reducers: {},
  extraReducers: {
    [getUsersAsync.pending.type]: (state) => {
      state.isLoading = true;
    },
    [getUsersAsync.fulfilled.type]: (state, action: PayloadAction<User[]>) => {
      state.isLoading = false;
      state.users = action.payload;
    },
    [getUsersAsync.rejected.type]: (state) => {
      state.isLoading = false;
    },
  },
});
