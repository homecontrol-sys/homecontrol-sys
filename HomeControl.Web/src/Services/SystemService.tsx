import { ConfigureMainSystemRequest, System } from "../Models/RESTModels/SystemModels";
import { API_URL, handleError, handleResponse } from "../Utils/APIUtils";
import { GUID } from "../Utils/TypeUtils";

const baseUrl = API_URL + "/system";

/* #region  CRUD */
function getSystems() {
  return fetch(baseUrl, {
    method: "GET",
  })
    .then(handleResponse)
    .catch(handleError);
}

function getSystem(systemId: GUID) {
  return fetch(baseUrl + systemId)
    .then(handleResponse)
    .catch(handleError);
}

function saveSystem(system: System) {
  return fetch(baseUrl + (system.id || ""), {
    method: system.id ? "PUT" : "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(system),
  })
    .then(handleResponse)
    .catch(handleError);
}

function deleteSystem(systemId: GUID) {
  return fetch(baseUrl + systemId, { method: "DELETE" })
    .then(handleResponse)
    .catch(handleError);
}

/* #endregion */

async function configureMainSystem(systemConfig: ConfigureMainSystemRequest) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(systemConfig),
  };
  const response = await fetch(baseUrl + "/configure-main", requestOptions);
  const data = await response.json();
  return data;
}

export { getSystems, getSystem, saveSystem, deleteSystem, configureMainSystem };
