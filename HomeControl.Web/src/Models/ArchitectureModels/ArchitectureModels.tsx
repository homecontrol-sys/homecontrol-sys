import { GUID } from "../../Utils/TypeUtils";

export interface IdentificationModel {
  id: GUID;
  name: string;
  description: string;
  sortOrder: number;
}

export interface LookupModel extends IdentificationModel {
  abrv: string;
  name: string;
  description: string;
  sortOrder: number;
}
