import { GUID } from "../../Utils/TypeUtils";

export type Device = {
  id: GUID;
  name: string;
  systemId: GUID;
};
