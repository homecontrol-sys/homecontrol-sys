export type PagingParams = {
  PageNumber: number;
  PageSize: number;
};

export interface APIResourceParams extends PagingParams {
  Prefetch: string[];
  Filter: object;
  SearchBy: object;
}
