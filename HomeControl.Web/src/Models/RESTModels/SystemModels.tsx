import { GUID } from "../../Utils/TypeUtils";
import { Element } from "./ElementModels";

export type System = {
  id: GUID;
  categoryId: GUID;
  name: string;
  description: string;
  isMain: boolean;
  isConfigured: boolean;
  elements: Element[];
};

export type SystemFilter = {
  ids?: GUID[];
  isMain?: boolean;
  name?: string;
};

export type ReadSystemRequestParams = {
  filter?: SystemFilter;
};

export type ConfigureMainSystemRequest = {
  name: string;
  SSID: string;
  password?: string;
};
