import { GUID } from "../../Utils/TypeUtils";

export type Module = {
  id: GUID;
  alias: string;
  name: string;
};
