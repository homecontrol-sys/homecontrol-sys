export type AuthenticationModel = {
  usernameOrEmail?: string;
  password?: string;
};
