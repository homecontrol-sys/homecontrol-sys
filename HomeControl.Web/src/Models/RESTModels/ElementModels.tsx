import { GUID } from "../../Utils/TypeUtils";
import { Device, System, Switch, SwitchState } from "../../Models";

export interface Element {
  id: GUID;
  name: string;
  alias: string;
  deviceId: GUID;
  device: Device;
  systemId: GUID;
  system: System;
  switchId: GUID;
  switch: Switch;
  switchStateId: GUID;
  switchState: SwitchState;
}

export interface SwitchElementStateRequest {
  elementId: GUID;
  newElementSwitchStateId: GUID;
}
