import { GUID } from "../../Utils/TypeUtils";

export type User = {
  id: GUID;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  isAuthenticated: boolean;
  emailConfirmed: boolean;
};
