import { LookupModel, SwitchState } from "../../Models";

export interface Switch extends LookupModel {
  switchStates: SwitchState[];
}
