import { SvgIconProps } from "@mui/material";

export enum DialogType {
  BASE = "base",
  SUBMIT = "submit",
  SUBMIT_FORM = "submit-form",
}

export interface BaseDialogTypeProps {
  icon?: React.ReactElement<SvgIconProps>;
  title?: string;
  content: JSX.Element | JSX.Element;
  actions?: JSX.Element | JSX.Element;
  onClose?: () => void;
}

export interface SubmitDialogTypeProps extends BaseDialogTypeProps {
  content: JSX.Element | JSX.Element;
  actions: JSX.Element | JSX.Element;
  onSubmit?: () => void;
}

export interface SubmitFormDialogTypeProps extends BaseDialogTypeProps {
  content: JSX.Element | JSX.Element;
  onSubmit?: () => void;
}
