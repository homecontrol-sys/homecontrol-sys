export enum RouteAccessType {
  PUBLIC = "public",
  PRIVATE = "private",
}

export type RoutePage = {
  access: RouteAccessType;
  url: string;
  name: string;
  title: string;
};
