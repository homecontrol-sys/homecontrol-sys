import { FC } from "react";
import { CircularProgress } from "@mui/material";

import { useAppSelector } from "../../Redux";
import { Overlay } from "..";

interface SpinnerProps {}

export const Spinner: FC<SpinnerProps> = (props: SpinnerProps) => {
  const { showSpinner } = useAppSelector((state) => state.app);

  return (
    <Overlay isOpen={showSpinner}>
      <CircularProgress />
    </Overlay>
  );
};
