import { FC } from "react";
import { PersonAddAltRounded } from "@mui/icons-material";
import { Dialog, UserRegistrationForm } from "../..";
import { SubmitFormDialogTypeProps } from "../../../Models";

interface UserRegistrationDialogProps {}

export const UserRegistrationDialog: FC<UserRegistrationDialogProps> = (props: UserRegistrationDialogProps) => {
  const dialogProps: SubmitFormDialogTypeProps = {
    title: "Registration",
    icon: <PersonAddAltRounded color="primary" />,
    content: <UserRegistrationForm formName="user-registration-form" />,
  };

  return <Dialog dialogProps={dialogProps} />;
};
