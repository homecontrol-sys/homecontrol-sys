import { FC } from "react";
import { Button, DialogActions as MuiDialogActions } from "@mui/material";
import { Check, Close } from "@mui/icons-material";

import { DialogType } from "../../../Models";

interface AppSubmitFormDialogActionsProps {
  onCancel: () => void;
  onCancelCallback?: () => void;
  onSubmit: () => void;
  onSubmitCallback?: () => void;
}

const AppSubmitFormDialogActions: FC<AppSubmitFormDialogActionsProps> = (props: AppSubmitFormDialogActionsProps) => {
  const { onCancel, onSubmit } = props;

  return (
    <MuiDialogActions>
      <Button autoFocus sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }} variant="outlined" color="error" startIcon={<Close />} onClick={onCancel}>
        Cancel
      </Button>
      <Button
        autoFocus
        sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }}
        variant="outlined"
        color="success"
        startIcon={<Check />}
        onClick={onSubmit}
      >
        Submit
      </Button>
    </MuiDialogActions>
  );
};

interface DialogActionsProps {
  dialogType: DialogType;
  onCancel?: () => void;
  onSubmit?: () => void;
}

export const DialogActions: FC<DialogActionsProps> = (props: DialogActionsProps) => {
  const { dialogType, onCancel, onSubmit } = props;
  let DialogActions;

  switch (dialogType) {
    case DialogType.BASE:
      DialogActions = null;
      break;
    case DialogType.SUBMIT_FORM:
      // DialogActions =
      //   onCancel && onSubmit ? (
      //     <AppSubmitFormDialogActions onCancel={onCancel} onSubmit={onSubmit} />
      //   ) : null;
      DialogActions = null;
      break;
    default:
      DialogActions = null;
  }

  return DialogActions;
};
