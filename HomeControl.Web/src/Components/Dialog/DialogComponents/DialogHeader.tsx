import { FC } from "react";
import { Grid, IconButton, SvgIconProps, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

import "./DialogHeader.scss";

interface DialogTitleProps {
  title: string;
}

export const DialogTitle: FC<DialogTitleProps> = (props: DialogTitleProps) => {
  const { title } = props;

  return (
    <Typography component="span" variant="h6" color="primary">
      {title}
    </Typography>
  );
};

interface DialogHeaderProps {
  id: string;
  title?: string;
  icon?: React.ReactElement<SvgIconProps>;
  onClose: () => void;
}

export const DialogHeader: FC<DialogHeaderProps> = (props: DialogHeaderProps) => {
  const { icon, title, onClose } = props;

  return (
    <Grid container className="header-section dialog-header">
      <Grid item xl={2} container sx={{ justifyContent: "center", alignItems: "center" }}>
        {icon ? icon : null}
      </Grid>
      <Grid item xl={8} container sx={{ justifyContent: "center", alignItems: "center" }}>
        {title ? <DialogTitle title={title} /> : null}
      </Grid>
      <Grid item xl={2} container sx={{ justifyContent: "center", alignItems: "center" }}>
        {onClose ? (
          <IconButton aria-label="close" onClick={onClose} color="error">
            <CloseIcon />
          </IconButton>
        ) : null}
      </Grid>
    </Grid>
  );
};
