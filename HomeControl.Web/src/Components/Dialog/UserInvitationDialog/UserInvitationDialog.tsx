import { FC } from "react";
import { PersonAddAltRounded } from "@mui/icons-material";
import { Dialog, UserInvitationForm } from "../..";
import { SubmitFormDialogTypeProps } from "../../../Models";

interface UserInvitationDialogProps {}

export const UserInvitationDialog: FC<UserInvitationDialogProps> = (props: UserInvitationDialogProps) => {
  const dialogProps: SubmitFormDialogTypeProps = {
    title: "Invite User",
    icon: <PersonAddAltRounded color="primary" />,
    content: <UserInvitationForm />,
  };

  return <Dialog dialogProps={dialogProps} />;
};
