import { FC } from "react";
import { useTheme } from "@material-ui/core";
import { Divider, useMediaQuery } from "@mui/material";

import { useAppDispatch, useAppSelector, setShowDialog } from "../../Redux";
import { BaseDialogTypeProps, SubmitDialogTypeProps, SubmitFormDialogTypeProps } from "../../Models";
import { DialogHeader } from ".";
import { StyledDialog } from "./DialogStyles";

type DialogTypeProps = BaseDialogTypeProps | SubmitDialogTypeProps | SubmitFormDialogTypeProps;

interface DialogProps {
  dialogProps: DialogTypeProps;
}

// const determDialogType = (dialogProps: DialogTypeProps) => {
//   switch (true) {
//     case dialogProps instanceof BaseDialogTypeProps:
//       return DialogType.BASE;
//     case dialogProps instanceof SubmitDialogTypeProps:
//       return DialogType.SUBMIT;
//     case dialogProps instanceof SubmitFormDialogTypeProps:
//       return DialogType.SUBMIT_FORM;
//     default:
//       return DialogType.BASE;
//   }
// };

export const Dialog: FC<DialogProps> = (props: DialogProps) => {
  const { showDialog } = useAppSelector((state) => state.app);
  const dispatch = useAppDispatch();
  const {
    dialogProps,
    dialogProps: { icon, title, content, actions, onClose },
  } = props;

  // const dialogType = determDialogType(dialogProps);

  const handleSubmit = () => {
    dispatch(setShowDialog(false));
  };
  const handleClose = () => {
    dispatch(setShowDialog(false));
  };

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <StyledDialog
      className="app-dialog"
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      fullScreen={fullScreen}
      open={showDialog}
    >
      <DialogHeader id="customized-dialog-title" icon={icon} title={title} onClose={handleClose} />
      <Divider />
      {content ? <div>{content}</div> : null}
      {/* {content ? <DialogContent>{content}</DialogContent> : null} */}
      {/* <DialogActions>
        {actions ? (
          <DialogActions>{actions}</DialogActions>
        ) : (
          <DialogActions
            onCancel={handleClose}
            onSubmit={handleClose}
            dialogType={DialogType.SUBMIT_FORM}
          />
        )}
      </DialogActions> */}
    </StyledDialog>
  );
};
