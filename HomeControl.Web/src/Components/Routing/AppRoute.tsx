import { FC, useContext } from "react";
import { Route, Redirect, RouteProps, useLocation } from "react-router-dom";
import { RoutingContext } from "../Provider";

interface AppRouteProps extends RouteProps {
  isMainSystemConfigured: boolean;
}

export const AppRoute: FC<AppRouteProps> = ({
  isMainSystemConfigured,
  component: Component,
  ...routeProps
}: AppRouteProps) => {
  const { configPage, systemStatePage } = useContext(RoutingContext);
  const curentLocation = useLocation();
  const isConfigPage = curentLocation.pathname === configPage.url;

  if (!isMainSystemConfigured && !isConfigPage) {
    return <Redirect to={configPage.url} />;
  } else if (isMainSystemConfigured && isConfigPage) {
    return <Redirect to={systemStatePage.url} />;
  }

  return <Route {...routeProps} render={(props) => <Component {...props} />} />;
};
