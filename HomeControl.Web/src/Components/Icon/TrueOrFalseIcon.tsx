import { Close, Done } from "@mui/icons-material";

interface TrueOrFalseIconProps {
  value: boolean;
}

export const TrueOrFalseIcon: React.FC<TrueOrFalseIconProps> = (
  props: TrueOrFalseIconProps
) => {
  const { value } = props;
  return value ? <Done color="success" /> : <Close color="error" />;
};
