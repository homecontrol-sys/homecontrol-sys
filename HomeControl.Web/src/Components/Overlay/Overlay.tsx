import { FC, useState } from "react";
import Backdrop from "@mui/material/Backdrop";

interface AppOverlayProps {
  isOpen: boolean;
  children: JSX.Element | JSX.Element[];
}

export const AppOverlay: FC<AppOverlayProps> = (props: AppOverlayProps) => {
  const { isOpen, children } = props;

  return (
    <div className="app-overlay">
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isOpen}
      >
        {children}
      </Backdrop>
    </div>
  );
};
