import { FC } from "react";
import { Card, CardHeader, CardContent, CardActions } from "@mui/material";
import MemoryIcon from "@mui/icons-material/Memory";

import { System } from "../../Models";

export interface CoreSystemCardProps {
  coreSystem: System | null;
}

export const CoreSystemCard: FC<CoreSystemCardProps> = (props: CoreSystemCardProps) => {
  const { coreSystem } = props;

  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardHeader
        avatar={<MemoryIcon fontSize="large" />}
        title={coreSystem && coreSystem.name}
        subheader="Status"
      />
      <CardContent></CardContent>
      <CardActions disableSpacing></CardActions>
    </Card>
  );
};
