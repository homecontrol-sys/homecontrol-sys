import * as React from "react";
import { Grid } from "@mui/material";

import { SystemCard } from ".";
import { System } from "../../Models";
import "./SystemsCardList.scss";

export interface SystemsCardListProps {
  otherSystems: System[];
}

export const SystemsCardList: React.FC<SystemsCardListProps> = (props: SystemsCardListProps) => {
  const { otherSystems } = props;

  return (
    <Grid container className="systems-list">
      {otherSystems &&
        otherSystems.map((system) => {
          return <SystemCard key={system.id} system={system} />;
        })}
    </Grid>
  );
};
