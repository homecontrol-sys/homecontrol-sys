import { FC } from "react";
import { Card, CardHeader, CardContent } from "@mui/material";
import EmojiObjectsTwoToneIcon from "@mui/icons-material/EmojiObjectsTwoTone";

import { Element } from "../../Models";
import { ElementSwitch } from "../../Components";

export interface ElementCardProps {
  element: Element | null;
}

export const ElementCard: FC<ElementCardProps> = (props: ElementCardProps) => {
  const { element } = props;

  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardHeader title={element.name} subheader="Status: " />
      <CardContent>
        <EmojiObjectsTwoToneIcon fontSize="large" />
        <ElementSwitch element={element} />
      </CardContent>
    </Card>
  );
};
