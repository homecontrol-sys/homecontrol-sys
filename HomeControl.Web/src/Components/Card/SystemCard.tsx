import { FC } from "react";
import { Avatar, Card, CardHeader, CardContent, CardActions } from "@mui/material";

import { System } from "../../Models";
import { ElementCard } from "../../Components";

interface SystemCardProps {
  system: System;
}

export const SystemCard: FC<SystemCardProps> = (props) => {
  const { system } = props;

  return (
    <Card sx={{ maxWidth: 345, minWidth: 250 }}>
      <CardHeader
        avatar={<Avatar aria-label="recipe">R</Avatar>}
        title={system.name}
        subheader="Status"
      />
      <CardContent>
        {system.elements.map((element) => {
          return <ElementCard key={element.id} element={element} />;
        })}
      </CardContent>
      <CardActions disableSpacing></CardActions>
    </Card>
  );
};
