//import { changeThemeMode } from "../appContextSlice";
import "./ThemeToggleSwitch.scss";

export interface ThemeToggleSwitchProps {}

export const ThemeToggleSwitch: React.FC<ThemeToggleSwitchProps> = () => {
  // const themeMode = useAppSelector((state) => state.appContext.themeMode);
  // const dispatch = useAppDispatch();

  // const handleToggle = () => {
  //   const newThemeMode =
  //     themeMode === ThemeMode.LIGHT ? ThemeMode.DARK : ThemeMode.LIGHT;
  //   dispatch(changeThemeMode(newThemeMode));
  // };

  return (
    <label className="switch" htmlFor="checkbox">
      <input
        type="checkbox"
        id="checkbox"
        // checked={themeMode === ThemeMode.DARK}
        // onChange={handleToggle}
      />
      <div className="slider round" />
    </label>
  );
};
