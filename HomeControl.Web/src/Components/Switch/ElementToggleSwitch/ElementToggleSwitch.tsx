import { FC, useState } from "react";
import { Stack, Typography, Switch } from "@mui/material";

import { SwitchState } from "../../../Models";
import { triggerSwitchChangeAsync, useAppDispatch } from "../../../Redux";
import { GUID } from "../../../Utils";

interface ElementToggleSwitchProps {
  elementId: GUID;
  startValue: SwitchState;
  endValue: SwitchState;
  currentValue: SwitchState;
}

export const ElementToggleSwitch: FC<ElementToggleSwitchProps> = (
  props: ElementToggleSwitchProps
) => {
  const { elementId, startValue, endValue, currentValue } = props;
  const [checked, setChecked] = useState(currentValue.id === endValue.id);
  const dispatch = useAppDispatch();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
    const newElementSwitchStateId = event.target.checked ? endValue.id : startValue.id;
    dispatch(triggerSwitchChangeAsync({ elementId, newElementSwitchStateId }));
  };

  return (
    <Stack
      className="light-switch"
      direction="row"
      spacing={1}
      justifyContent="center"
      alignItems="center"
    >
      <Typography color="">{startValue.name}</Typography>
      <Switch checked={checked} onChange={handleChange} size="medium" />
      <Typography>{endValue.name}</Typography>
    </Stack>
  );
};
