import { FC } from "react";
import { Stack } from "@mui/material";

import { Element } from "../../../Models";
import { ElementToggleSwitch } from "../../../Components";

interface ElementSwitchProps {
  element: Element;
}

export const ElementSwitch: FC<ElementSwitchProps> = (props: ElementSwitchProps) => {
  const { element } = props;
  console.log(element.switch.switchStates);

  let SwitchComponent =
    element.switch.name === "Toggle Switch" ? (
      <ElementToggleSwitch
        elementId={element.id}
        startValue={element.switch.switchStates[0]}
        endValue={element.switch.switchStates[1]}
        currentValue={element.switchState}
      />
    ) : null;

  return (
    <Stack
      className="element-switch"
      direction="row"
      spacing={1}
      justifyContent="center"
      alignItems="center"
    >
      {SwitchComponent}
    </Stack>
  );
};
