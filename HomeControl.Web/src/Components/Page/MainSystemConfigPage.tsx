import { MainSystemConfigForm } from "..";
import { MainLayout } from "..";

export const MainSystemConfigPage = () => {
  return (
    <div className="screen__home-control-configuration">
      <MainLayout>
        <MainSystemConfigForm formName="main-system-configuration-form" />
      </MainLayout>
    </div>
  );
};
