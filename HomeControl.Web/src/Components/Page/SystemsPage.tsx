import { MainMenu, DashboardLayout } from "..";
import { SystemsScreen } from "../../Containers";

export const SystemsPage = () => {
  return (
    <DashboardLayout
      title="Systems"
      menu={<MainMenu />}
      content={<SystemsScreen name="systems" />}
    />
  );
};
