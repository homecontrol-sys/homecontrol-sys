import { MainMenu, DashboardLayout } from "..";
import { UsersScreen } from "../../Containers";

export const UsersPage = () => {
  return (
    <DashboardLayout title="Users" menu={<MainMenu />} content={<UsersScreen name="users" />} />
  );
};
