import { MainMenu } from "..";
import { DevicesScreen } from "../../Containers";
import { DashboardLayout } from "..";

export const DevicesPage = () => {
  return (
    <DashboardLayout
      title="Devices"
      menu={<MainMenu />}
      content={<DevicesScreen name="devices" />}
    />
  );
};
