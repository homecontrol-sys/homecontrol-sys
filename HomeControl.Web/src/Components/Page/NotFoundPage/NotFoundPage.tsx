import { Page } from "../../../Components";
import "./NotFoundPage.scss";

export const NotFoundPage = () => {
  return (
    <Page name="not-found">
      <h1>404</h1>
      <h2>Page Not Found</h2>
    </Page>
  );
};
