import { FC } from "react";

import { MainLayout } from "../../Components";

interface PageProps {
  children: JSX.Element | JSX.Element[];
  name: string;
}

export const Page: FC<PageProps> = (props: PageProps) => {
  const { children, name } = props;

  return (
    <div className={`page ${name}-page`}>
      <MainLayout>{children}</MainLayout>
      {/* {layout === LayoutType.MAIN ? (
        <MainLayout>{children}</MainLayout>
      ) : (
        <DashboardLayout content={children} />
      )} */}
    </div>
  );
};

export default Page;
