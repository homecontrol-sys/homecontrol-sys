import { MainMenu, DashboardLayout } from "..";
import { SwitchStatesScreen } from "../../Containers";

export const SwitchStatesPage = () => {
  return (
    <DashboardLayout
      title="State Switch States"
      menu={<MainMenu />}
      content={<SwitchStatesScreen name="element-states" />}
    />
  );
};
