import { MainMenu, DashboardLayout } from "..";
import { SystemStateScreen } from "../../Containers";

export const SystemStatePage = () => {
  return (
    <DashboardLayout
      title="System state"
      menu={<MainMenu />}
      content={<SystemStateScreen name="system-state" />}
    />
  );
};
