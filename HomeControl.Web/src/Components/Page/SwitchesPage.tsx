import { MainMenu, DashboardLayout } from "..";
import { SwitchesScreen } from "../../Containers";

export const SwitchesPage = () => {
  return (
    <DashboardLayout
      title="State Switches"
      menu={<MainMenu />}
      content={<SwitchesScreen name="state-switches" />}
    />
  );
};
