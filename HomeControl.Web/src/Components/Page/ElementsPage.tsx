import { MainMenu, DashboardLayout } from "../../Components";
import { ElementsScreen } from "../../Containers";

export const ElementsPage = () => {
  return (
    <DashboardLayout
      title="Elemments"
      menu={<MainMenu />}
      content={<ElementsScreen name="elements" />}
    />
  );
};
