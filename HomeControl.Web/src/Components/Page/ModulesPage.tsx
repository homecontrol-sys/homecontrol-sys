import { MainMenu, DashboardLayout } from "..";
import { ModulesScreen } from "../../Containers";

export const ModulesPage = () => {
  return (
    <DashboardLayout
      title="Modules"
      menu={<MainMenu />}
      content={<ModulesScreen name="modules" />}
    />
  );
};
