import { Paper, Typography, Container } from "@mui/material";

import { LoginForm, Page } from "../../../Components";
import hcImage from "../../../Assets/Img/hc_icon.png";
import "./WelcomePage.scss";

export const WelcomePage = () => {
  return (
    <Page name="welcome">
      <Container className="page__welcome__message-container">
        <Paper className="page__welcome__message-container__content-container" elevation={6}>
          <Typography
            variant="h4"
            gutterBottom
            component="div"
            className="page__welcome__message-container__content-container__title"
          >
            Welcome to HomeControl App
          </Typography>
          <img
            className="page__welcome__message-container__content-container__img-container"
            src={hcImage}
            alt="homecontrol_image"
          />
          <Typography
            variant="body1"
            gutterBottom
            component="div"
            className="page__welcome__message-container__content-container__message"
          >
            Log in to configure Your HomeControl System!
          </Typography>
        </Paper>
      </Container>
      <Container className="page__welcome__login-form-container">
        <LoginForm />
      </Container>
    </Page>
  );
};
