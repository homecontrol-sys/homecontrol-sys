import * as React from "react";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";

const Navigation = (props: any) => {
  const [value, setValue] = React.useState(0);
  const { items } = props;

  const Links = (props: any) => {
    const { items } = props;

    if (items === undefined || items.length === 0) {
      return <p>...</p>;
    } else {
      return (
        <Tabs value={value} onChange={handleChange} centered>
          {items.map((item: any) => {
            return <Tab label={item.name} key={`${item.abrv}}-${item.id}`} />;
          })}
        </Tabs>
      );
    }
  };

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%", bgcolor: "background.paper" }}>
      <Links items={items} />
    </Box>
  );
};

export default Navigation;
