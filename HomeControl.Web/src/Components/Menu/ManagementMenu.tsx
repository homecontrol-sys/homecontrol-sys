import { useContext } from "react";
import { Link, useLocation } from "react-router-dom";
import { List, ListItem, ListItemIcon, ListItemText } from "@mui/material";
import { People, AccountTreeTwoTone } from "@mui/icons-material";

import { RoutingContext } from "../Provider";

export const ManagementMenu = () => {
  const {} = useContext(RoutingContext);

  const currentPath = useLocation().pathname;

  return null;

  // return (
  //   <List>
  //     <ListItem
  //       button
  //       component={Link}
  //       to={systemStatePage.url}
  //       selected={currentPath === systemStatePage.url}
  //     >
  //       <ListItemIcon>
  //         <AccountTreeTwoTone />
  //       </ListItemIcon>
  //       <ListItemText primary={systemStatePage.title} />
  //     </ListItem>
  //     <ListItem button component={Link} to={usersPage.url} selected={currentPath === usersPage.url}>
  //       <ListItemIcon>
  //         <People />
  //       </ListItemIcon>
  //       <ListItemText primary={usersPage.title} />
  //     </ListItem>
  //   </List>
  // );
};
