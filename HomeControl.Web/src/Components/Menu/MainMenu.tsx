import { useContext } from "react";
import { Link, useLocation } from "react-router-dom";
import { List, ListItem, ListItemIcon, ListItemText } from "@mui/material";
import {
  People,
  AccountTreeTwoTone,
  DevicesOther,
  SettingsSuggest,
  ElectricalServicesOutlined,
  MiscellaneousServicesOutlined,
  Home,
} from "@mui/icons-material";

import { RoutingContext } from "../Provider";

export const MainMenu = () => {
  const {
    systemStatePage,
    systemsPage,
    devicesPage,
    elementsPage,
    switchStatesPage,
    switchesPage,
    modulesPage,
    usersPage,
  } = useContext(RoutingContext);

  const currentPath = useLocation().pathname;

  return (
    <List>
      <ListItem
        button
        component={Link}
        to={systemStatePage.url}
        selected={currentPath === systemStatePage.url}
      >
        <ListItemIcon>
          <Home />
        </ListItemIcon>
        <ListItemText primary={systemStatePage.title} />
      </ListItem>
      <ListItem
        button
        component={Link}
        to={systemsPage.url}
        selected={currentPath === systemsPage.url}
      >
        <ListItemIcon>
          <MiscellaneousServicesOutlined />
        </ListItemIcon>
        <ListItemText primary={systemsPage.title} />
      </ListItem>
      <ListItem
        button
        component={Link}
        to={devicesPage.url}
        selected={currentPath === devicesPage.url}
      >
        <ListItemIcon>
          <SettingsSuggest />
        </ListItemIcon>
        <ListItemText primary={devicesPage.title} />
      </ListItem>
      <ListItem
        button
        component={Link}
        to={elementsPage.url}
        selected={currentPath === elementsPage.url}
      >
        <ListItemIcon>
          <ElectricalServicesOutlined />
        </ListItemIcon>
        <ListItemText primary={elementsPage.title} />
      </ListItem>
      <ListItem
        button
        component={Link}
        to={switchStatesPage.url}
        selected={currentPath === switchStatesPage.url}
      >
        <ListItemIcon>
          <ElectricalServicesOutlined />
        </ListItemIcon>
        <ListItemText primary={switchesPage.title} />
      </ListItem>
      <ListItem
        button
        component={Link}
        to={switchesPage.url}
        selected={currentPath === switchesPage.url}
      >
        <ListItemIcon>
          <ElectricalServicesOutlined />
        </ListItemIcon>
        <ListItemText primary={switchesPage.title} />
      </ListItem>
      <ListItem
        button
        component={Link}
        to={modulesPage.url}
        selected={currentPath === modulesPage.url}
      >
        <ListItemIcon>
          <AccountTreeTwoTone />
        </ListItemIcon>
        <ListItemText primary={modulesPage.title} />
      </ListItem>
      <ListItem button component={Link} to={usersPage.url} selected={currentPath === usersPage.url}>
        <ListItemIcon>
          <People />
        </ListItemIcon>
        <ListItemText primary={usersPage.title} />
      </ListItem>
    </List>
  );
};
