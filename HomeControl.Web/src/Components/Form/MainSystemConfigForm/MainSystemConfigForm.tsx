import React, { FC, FormEvent, useState } from "react";
import { Grid, ToggleButton, ToggleButtonGroup, Divider } from "@mui/material";

import { NetworkType } from "../../../Models";
import { Form, FormControl, FormActions, useForm } from "../../../Components";
import { configureMainSystem } from "../../../Services/SystemService";
import "./MainSystemConfigForm.scss";

export type MainSystemConfigFormModel = {
  name: string;
  ssid: string;
  password: string;
};

const initialFormValues: MainSystemConfigFormModel = {
  name: "",
  ssid: "",
  password: "",
};

interface MainSystemConfigFormProps {
  formName: string;
}

export const MainSystemConfigForm: FC<MainSystemConfigFormProps> = (
  props: MainSystemConfigFormProps
) => {
  const { formName } = props;
  const [networkType, setNetworkType] = useState(NetworkType.SECURE);

  const validate = (fieldValue = values) => {
    let errorMessages = { ...errors };
    if ("name" in fieldValue) errorMessages.name = fieldValue.name ? "" : "This field is required.";
    if ("ssid" in fieldValue) errorMessages.ssid = fieldValue.ssid ? "" : "This field is required.";
    if ("password" in fieldValue && networkType === NetworkType.SECURE)
      errorMessages.password = fieldValue.password ? "" : "This field is required.";
    setErrors({ ...errorMessages });

    let formErrors = NetworkType.SECURE
      ? errorMessages
      : (errorMessages = delete errorMessages.password);
    if (fieldValue === values) return Object.values(formErrors).every((x) => x === "");
  };

  const { values, errors, setErrors, handleInputChange, reset } = useForm(
    initialFormValues,
    validate
  );

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (validate?.()) {
      window.alert("form is valid");
      configureMainSystem(values);
    }
  };

  const handleNetworkTypeChange = (
    event: React.MouseEvent<HTMLElement>,
    newNetworkType: NetworkType | null
  ) => {
    if (newNetworkType !== networkType && newNetworkType !== null) {
      if (newNetworkType === NetworkType.OPEN) values.password = "";
      setNetworkType(newNetworkType);
    }
  };

  return (
    <Form title="Main System Configuration" name={formName} onSubmit={handleSubmit}>
      <Grid container gap={1} className={`${formName}__container`}>
        {/* <Grid item xl={12}>
          <Divider flexItem>Details</Divider>
        </Grid> */}
        <Grid item xl={12} className={`${formName}__container__row`}>
          <FormControl.UserTextInput
            name="name"
            label="Name"
            value={values.name}
            handleInputChange={handleInputChange}
            {...(errors.name && { error: true, helperText: errors.name })}
          />
        </Grid>
        <Grid item xl={12}>
          <Divider flexItem>Network</Divider>
        </Grid>
        <Grid item xl={12} container className={`${formName}__container__row`}>
          <ToggleButtonGroup
            color="primary"
            value={networkType}
            exclusive
            onChange={handleNetworkTypeChange}
            fullWidth
          >
            <ToggleButton fullWidth value={NetworkType.SECURE}>
              Secure
            </ToggleButton>
            <ToggleButton fullWidth value={NetworkType.OPEN}>
              Open
            </ToggleButton>
          </ToggleButtonGroup>
        </Grid>
        <Grid item xl={12} container>
          <Grid
            item
            xl={networkType === NetworkType.OPEN ? 12 : 6}
            container
            className={`${formName}__container__row`}
          >
            <FormControl.UserTextInput
              name="ssid"
              label="SSID"
              value={values.ssid}
              handleInputChange={handleInputChange}
              {...(errors.ssid && { error: true, helperText: errors.ssid })}
            />
          </Grid>
          {networkType === NetworkType.SECURE ? (
            <Grid item xl={6} container className={`${formName}__container__row`}>
              <FormControl.UserTextInput
                name="password"
                label="Password"
                value={values.password}
                handleInputChange={handleInputChange}
                {...(errors.password && { error: true, helperText: errors.password })}
              />
            </Grid>
          ) : null}
        </Grid>
        <Grid item xl={12} container className={`${formName}__container__row`}></Grid>
      </Grid>
      <Divider />
      <div className={`${formName}__container-actions`}>
        <FormActions onCancel={reset} onClear={reset} />
      </div>
    </Form>
  );
};
