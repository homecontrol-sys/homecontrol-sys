import { FC } from "react";
import { Button, Divider, Grid } from "@mui/material";
import { ArrowBackIosNew, Check, Close } from "@mui/icons-material";

interface FormActionsProps {
  onCancel: () => void;
  onCancelCallback?: () => void;
  onClear?: () => void;
  onClearCallback?: () => void;
}

export const FormActions: FC<FormActionsProps> = (props: FormActionsProps) => {
  const { onCancel, onCancelCallback, onClear, onClearCallback } = props;

  const handleCancel = () => {
    onCancel();
    onCancelCallback?.();
  };

  const handleClear = () => {
    onClear?.();
    onClearCallback?.();
  };

  return (
    <Grid container className="form-actions">
      {/* {onClear ? (
        <>
          
        </>
      ) : null} */}
      <Grid container item xl={4} justifyContent="flex-start">
        <Button
          sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }}
          variant="outlined"
          color="primary"
          startIcon={<ArrowBackIosNew />}
          onClick={handleClear}
        >
          Clear
        </Button>
      </Grid>
      <Divider flexItem orientation="vertical" />
      <Grid container item xl={8} justifyContent="flex-end">
        <Button sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }} variant="outlined" color="error" startIcon={<Close />} onClick={handleCancel}>
          Cancel
        </Button>
        <Button type="submit" sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }} variant="outlined" color="success" startIcon={<Check />}>
          Submit
        </Button>
      </Grid>
    </Grid>
  );
};
