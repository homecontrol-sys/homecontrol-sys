import { FC } from "react";
import { Typography, Divider } from "@mui/material";

import "./FormHeader.scss";

interface FormHeaderProps {
  title: string;
}

export const FormHeader: FC<FormHeaderProps> = (props: FormHeaderProps) => {
  const { title } = props;

  return (
    <>
      <Typography className="form-header" component="span" variant="h6" color="primary">
        {title}
      </Typography>
      <Divider />
    </>
  );
};
