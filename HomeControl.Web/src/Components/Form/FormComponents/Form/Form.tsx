import { FC, ChangeEvent, useState, FormEventHandler } from "react";
import { Box, Paper } from "@mui/material";

import { FormHeader } from "../../../../Components";
import "./Form.scss";

//export const useAppForm = (initialValues: any, validate: any) => {
export const useForm = (initialValues: any, validateInputOnChange?: (arg: any) => void) => {
  const [values, setValues] = useState(initialValues);
  const [errors, setErrors] = useState<any>({});

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    const fieldValue = { [name]: value };

    setValues({
      ...values,
      ...fieldValue,
    });

    if (validateInputOnChange !== undefined) {
      validateInputOnChange(fieldValue);
    }
  };

  const reset = () => {
    setValues(initialValues);
    setErrors({});
  };

  return { values, setValues, errors, setErrors, handleInputChange, reset };
};

interface AppFormProps {
  name: string;
  title?: string;
  children: JSX.Element | JSX.Element[];
  onSubmit: FormEventHandler<HTMLFormElement>;
}

export const Form: FC<AppFormProps> = (props: AppFormProps) => {
  const { name, title, children, onSubmit } = props;

  return (
    <Box
      component="form"
      noValidate
      autoComplete="off"
      id={name}
      className={name}
      onSubmit={onSubmit}
    >
      <Paper className="form__content" elevation={6}>
        {title ? <FormHeader title={title} /> : null}
        {children}
      </Paper>
    </Box>
  );
};
