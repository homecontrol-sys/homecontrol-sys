import { FC, ChangeEvent } from "react";
import { TextField } from "@mui/material";

export interface UserPasswordnputErrorModelProps {
  error: boolean;
  helperText: string;
}

interface UserPasswordInputProps {
  name: string;
  label: string;
  value: string;
  error: boolean;
  helperText: string;
  handleInputChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

export const UserPasswordInput: FC<UserPasswordInputProps> = (props: UserPasswordInputProps) => {
  const { name, label, value, handleInputChange, error, helperText = null } = props;

  return (
    <TextField
      id="standard-basic"
      variant="standard"
      name={name}
      label={label}
      value={value}
      onChange={handleInputChange}
      error={error}
      helperText={helperText ? helperText : " "}
    />
  );
};
