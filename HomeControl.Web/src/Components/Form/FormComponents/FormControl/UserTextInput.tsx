import { FC, ChangeEvent } from "react";
import { TextField } from "@mui/material";

export interface UserTextInputErrorModelProps {
  error: boolean;
  helperText: string;
}

interface UserTextInputProps {
  name: string;
  label: string;
  value: string;
  error: boolean;
  helperText: string;
  handleInputChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

export const UserTextInput: FC<UserTextInputProps> = (props: UserTextInputProps) => {
  const { name, label, value, handleInputChange, error, helperText = null } = props;

  return (
    <TextField
      id="standard-basic"
      variant="standard"
      name={name}
      label={label}
      value={value}
      onChange={handleInputChange}
      error={error}
      helperText={helperText ? helperText : " "}
    />
  );
};
