import "./NetworkConfigForm.scss";

interface NetworkConfigFormProps {}

export const NetworkConfigForm: React.FC<NetworkConfigFormProps> = (
  props: NetworkConfigFormProps
) => {
  return (
    <div className="login">
      <h3 className="login__title">Network Configuration</h3>
      <form className="login__form">
        <input className="login__form__input" type="text" name="usernameOrEmail" />
        <input className="login__form__input" type="text" name="password" />
        <button type="submit">Login</button>
      </form>
    </div>
  );
};
