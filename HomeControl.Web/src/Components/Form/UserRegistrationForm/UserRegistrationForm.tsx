import { FC, FormEvent } from "react";
import { Grid, Divider } from "@mui/material";
import { Form, useForm, FormControl, FormActions } from "../../../Components";
import { isValidEmail } from "../../../Utils";
import "./UserRegistrationForm.scss";

export type UserRegistrationFormModel = {
  firstName: string;
  lastName: string;
  email: string;
  emailConfirmation: string;
  password: string;
};

const initialFormValues: UserRegistrationFormModel = {
  firstName: "",
  lastName: "",
  email: "",
  emailConfirmation: "",
  password: "",
};

interface UserRegistrationFormProps {
  formName: string;
}

export const UserRegistrationForm: FC<UserRegistrationFormProps> = (
  props: UserRegistrationFormProps
) => {
  const { formName } = props;

  const validate = (fieldValue = values) => {
    let errorMessages = { ...errors };
    if ("firstName" in fieldValue)
      errorMessages.firstName = fieldValue.firstName ? "" : "This field is required.";
    if ("lastName" in fieldValue)
      errorMessages.lastName = fieldValue.lastName ? "" : "This field is required.";
    if ("email" in fieldValue)
      errorMessages.email = isValidEmail(fieldValue.email) ? "" : "Please enter valid E-mail.";
    if ("emailConfirmation" in fieldValue)
      errorMessages.emailConfirmation = fieldValue.emailConfirmation
        ? ""
        : "This field is required.";
    setErrors({ ...errorMessages });

    if (fieldValue === values) return Object.values(errorMessages).every((x) => x === "");
  };

  const { values, errors, setErrors, handleInputChange, reset } = useForm(
    initialFormValues,
    validate
  );

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (validate?.()) {
      window.alert("form is valid");
    }
  };

  return (
    <Form name={formName} onSubmit={handleSubmit}>
      <Grid container gap={1} className={`${formName}__container`}>
        <Grid item xl={12} className={`${formName}__container__row`}>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="firstName"
              label="First Name"
              value={values.firstName}
              handleInputChange={handleInputChange}
              {...(errors.firstName && { error: true, helperText: errors.firstName })}
            />
          </Grid>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="lastName"
              label="Last Name"
              value={values.lastName}
              handleInputChange={handleInputChange}
              {...(errors.lastName && { error: true, helperText: errors.lastName })}
            />
          </Grid>
        </Grid>
        <Grid item xl={12} container className={`${formName}__container__row`}>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="email"
              label="E-mail"
              value={values.email}
              handleInputChange={handleInputChange}
              {...(errors.email && { error: true, helperText: errors.email })}
            />
          </Grid>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="emailConfirmation"
              label="Confirm E-mail"
              value={values.emailConfirmation}
              handleInputChange={handleInputChange}
              {...(errors.emailConfirmation && {
                error: true,
                helperText: errors.emailConfirmation,
              })}
            />
          </Grid>
        </Grid>
      </Grid>
      <Divider />
      <div className={`${formName}__container-actions`}>
        <FormActions onCancel={reset} onClear={reset} />
      </div>
    </Form>
  );
};
