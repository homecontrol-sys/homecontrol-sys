import { Box, Paper } from "@mui/material";
import { useState } from "react";
import "./LoginForm.css";
import {
  TextField,
  Grid,
  Button,
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  IconButton,
} from "@mui/material";
import {
  AccountCircle,
  Login,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";

const LoginForm = () => {
  const [usernameOrEmail, setUsernameOrEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <Box
      component="form"
      sx={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        "& > :not(style)": {
          m: 1,
        },
      }}
      noValidate
      autoComplete="off"
    >
      <Paper
        className="login"
        elevation={6}
        sx={{
          justifyContent: "space-around",
          padding: "2rem",
          m: 2,
        }}
      >
        <Grid sx={{ justifyContent: "center" }} container gap={3}>
          <Grid item xl={12} container>
            <Grid
              item
              xl={6}
              sx={{ display: "flex", justifyContent: "center" }}
            >
              <Box sx={{ m: 1, display: "flex", alignItems: "flex-end" }}>
                <AccountCircle
                  sx={{ color: "action.active", mr: 1, my: 0.5 }}
                />
                <TextField
                  id="input-username"
                  label="Username"
                  variant="standard"
                />
              </Box>
            </Grid>
            <Grid
              item
              xl={6}
              sx={{ display: "flex", justifyContent: "center" }}
            >
              <FormControl sx={{ m: 1, width: "25ch" }} variant="standard">
                <InputLabel htmlFor="standard-adornment-password">
                  Password
                </InputLabel>
                <Input
                  id="standard-adornment-password"
                  // type={values.showPassword ? "text" : "password"}
                  // value={values.password}
                  // onChange={handleChange("password")}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                      // aria-label="toggle password visibility"
                      // onClick={handleClickShowPassword}
                      // onMouseDown={handleMouseDownPassword}
                      >
                        {false ? <VisibilityOff /> : <Visibility />}
                        {/* {values.showPassword ? <VisibilityOff /> : <Visibility />} */}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
              {/* <TextField
              className="login__form__input"
              type="text"
              name="password"
              label="Password"
              variant="standard"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            /> */}
            </Grid>
            {/* <TextField
            className="login__form__input"
            type="text"
            name="usernameOrEmail"
            
            label="Username or E-Mail"
            variant="standard"
            value={usernameOrEmail}
            onChange={(e) => setUsernameOrEmail(e.target.value)}
          /> */}
          </Grid>
          <Grid item xl={12} sx={{ textAlign: "center" }}>
            <Button
              sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }}
              variant="outlined"
              startIcon={<Login />}
            >
              Login
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </Box>
  );
};

export default LoginForm;
