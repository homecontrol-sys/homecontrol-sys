import { FC, FormEvent } from "react";
import { Button, Grid, Avatar } from "@mui/material";
import { Person, PasswordTwoTone } from "@mui/icons-material";
import { Form, useForm, FormControl } from "..";
import { isValidEmail } from "../../../Utils";
import "./UserInvitationForm.scss";

export type UserInvitationFormModel = {
  email: string;
  emailConfirmation: string;
  invitationExpirationDate: string;
};

const initialFormValues: UserInvitationFormModel = {
  email: "",
  emailConfirmation: "",
  invitationExpirationDate: "",
};

interface UserInvitationFormProps {}

export const UserInvitationForm: FC<UserInvitationFormProps> = () => {
  const formName = "user-invitation-form";

  const validateForm = (fieldValue: any) => {
    let errorMessages = { ...errors };

    if ("email" in fieldValue)
      errorMessages.email = !isValidEmail(fieldValue.email) ? "" : "This field is required.";
    if ("emailConfirmation" in fieldValue)
      errorMessages.emailConfirmation = fieldValue.emailConfirmation
        ? ""
        : "This field is required.";
    if ("invitationExpirationDate" in fieldValue)
      errorMessages.password = fieldValue.password ? "" : "This field is required.";
    setErrors({ ...errorMessages });

    return Object.values(errorMessages).every((x) => x === "");
  };

  const { values, errors, setErrors, handleInputChange } = useForm(initialFormValues);

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  };

  return (
    <Form name={formName} onSubmit={handleSubmit}>
      <Grid container gap={1} className={`${formName}__container`}>
        <Grid item xl={12} className={`${formName}__container__row`}>
          <Avatar>
            <Person fontSize="large" />
          </Avatar>
        </Grid>
        <Grid item xl={12} className={`${formName}__container__row`}>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="firstName"
              label="First Name"
              value={values.firstName}
              handleInputChange={handleInputChange}
              {...(errors.firstName && { error: true, helperText: errors.firstName })}
            />
          </Grid>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="lastName"
              label="Last Name"
              value={values.lastName}
              handleInputChange={handleInputChange}
              {...(errors.lastName && { error: true, helperText: errors.lastName })}
            />
          </Grid>
        </Grid>
        <Grid item xl={12} container className={`${formName}__container__row`}>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="email"
              label="E-mail"
              value={values.email}
              handleInputChange={handleInputChange}
              {...(errors.email && { error: true, helperText: errors.email })}
            />
          </Grid>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="emailConfirmation"
              label="Confirm E-mail"
              value={values.emailConfirmation}
              handleInputChange={handleInputChange}
              {...(errors.emailConfirmation && {
                error: true,
                helperText: errors.emailConfirmation,
              })}
            />
          </Grid>
        </Grid>
        <Grid item xl={12} container className={`${formName}__container__row`}>
          <Grid item xl={6} className={`${formName}__container__column`}>
            <FormControl.UserTextInput
              name="password"
              label="Password"
              value={values.password}
              handleInputChange={handleInputChange}
              {...(errors.password && { error: true, helperText: errors.password })}
            />
          </Grid>
          <Grid item xl={6} className={`${formName}__container__row`}>
            <Button
              sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }}
              variant="outlined"
              startIcon={<PasswordTwoTone />}
            >
              Generate Password
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Form>
  );
};
