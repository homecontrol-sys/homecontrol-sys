import { createContext } from "react";

export type IAppContext = {
  siteName: string;
};

const AppContextDefaultValues: IAppContext = {
  siteName: "HomeControl",
};

export const AppContext = createContext<IAppContext>(AppContextDefaultValues);

export const AppContextProvider: React.FC = ({ children }) => {
  return (
    <AppContext.Provider value={AppContextDefaultValues}>
      {children}
    </AppContext.Provider>
  );
};
