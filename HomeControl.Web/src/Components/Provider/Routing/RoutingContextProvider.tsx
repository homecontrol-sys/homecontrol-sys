import { createContext } from "react";

import { RoutePage, RouteAccessType } from "../../../Models";

const baseUrl = "/";

export type IPageRoutingContext = {
  systemStatePage: RoutePage;
  welcomePage: RoutePage;
  configPage: RoutePage;
  systemsPage: RoutePage;
  devicesPage: RoutePage;
  elementsPage: RoutePage;
  switchStatesPage: RoutePage;
  switchesPage: RoutePage;
  modulesPage: RoutePage;
  usersPage: RoutePage;
};

const RoutingContextDefaultValues: IPageRoutingContext = {
  systemStatePage: {
    url: baseUrl,
    access: RouteAccessType.PRIVATE,
    name: "state",
    title: "State",
  },
  welcomePage: {
    url: "/welcome",
    access: RouteAccessType.PUBLIC,
    name: "welcome",
    title: "Welcome",
  },
  configPage: {
    url: "/configuration",
    access: RouteAccessType.PRIVATE,
    name: "configuration",
    title: "Configuration",
  },
  systemsPage: {
    url: "/systems",
    access: RouteAccessType.PRIVATE,
    name: "systems",
    title: "Systems",
  },
  devicesPage: {
    url: "/devices",
    access: RouteAccessType.PRIVATE,
    name: "devices",
    title: "Devices",
  },
  elementsPage: {
    url: "/elements",
    access: RouteAccessType.PRIVATE,
    name: "elements",
    title: "Elements",
  },
  switchStatesPage: {
    url: "/switch-states",
    access: RouteAccessType.PRIVATE,
    name: "switchStates",
    title: "Switch States",
  },
  switchesPage: {
    url: "/switches",
    access: RouteAccessType.PRIVATE,
    name: "switches",
    title: "Switches",
  },
  modulesPage: {
    url: "/modules",
    access: RouteAccessType.PRIVATE,
    name: "modules",
    title: "Modules",
  },
  usersPage: {
    url: "/users",
    access: RouteAccessType.PRIVATE,
    name: "users",
    title: "Users",
  },
};

export const RoutingContext = createContext<IPageRoutingContext>(RoutingContextDefaultValues);

export const AppRoutingContextProvider: React.FC = ({ children }) => {
  return (
    <RoutingContext.Provider value={RoutingContextDefaultValues}>
      {children}
    </RoutingContext.Provider>
  );
};
