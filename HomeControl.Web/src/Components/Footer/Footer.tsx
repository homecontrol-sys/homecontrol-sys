import { Divider } from "@mui/material";

import Rights from "./Rights";
import "./Footer.scss";

interface FooterProps {
  className?: string;
}

const Footer: React.FC<FooterProps> = (props: FooterProps) => {
  const { className } = props;

  return (
    <footer className={`footer ${className ? className : ""}`}>
      <Divider>
        <Rights />
      </Divider>
    </footer>
  );
};

export default Footer;
