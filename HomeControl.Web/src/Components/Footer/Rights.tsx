import React, { useContext } from "react";

import { Typography } from "@mui/material";
import { AppContext } from "../Provider";

interface RightsProps {}

const Rights: React.FC<RightsProps> = (props: RightsProps) => {
  const { siteName } = useContext(AppContext);

  const copyrightsSymbol = "\u00A9";
  const year = new Date().getFullYear();
  const rightsText = "all rights reserved";

  const rightsString = `${copyrightsSymbol} ${year} ${siteName} ${rightsText}.`;

  return (
    <Typography className="rights" variant="subtitle1" color="text.secondary" align="center">
      {rightsString}
    </Typography>
  );
};

export default Rights;
