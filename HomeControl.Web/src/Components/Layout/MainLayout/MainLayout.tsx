import React from "react";
import { Box, Container } from "@mui/material";

import { Footer } from "../../";
import "./MainLayout.scss";

interface MainLayoutProps {
  children: JSX.Element | Array<JSX.Element>;
  className?: string;
}

const MainLayout: React.FC<MainLayoutProps> = (props: MainLayoutProps) => {
  const { children, className } = props;

  return (
    <Box
      className={`layout ${className ? className : ""}`}
      sx={{
        bgcolor: "background.default",
        color: "text.primary",
      }}
    >
      <Container className="layout__main-content">{children}</Container>
      <Container className="layout__footer">
        <Footer />
      </Container>
    </Box>
  );
};

export default MainLayout;
