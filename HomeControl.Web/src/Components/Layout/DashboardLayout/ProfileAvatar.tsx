import { ListItem, ListItemAvatar, ListItemText } from "@mui/material";
import Avatar from "@mui/material/Avatar/Avatar";
import profileSrc from "../../../Assets/Img/profile.jpg";

interface ProfileAvatarProps {}

const ProfileAvatar: React.FC<ProfileAvatarProps> = (
  props: ProfileAvatarProps
) => {
  const fullName = "Anđelo Barić";
  const role = "Admin";

  return (
    <>
      <ListItem>
        <ListItemAvatar>
          <Avatar alt={fullName} src={profileSrc} />
        </ListItemAvatar>
        <ListItemText primary={fullName} secondary={role} />
      </ListItem>
    </>
  );
};

export default ProfileAvatar;
