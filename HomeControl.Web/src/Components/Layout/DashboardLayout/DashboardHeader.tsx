import * as React from "react";
import { Grid, Typography } from "@mui/material";

interface DashboardHeaderProps {
  text: string;
}

export const DashboardHeader: React.FC<DashboardHeaderProps> = (props: DashboardHeaderProps) => {
  const { text } = props;

  return (
    <Grid container sx={{ m: 0 }}>
      <Grid item xl={2} container sx={{ justifyContent: "center", alignItems: "center" }}></Grid>
      <Grid item xl={8} container sx={{ justifyContent: "center", alignItems: "center" }}>
        <Typography component="h1" variant="h6" color="primary" noWrap align="center" sx={{ flexGrow: 1 }}>
          {text}
        </Typography>
      </Grid>
      <Grid item xl={2} container sx={{ justifyContent: "center", alignItems: "center" }}></Grid>
    </Grid>
  );
};
