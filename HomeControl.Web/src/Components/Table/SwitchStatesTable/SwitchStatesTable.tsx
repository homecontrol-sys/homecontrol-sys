import { FC } from "react";
import {
  Button,
  Paper,
  Table,
  TableContainer,
  TableBody,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import { PersonAddAltRounded } from "@mui/icons-material";
import { TableProps, StateSwitchState } from "../../../Models";
// import { StateSwitchStateRegistrationDialog, TrueOrFalseIcon } from "../..";
import { useAppDispatch, setShowDialog } from "../../../Redux";
import { StyledTableRow, StyledTableCell } from "../TableStyles";
import "./SwitchStatesTable.scss";

export interface SwitchStatesTableProps extends TableProps {
  switchStates: StateSwitchState[];
}

export const SwitchStatesTable: FC<SwitchStatesTableProps> = (props: SwitchStatesTableProps) => {
  const dispatch = useAppDispatch();
  const { switchStates = [], name } = props;

  const handleChangePage = () => {
    console.log("PageChanged");
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log("RowsPerPageChanged");
  };

  return (
    <>
      {/* <StateSwitchStateRegistrationDialog /> */}
      <Paper className={`${name}-table-container`} elevation={6}>
        <Typography
          className={`${name}-table-container__header`}
          component="h2"
          variant="h6"
          color="primary"
          gutterBottom
        >
          SwitchStates
        </Typography>
        <TableContainer component={Paper} className={`${name}-table-container__main`} elevation={6}>
          <Table size="small" className={`${name}-table-container__main__table`}>
            <TableHead className={`${name}-table-container__main__table__header`}>
              <TableRow>
                <StyledTableCell align="center">#</StyledTableCell>
                <StyledTableCell align="center">Name</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody className={`${name}-table-container__main__table__body`}>
              {switchStates &&
                switchStates.map((switchState, index) => (
                  <StyledTableRow key={switchState.id}>
                    <StyledTableCell>
                      <div className="nr-icon-cell">
                        <span>{index + 1}</span>.
                      </div>
                    </StyledTableCell>
                    <StyledTableCell align="center">{switchState.name}</StyledTableCell>
                  </StyledTableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={100}
          rowsPerPage={25}
          page={1}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
        <Button
          sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }}
          variant="outlined"
          startIcon={<PersonAddAltRounded />}
          onClick={() => dispatch(setShowDialog(true))}
        >
          Register StateSwitchState
        </Button>
      </Paper>
    </>
  );
};
