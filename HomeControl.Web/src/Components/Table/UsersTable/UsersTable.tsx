import { FC } from "react";
import { Button, Paper, Table, TableContainer, TableBody, TableHead, TablePagination, TableRow, Typography } from "@mui/material";
import { PersonAddAltRounded } from "@mui/icons-material";
import { Person } from "@mui/icons-material";
import { TableProps, User } from "../../../Models";
import { UserRegistrationDialog, TrueOrFalseIcon } from "../../../Components";
import { useAppDispatch, setShowDialog } from "../../../Redux";
import { StyledTableRow, StyledTableCell } from "../TableStyles";
import "./UsersTable.scss";

interface UsersTableProps extends TableProps {
  users: User[];
}

export const UsersTable: FC<UsersTableProps> = (props: UsersTableProps) => {
  const dispatch = useAppDispatch();
  const { users = [], name } = props;

  const handleChangePage = () => {
    console.log("PageChanged");
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log("RowsPerPageChanged");
  };

  return (
    <>
      <UserRegistrationDialog />
      <Paper className={`${name}-table-container`} elevation={6}>
        <Typography className={`${name}-table-container__header`} component="h2" variant="h6" color="primary" gutterBottom>
          Users
        </Typography>
        <TableContainer component={Paper} className={`${name}-table-container__main`} elevation={6}>
          <Table size="small" className={`${name}-table-container__main__table`}>
            <TableHead className={`${name}-table-container__main__table__header`}>
              <TableRow>
                <StyledTableCell align="center">#</StyledTableCell>
                <StyledTableCell align="center">First Name</StyledTableCell>
                <StyledTableCell align="center">Last Name</StyledTableCell>
                <StyledTableCell align="center">User Name</StyledTableCell>
                <StyledTableCell align="center">E-mail</StyledTableCell>
                <StyledTableCell align="center">E-mail confirmed</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody className={`${name}-table-container__main__table__body`}>
              {users &&
                users.map((user, index) => (
                  <StyledTableRow key={user.id}>
                    <StyledTableCell>
                      <div className="nr-icon-cell">
                        <span>{index + 1}</span>. <Person color={user.isAuthenticated ? "success" : "disabled"} fontSize="small" />
                      </div>
                    </StyledTableCell>
                    <StyledTableCell align="center">{user.firstName}</StyledTableCell>
                    <StyledTableCell align="center">{user.lastName}</StyledTableCell>
                    <StyledTableCell align="center">{user.userName}</StyledTableCell>
                    <StyledTableCell align="center">{user.email}</StyledTableCell>
                    <StyledTableCell align="center">
                      <TrueOrFalseIcon value={user.emailConfirmed} />
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={100}
          rowsPerPage={25}
          page={1}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
        <Button
          sx={{ m: 1, pt: 1, pb: 1, pl: 5, pr: 5 }}
          variant="outlined"
          startIcon={<PersonAddAltRounded />}
          onClick={() => dispatch(setShowDialog(true))}
        >
          Register User
        </Button>
      </Paper>
    </>
  );
};
