import { FC, useEffect } from "react";

import { SwitchStatesTable } from "../../Components";
import { ScreenProps } from "../../Models";
import { useAppDispatch, useAppSelector, getSwitchStatesAsync, setShowSpinner } from "../../Redux";

interface SwitchStatesScreenProps extends ScreenProps {}

export const SwitchStatesScreen: FC<SwitchStatesScreenProps> = (props: SwitchStatesScreenProps) => {
  const { name } = props;
  const { showSpinner: showSpinnerCurrentState } = useAppSelector((state) => state.app);
  const { isLoading, switchStates } = useAppSelector((state) => state.switchStates);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getSwitchStatesAsync());
  }, [dispatch]);

  useEffect(() => {
    if (showSpinnerCurrentState !== isLoading) dispatch(setShowSpinner(isLoading));
  }, [dispatch, showSpinnerCurrentState, isLoading]);

  return (
    <div className={`${name}--screen`}>
      <SwitchStatesTable name={name} switchStates={switchStates} />
    </div>
  );
};
