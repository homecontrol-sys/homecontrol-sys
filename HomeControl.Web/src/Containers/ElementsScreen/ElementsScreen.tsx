import { FC, useEffect } from "react";

import { ElementsTable } from "../../Components";
import { ScreenProps } from "../../Models";
import { useAppDispatch, useAppSelector, getElementsAsync, setShowSpinner } from "../../Redux";

interface ElementsScreenProps extends ScreenProps {}

export const ElementsScreen: FC<ElementsScreenProps> = (props: ElementsScreenProps) => {
  const { name } = props;
  const { showSpinner: showSpinnerCurrentState } = useAppSelector((state) => state.app);
  const { isLoading, elements } = useAppSelector((state) => state.elements);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getElementsAsync());
  }, [dispatch]);

  useEffect(() => {
    if (showSpinnerCurrentState !== isLoading) dispatch(setShowSpinner(isLoading));
  }, [dispatch, showSpinnerCurrentState, isLoading]);

  return (
    <div className={`${name}--screen`}>
      <ElementsTable name={name} elements={elements} />
    </div>
  );
};
