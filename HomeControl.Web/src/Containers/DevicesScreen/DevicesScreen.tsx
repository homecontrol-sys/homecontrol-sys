import { FC, useEffect } from "react";

import { ScreenProps } from "../../Models";
import { DevicesTable } from "../../Components";
import { useAppDispatch, useAppSelector, getDevicesAsync, setShowSpinner } from "../../Redux";

interface DevicesScreenProps extends ScreenProps {}

export const DevicesScreen: FC<DevicesScreenProps> = (props: DevicesScreenProps) => {
  const { name } = props;
  const { showSpinner: showSpinnerCurrentState } = useAppSelector((state) => state.app);
  const { isLoading, devices } = useAppSelector((state) => state.devices);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getDevicesAsync());
  }, [dispatch]);

  useEffect(() => {
    if (showSpinnerCurrentState !== isLoading) dispatch(setShowSpinner(isLoading));
  }, [dispatch, showSpinnerCurrentState, isLoading]);

  return (
    <div className={`${name}--screen`}>
      <DevicesTable name={name} devices={devices} />
    </div>
  );
};
