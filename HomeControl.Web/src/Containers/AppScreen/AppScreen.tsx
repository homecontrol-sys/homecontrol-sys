import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { useContext, useEffect } from "react";

import {
  AppContextProvider,
  RoutingContext,
  AppRoutingContextProvider,
} from "../../Components/Provider";
import { Provider as ReduxStoreProvider } from "react-redux";
import { ThemeProvider, createTheme } from "@mui/material";
import { isMainSystemConfiguredAsync, store, useAppDispatch, useAppSelector } from "../../Redux";
import {
  DevicesPage,
  ElementsPage,
  SwitchStatesPage,
  SwitchesPage,
  SystemStatePage,
  MainSystemConfigPage,
  ModulesPage,
  NotFoundPage,
  SystemsPage,
  UsersPage,
  WelcomePage,
  Spinner,
  AppRoute,
} from "../../Components";
import "./AppScreen.scss";

export const AppScreen = () => {
  const {
    systemStatePage,
    welcomePage,
    configPage,
    systemsPage,
    devicesPage,
    elementsPage,
    switchStatesPage,
    switchesPage,
    modulesPage,
    usersPage,
  } = useContext(RoutingContext);

  const { isSystemMainConfigured } = useAppSelector((state) => state.systems);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(isMainSystemConfiguredAsync());
  }, [dispatch]);

  return (
    <div className="screen__app">
      <Spinner />
      {isSystemMainConfigured !== null ? (
        <Router>
          <Switch>
            <AppRoute
              exact
              isMainSystemConfigured={isSystemMainConfigured}
              path={systemStatePage.url}
              component={SystemStatePage}
            />
            {/* <Route path={welcomePage.url} component={WelcomePage} /> */}
            <AppRoute
              isMainSystemConfigured={isSystemMainConfigured}
              path={configPage.url}
              component={MainSystemConfigPage}
            />
            <AppRoute
              isMainSystemConfigured={isSystemMainConfigured}
              path={systemsPage.url}
              component={SystemsPage}
            />
            <AppRoute
              isMainSystemConfigured={isSystemMainConfigured}
              path={devicesPage.url}
              component={DevicesPage}
            />
            <AppRoute
              isMainSystemConfigured={isSystemMainConfigured}
              path={elementsPage.url}
              component={ElementsPage}
            />
            <AppRoute
              isMainSystemConfigured={isSystemMainConfigured}
              path={switchStatesPage.url}
              component={SwitchStatesPage}
            />
            <AppRoute
              isMainSystemConfigured={isSystemMainConfigured}
              path={switchesPage.url}
              component={SwitchesPage}
            />
            <AppRoute
              isMainSystemConfigured={isSystemMainConfigured}
              path={modulesPage.url}
              component={ModulesPage}
            />
            <AppRoute
              isMainSystemConfigured={isSystemMainConfigured}
              path={usersPage.url}
              component={UsersPage}
            />
            <AppRoute isMainSystemConfigured={isSystemMainConfigured} component={NotFoundPage} />
          </Switch>
        </Router>
      ) : (
        <></>
      )}
    </div>
  );
};
