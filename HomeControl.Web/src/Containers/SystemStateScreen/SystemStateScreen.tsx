import { FC, useEffect } from "react";
import { CircularProgress, Container, Divider } from "@mui/material";

import { ScreenProps } from "../../Models";
import { SystemsCardList } from "../../Components";
import { useAppDispatch, useAppSelector, getSystemStateAsync } from "../../Redux";

interface SystemStateScreenProps extends ScreenProps {}

export const SystemStateScreen: FC<SystemStateScreenProps> = (props: SystemStateScreenProps) => {
  const { isLoading, systems } = useAppSelector((state) => state.systems);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getSystemStateAsync());
  }, [dispatch]);

  const otherSystems = systems.filter((system) => !system.isMain);

  return (
    <div className="screen">
      {isLoading ? (
        <CircularProgress disableShrink />
      ) : (
        <>
          <Container sx={{ m: 2 }}>
            <Divider>Systems</Divider>
          </Container>
          <SystemsCardList otherSystems={otherSystems} />
        </>
      )}
    </div>
  );
};
