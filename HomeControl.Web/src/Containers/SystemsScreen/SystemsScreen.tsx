import { FC, useEffect } from "react";

import { ScreenProps } from "../../Models";
import { SystemsTable } from "../../Components";
import { useAppDispatch, useAppSelector, getSystemsAsync, setShowSpinner } from "../../Redux";

interface SystemScreenProps extends ScreenProps {}

export const SystemsScreen: FC<SystemScreenProps> = (props: SystemScreenProps) => {
  const { name } = props;
  const { showSpinner: showSpinnerCurrentState } = useAppSelector((state) => state.app);
  const { isLoading, systems } = useAppSelector((state) => state.systems);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getSystemsAsync());
  }, [dispatch]);

  useEffect(() => {
    if (showSpinnerCurrentState !== isLoading) dispatch(setShowSpinner(isLoading));
  }, [dispatch, showSpinnerCurrentState, isLoading]);

  return (
    <div className={`${name}-screen`}>
      <SystemsTable name={name} systems={systems} />
    </div>
  );
};
