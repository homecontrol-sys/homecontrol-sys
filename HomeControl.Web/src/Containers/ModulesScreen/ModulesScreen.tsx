import { FC, useEffect } from "react";

import { ScreenProps } from "../../Models";
import { ModulesTable } from "../../Components";
import { useAppDispatch, useAppSelector, getModulesAsync, setShowSpinner } from "../../Redux";

interface ModulesScreenProps extends ScreenProps {}

export const ModulesScreen: FC<ModulesScreenProps> = (props: ModulesScreenProps) => {
  const { name } = props;
  const { showSpinner: showSpinnerCurrentState } = useAppSelector((state) => state.app);
  const { isLoading, modules } = useAppSelector((state) => state.modules);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getModulesAsync());
  }, [dispatch]);

  useEffect(() => {
    if (showSpinnerCurrentState !== isLoading) dispatch(setShowSpinner(isLoading));
  }, [dispatch, showSpinnerCurrentState, isLoading]);

  return (
    <div className={`${name}--screen`}>
      <ModulesTable name={name} modules={modules} />
    </div>
  );
};
