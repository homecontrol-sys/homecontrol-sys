import { FC, useEffect } from "react";

import { SwitchesTable } from "../../Components";
import { ScreenProps } from "../../Models";
import { useAppDispatch, useAppSelector, getSwitchesAsync, setShowSpinner } from "../../Redux";

interface SwitchesScreenProps extends ScreenProps {}

export const SwitchesScreen: FC<SwitchesScreenProps> = (props: SwitchesScreenProps) => {
  const { name } = props;
  const { showSpinner: showSpinnerCurrentState } = useAppSelector((state) => state.app);
  const { isLoading, switches } = useAppSelector((state) => state.switches);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getSwitchesAsync());
  }, [dispatch]);

  useEffect(() => {
    if (showSpinnerCurrentState !== isLoading) dispatch(setShowSpinner(isLoading));
  }, [dispatch, showSpinnerCurrentState, isLoading]);

  return (
    <div className={`${name}--screen`}>
      <SwitchesTable name={name} switches={switches} />
    </div>
  );
};
