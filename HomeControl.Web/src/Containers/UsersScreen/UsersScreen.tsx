import { FC, useEffect } from "react";

import { ScreenProps } from "../../Models";
import { UsersTable } from "../../Components";
import { useAppDispatch, useAppSelector, getUsersAsync, setShowSpinner } from "../../Redux";

interface UsersScreenProps extends ScreenProps {}

export const UsersScreen: FC<UsersScreenProps> = (props: UsersScreenProps) => {
  const { name } = props;
  const { showSpinner: showSpinnerCurrentState } = useAppSelector((state) => state.app);
  const { isLoading, users } = useAppSelector((state) => state.users);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getUsersAsync());
  }, [dispatch]);

  useEffect(() => {
    if (showSpinnerCurrentState !== isLoading) dispatch(setShowSpinner(isLoading));
  }, [dispatch, showSpinnerCurrentState, isLoading]);

  return (
    <div className={`${name}--screen`}>
      <UsersTable name={name} users={users} />
    </div>
  );
};
