import { FC } from "react";
import { Provider as ReduxStoreProvider } from "react-redux";
import { ThemeProvider, createTheme } from "@mui/material";
import { store } from "../../Redux";
import { AppContextProvider, AppRoutingContextProvider } from "../../Components/Provider";

interface AppRootProps {
  children: JSX.Element;
}

export const AppRoot: FC<AppRootProps> = (props: AppRootProps) => {
  const { children } = props;
  const mdTheme = createTheme({
    palette: {
      mode: "dark",
    },
  });

  return (
    <div className="app">
      <ReduxStoreProvider store={store}>
        <AppContextProvider>
          <AppRoutingContextProvider>
            <ThemeProvider theme={mdTheme}>{children}</ThemeProvider>
          </AppRoutingContextProvider>
        </AppContextProvider>
      </ReduxStoreProvider>
    </div>
  );
};
