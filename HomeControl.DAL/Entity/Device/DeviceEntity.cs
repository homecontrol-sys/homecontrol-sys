using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;
using HomeControl.DAL.Entity.Identity;

namespace HomeControl.DAL.Entity
{
    [Table("Device")]
    public class DeviceEntity : EntityBaseModel, IDeviceEntity
    {

        #region Fields

        public static Guid RaspberyrPiThreeBPlusId = new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b");

        #endregion

        #region Properties

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }


        public ICollection<SystemDeviceEntity> SystemDevices { get; set; }
        public ICollection<ModuleEntity> Modules { get; set; }
        public ICollection<PinEntity> Pins { get; set; }
        public ICollection<ElementEntity> Elements { get; set; }
            
        #endregion

        #region Methods

        #region Data Seed

        public static List<DeviceEntity> Seed()
        {   
            var entities = new List<DeviceEntity>();
            entities.AddRange(DeviceData());
            entities.AddRange(DeviceTestData());

            return entities;
        }

        private static List<DeviceEntity> DeviceData(){
            var actionName = "DeviceEntityDataSeed";
            var dataEntities = new List<DeviceEntity>
            {
                new DeviceEntity { 
                    Id = RaspberyrPiThreeBPlusId, 
                    Name = "Raspberry Pi",
                    Description = "Main System Device",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
            };

            return dataEntities;
        }

        private static List<DeviceEntity> DeviceTestData(){
            var actionName = "DeviceEntityTestDataSeed";
            var testEntities = new List<DeviceEntity>();

            for (var i = 0; i < 10 ; i++){
                testEntities.Add(
                    new DeviceEntity {
                        Id = Guid.NewGuid(),
                        Name = $"Raspberry Pi{i}",
                        Description = $"Raspberry Pi Device{i}",
                        DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                        DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                        Action = $"{actionName}"
                    });
            }

            return testEntities;
        }

        #endregion

        #endregion

    }
}
