using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface IDeviceEntity : IEntityBaseModel
    {
        
        #region Properties

        [Required]
        string Name { get; set; }
        string Description { get; set; }

        ICollection<SystemDeviceEntity> SystemDevices { get; set; }
        ICollection<ModuleEntity> Modules { get; set; }
        ICollection<PinEntity> Pins { get; set; }

        #endregion
    }
}
