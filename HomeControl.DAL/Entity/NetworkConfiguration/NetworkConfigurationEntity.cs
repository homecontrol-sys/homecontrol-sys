using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    [Table("NetworkConfiguration")]
    public class NetworkConfigurationEntity : EntityBaseModel, INetworkConfigurationEntity
    {

        #region Properties
        
        public string SSID { get; set; }
        public string Password { get; set; }
        [DefaultValue(false)]
        public bool IsConnectionVerified { get; set; }

        public SystemEntity System { get; set; }

        #endregion
    
    }
}
