using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface INetworkConfigurationEntity : IEntityBaseModel
    {
        
        #region Properties
        
        string SSID { get; set; }
        string Password { get; set; }
        bool IsConnectionVerified { get; set; }
        
        SystemEntity System { get; set; }

        #endregion

    }
}
