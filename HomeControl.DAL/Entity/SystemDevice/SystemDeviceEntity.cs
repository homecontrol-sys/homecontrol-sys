using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeControl.DAL.Entity
{
    [Table("SystemDevice")]
    public class SystemDeviceEntity : ISystemDeviceEntity
    {

        #region Properties

        public Guid SystemId { get; set; }
        public SystemEntity System { get; set; }

        public Guid DeviceId { get; set; }
        public DeviceEntity Device { get; set; }

        #endregion

    }
}
