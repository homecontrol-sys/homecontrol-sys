using System;

namespace HomeControl.DAL.Entity
{
    public interface ISystemDeviceEntity
    {

        #region Properties


        Guid SystemId { get; set; }
        SystemEntity System { get; set; }
        
        Guid DeviceId { get; set; }
        DeviceEntity Device { get; set; }


        #endregion

    }
}
