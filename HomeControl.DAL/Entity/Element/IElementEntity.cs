using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface IElementEntity : IEntityBaseModel
    {
        
        #region Properties
        
        string Name { get; set; }
        string Description { get; set; }
        string Alias { get; set; }

        Guid SystemId { get; set; }
        SystemEntity System { get; set; }
        Guid? DeviceId { get; set; }
        DeviceEntity Device { get; set; }
        Guid? ModuleId { get; set; }
        ModuleEntity Module { get; set; }
        Guid SwitchId { get; set; }
        SwitchEntity Switch { get; set;} 
        Guid SwitchStateId { get; set; }
        SwitchStateEntity SwitchState { get; set; }
        ICollection<PinEntity> Pins { get; set; }


        #endregion

    }
}
