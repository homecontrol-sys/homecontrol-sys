using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;
using HomeControl.DAL.Entity.Identity;

namespace HomeControl.DAL.Entity
{
    [Table("Element")]
    public class ElementEntity : EntityBaseModel, IElementEntity
    {

        #region Fieleds

        public static Guid LightBulbId = new Guid("e8474c0c-40e5-4c40-9f6b-25ceb7c7e456");

        #endregion

        #region Properties
        
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Alias { get; set; }

        [Required]
        public Guid SystemId { get; set; }
        public SystemEntity System { get; set; }
        public Guid? DeviceId { get; set; }
        public DeviceEntity Device { get; set; }
        public Guid? ModuleId { get; set; }
        public ModuleEntity Module { get; set; }
        [Required]
        public Guid SwitchId { get; set; }
        public SwitchEntity Switch { get; set; }
        [Required]
        public Guid SwitchStateId { get; set; }
        public SwitchStateEntity SwitchState { get; set; }
        public ICollection<PinEntity> Pins { get; set; }

        #endregion

        #region Methods

        #region Data Seed

        public static List<ElementEntity> Seed()
        {   
            var entities = new List<ElementEntity>();
            entities.AddRange(ElementData());

            return entities;
        }

        private static List<ElementEntity> ElementData(){
            var actionName = "ElementEntityDataSeed";
            var dataEntities = new List<ElementEntity>
            {
                new ElementEntity {
                    Id = LightBulbId,
                    SystemId = SystemEntity.LightSystemId,
                    SwitchId = SwitchEntity.ToggleSwitchId,
                    SwitchStateId = SwitchStateEntity.ToggleSwitchStateOffId,
                    Name = "Light Bulb",
                    Alias = "light-bulb",
                    Description = "Light bulb description",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
            };

            return dataEntities;
        }

        #endregion

        #endregion

    }
}
