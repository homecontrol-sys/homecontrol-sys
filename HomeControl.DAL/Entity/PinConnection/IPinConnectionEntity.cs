using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface IPinConnectionEntity : IEntityBaseModel
    {
        
        #region Properties
        
        string Name { get; set; }
        string Description { get; set; }
        bool IsTested { get; set; }
        bool IsValid { get; set; }

        Guid StartPinId { get; set; }
        Guid EndPinId { get; set; }
        ICollection<PinEntity> Pins { get; set; }

        #endregion

    }
}
