using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    [Table("PinConnection")]
    public class PinConnectionEntity : EntityBaseModel, IPinConnectionEntity
    {

        #region Fields

        #endregion

        #region Properties

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsTested { get; set; }
        public bool IsValid { get; set; }

        public Guid StartPinId { get; set; }
        public Guid EndPinId { get; set; }
        public ICollection<PinEntity> Pins { get; set; }

        #endregion

        #region Methods

        #region Data Seed

        public static List<PinEntity> Seed()
        {   
            var entities = new List<PinEntity>();
            // entities.AddRange(PinConnectionData());

            return entities;
        }

        
        // private static List<PinEntity> PinConnectionData(){
        //     var actionName = "PinConnectionEntityDataSeed";
        //     var dataEntities = new List<PinEntity>();

        //     #region Raspbery Pi - Relay Module connections

        //     #endregion

        //     return dataEntities;
        // }

        #endregion

        #endregion
    
    }
}
