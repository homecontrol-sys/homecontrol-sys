using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity.Identity
{
    public interface IUserEntity : IEntityBaseModel
    {
        
        #region Properties

        string FirstName { get; set; }
        string LastName { get; set; }
        bool IsAuthenticated { get; set; }
        string PasswordSalt { get; set; }
        DateTime PasswordModifiedDate { get; set; }
        DateTime? LastLoginDate { get; set; }
        short Status { get; set; }


        #region Navigation propereties

        ICollection<UserClaimEntity> Claims { get; set; }
        ICollection<UserLoginEntity> Logins { get; set; }
        ICollection<UserTokenEntity> Tokens { get; set; }
        ICollection<UserRoleEntity> UserRoles { get; set; }

        #endregion

        #endregion 

    }
}
