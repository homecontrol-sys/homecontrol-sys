using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Modules.Security._Extension;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.DAL.Entity.Identity
{
    [Table("User")]
    public class UserEntity : IdentityUser<Guid>, IUserEntity
    {

        #region Fields

        public static Guid SystemUserId = new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8");
        public static Guid AdminUserId = new Guid("5e5aa47c-dd83-4523-a514-ce092cb9c30f");

        #endregion

        #region Properties

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsAuthenticated { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime PasswordModifiedDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public short Status { get; set; }

        #region Navigation Properties

        public virtual ICollection<UserClaimEntity> Claims { get; set; }
        public virtual ICollection<UserLoginEntity> Logins { get; set; }
        public virtual ICollection<UserTokenEntity> Tokens { get; set; }
        public virtual ICollection<UserRoleEntity> UserRoles { get; set; }

        #endregion

        #region Jurnalism

        public DateTime DateCreated { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByUserId { get; set; }
        public string Action { get; set; }
            
        #endregion

        #endregion

        #region Methods

        #region Data Seed

        public static List<IUserEntity> Seed()
        {   
            var userEntities = new List<IUserEntity>();
            userEntities.AddRange(SystemData());
            userEntities.AddRange(SystemTestData());

            return userEntities;
        }

        private static List<UserEntity> SystemData(){
            var actionName = "UserEntityDataSeed";
            var userDataEntities = new List<UserEntity>
            {
                new UserEntity { 
                    Id = SystemUserId, 
                    UserName = "system", 
                    PasswordHash = "0000".SHA512Hash(),
                    DateCreated = DateTime.Now, CreatedByUserId = SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = SystemUserId,
                    Action = $"System {actionName}"
                },

                new UserEntity { 
                    Id = AdminUserId, 
                    UserName = "admin", 
                    PasswordHash = "0000".SHA512Hash(),
                    DateCreated = DateTime.Now, CreatedByUserId = SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = SystemUserId,
                    Action = $"Admin {actionName}"
                }
            };

            return userDataEntities;
        }

        private static List<UserEntity> SystemTestData(){
            var actionName = "UserEntityTestDataSeed";
            var userTestEntities = new List<UserEntity>();

            for (var i = 0; i < 10 ; i++){
                userTestEntities.Add(
                    new UserEntity {
                        Id = Guid.NewGuid(),
                        UserName = $"test{i}", 
                        PasswordHash = $"test{i}".SHA512Hash(),
                        DateCreated = DateTime.Now, CreatedByUserId = SystemUserId,
                        DateUpdated = DateTime.Now, UpdatedByUserId = SystemUserId,
                        Action = $"Admin{i} {actionName}"
                    });
            }

            return userTestEntities;
        }

        #endregion
        
        #endregion

    }

}
