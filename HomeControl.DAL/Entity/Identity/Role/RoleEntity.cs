using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.DAL.Entity.Identity
{

    [Table("Role")]
    public class RoleEntity : IdentityRole<Guid>, IRoleEntity
    {

        #region Fields

        public static Guid AdminRoleId = new Guid("7950da96-a257-444f-b344-5cc3c6dc5bd7");

        #endregion

        #region Properties

        public string Abrv { get; set; }
        public int Level { get; set; }

        public virtual ICollection<UserRoleEntity> UserRoles { get; set; }
        public virtual ICollection<RoleClaimEntity> RoleClaims { get; set; }

        #region Jurnalism

        public DateTime DateCreated { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByUserId { get; set; }
        public string Action { get; set; }
            
        #endregion

        #endregion

        #region Data Seed

        public static List<RoleEntity> Seed() 
        {
            string actionName = "RoleEntitySeed";          

            var roleEntities = new List<RoleEntity>
            {
                new RoleEntity 
                {
                    Abrv = "admin", Name = "Admin", Id = AdminRoleId, Level = 1,
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = actionName 
                },

            };

            return roleEntities;
        }

        #endregion
    
    }
}
