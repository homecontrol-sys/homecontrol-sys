using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity.Identity
{
    public interface IRoleEntity : IEntityBaseModel
    {

        #region Properties

        ICollection<UserRoleEntity> UserRoles { get; set; }
        ICollection<RoleClaimEntity> RoleClaims { get; set; }

        #endregion

    }
}
