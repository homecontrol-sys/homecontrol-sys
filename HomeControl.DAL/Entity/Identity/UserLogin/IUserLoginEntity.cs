
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity.Identity
{
    public interface IUserLoginEntity : IEntityBaseModel
    {
        
        #region Properties

        UserEntity User { get; set; }

        #endregion

    }
}
