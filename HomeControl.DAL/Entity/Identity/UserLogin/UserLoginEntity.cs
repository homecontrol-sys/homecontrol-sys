using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.DAL.Entity.Identity
{
    [Table("UserLogin")]
    public class UserLoginEntity : IdentityUserLogin<Guid>, IUserLoginEntity
    {

        #region Properties
  
        public UserEntity User { get; set; }
        
        #region Entity
            
        public Guid Id { get; set; }

        #endregion

        #region Jurnalism

        public DateTime DateCreated { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByUserId { get; set; }
        public string Action { get; set; }

        #endregion

        #endregion

    }
}
