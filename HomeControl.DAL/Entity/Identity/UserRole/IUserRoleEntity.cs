using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity.Identity
{
    public interface IUserRoleEntity : IEntityBaseModel
    {
        
        #region Properties 

        UserEntity User { get; set; }
        RoleEntity Role { get; set; }

        #endregion 

    }
}
