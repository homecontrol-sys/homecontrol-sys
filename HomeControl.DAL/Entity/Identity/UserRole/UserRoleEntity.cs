using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.DAL.Entity.Identity
{

    [Table("UserRole")]
    public class UserRoleEntity : IdentityUserRole<Guid>, IUserRoleEntity
    {

        #region Properties

        public virtual UserEntity User { get; set; }
        public virtual RoleEntity Role { get; set; }

        #region Entiy

        public Guid Id { get; set; }

        #endregion

        #region Jurnalism

        public DateTime DateCreated { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByUserId { get; set; }
        public string Action { get; set; }
            
        #endregion

        #endregion

        #region Data Seed

        public static List<UserRoleEntity> Seed()
        {   
            var actionName = "UserRoleEntitySeed";
            var userEntities = new List<UserRoleEntity>
            {
                new UserRoleEntity { 
                    UserId = UserEntity.SystemUserId, 
                    RoleId = RoleEntity.AdminRoleId,
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"System {actionName}"
                },
                new UserRoleEntity { 
                    UserId = UserEntity.AdminUserId, 
                    RoleId = RoleEntity.AdminRoleId ,
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"Admin {actionName}"
                }
            };

            return userEntities;
        }

        #endregion

    }

}
