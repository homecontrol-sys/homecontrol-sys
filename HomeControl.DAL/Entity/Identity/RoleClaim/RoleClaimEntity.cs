using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.DAL.Entity.Identity
{

    [Table("RoleClaim")]
    public class RoleClaimEntity : IdentityRoleClaim<Guid>, IRoleClaimEntity
    {

        #region Properties

        public virtual RoleEntity Role { get; set; }

        #region Entity

        new public Guid Id { get; set;}

        #endregion

        #region Jurnalism
            
        public DateTime DateCreated { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByUserId { get; set; }
        public string Action { get; set; }

        #endregion

        #endregion
    }
    
}
