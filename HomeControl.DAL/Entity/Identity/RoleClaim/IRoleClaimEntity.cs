using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity.Identity
{
    public interface IRoleClaimEntity : IEntityBaseModel
    {

        #region Properties

        RoleEntity Role { get; set; }

        #endregion

    }
}
