using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.DAL.Entity.Identity
{

    [Table("UserClaim")]
    public class UserClaimEntity : IdentityUserClaim<Guid>, IUserClaimEntity
    {

        #region Properties

        public UserEntity User { get; set; }

        #region Entity
            
        new public Guid Id { get; set; }

        #endregion

        #region Jurnalism

        public DateTime DateCreated { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime DateUpdated { get; set; }
        public Guid UpdatedByUserId { get; set; }
        public string Action { get; set; }

        #endregion

        #endregion
    }
}
