using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity.Identity
{
    public interface IUserTokenEntity : IEntityBaseModel
    {

        #region Properties

        UserEntity User { get; set; }

        #endregion

    }
}
