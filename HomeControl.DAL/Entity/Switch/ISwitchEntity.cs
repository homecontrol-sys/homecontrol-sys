using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface ISwitchEntity : ILookupBaseModel
    {
        
        #region Properties

        string Alias { get; set; }
        int Dilay { get; set; }
        
        ICollection<ElementEntity> Elements { get; set; }
        ICollection<SwitchStateEntity> SwitchStates { get; set; }

        #endregion

    }
}
