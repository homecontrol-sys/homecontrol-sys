using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;
using HomeControl.DAL.Entity.Identity;

namespace HomeControl.DAL.Entity
{
    [Table("Switch")]
    public class SwitchEntity : LookupBaseModel, ISwitchEntity
    {

        #region Fields

        public static Guid OneTimeSwitchId = new Guid("d237a75a-d6df-4987-a663-8e81876caa4b");
        public static Guid ToggleSwitchId = new Guid("89f697f4-b6b3-4f44-9407-9606470d1f26");

        #endregion
        
        #region Properties
        
        [Required]
        public string Alias { get; set; }
        public int Dilay { get; set; }

        public ICollection<ElementEntity> Elements { get; set; }
        public ICollection<SwitchStateEntity> SwitchStates { get; set; }

        #endregion

        #region Methods

        #region Data Seed

        public static List<SwitchEntity> Seed()
        {   
            var entities = new List<SwitchEntity>();
            entities.AddRange(SwitchData());

            return entities;
        }

        private static List<SwitchEntity> SwitchData(){
            var actionName = "SwitchEntityDataSeed";
            var dataEntities = new List<SwitchEntity>
            {
                new SwitchEntity {
                    Id = ToggleSwitchId,
                    Name = "Toggle Switch",
                    Description = "Sets value true-false",
                    Alias = "toggle-switch",
                    Abrv = "toggle-switch",
                    Dilay = 0,
                    SortOrder = 1,
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new SwitchEntity {
                    Id = OneTimeSwitchId,
                    Name = "One Time Switch",
                    Description = "Sets value of the switch one time until reset",
                    Alias = "one-time-switch",
                    Abrv = "one-time-switch",
                    Dilay = 0,
                    SortOrder = 2,
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
            };

            return dataEntities;
        }

        #endregion

        #endregion

    }
}
