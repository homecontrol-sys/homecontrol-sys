using System;
using System.ComponentModel.DataAnnotations;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface IPinEntity : IEntityBaseModel
    {
        
        #region Properties
        
        string Name { get; set; }
        string Description { get; set; }
        int Number { get; set; }

        Guid PinTypeId { get; set; }
        PinTypeEntity PinType { get; set; }
        Guid? PinConnectionId { get; set; }
        PinConnectionEntity PinConnection { get; set; }
        Guid? DeviceId { get; set; }
        DeviceEntity Device { get; set; }
        Guid? ModuleId { get; set; }
        ModuleEntity Module { get; set; }
        Guid? ElementId { get; set; }
        ElementEntity Element { get; set; }

        #endregion

    }
}
