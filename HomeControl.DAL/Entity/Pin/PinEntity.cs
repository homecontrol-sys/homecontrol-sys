using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;
using HomeControl.DAL.Entity.Identity;

namespace HomeControl.DAL.Entity
{
    [Table("Pin")]
    public class PinEntity : EntityBaseModel, IPinEntity
    {

        #region Fields

        #region Raspberry Pins

        #region Power Pins

        public static Guid RaspberryThreeVoltPowerPinFirstId = new Guid("d44b093a-9b00-4bf1-952e-3ed182ce2733");
        public static Guid RaspberryThreeVoltPowerPinSecondId = new Guid("9ee170e9-1de1-4073-943d-6c82b42b3361");
        public static Guid RaspberryFiveVoltPowerPinFirstnId = new Guid("4f51bd3f-c0b0-4a29-a616-28a2ce50eb5e");
        public static Guid RaspberryFiveVoltPowerPinSecondId = new Guid("9de68590-81dc-4f92-ad63-9439e0d7ad21");

        #endregion

        #region Ground Pins

        public static Guid RaspberryGroundPinFirstId = new Guid("268e656b-0a6d-47fb-b433-75590ee90d47");
        public static Guid RaspberryGroundPinSecondId = new Guid("624ea535-418c-4cf9-94ab-689156a8cf63");
        public static Guid RaspberryGroundPinThirdId = new Guid("434b6742-ce21-4969-b6c3-bb04c9ddb8af");
        public static Guid RaspberryGroundPinFourthId = new Guid("50fcc20c-dd8d-4bc8-a6e1-95b1314c68a5");
        public static Guid RaspberryGroundPinFifthId = new Guid("192e4e09-cbaf-42e7-ad08-80091e7ccaa9");
        public static Guid RaspberryGroundPinSixthId = new Guid("0cfeffad-f35f-4c3c-a018-d97f458f58e9");
        public static Guid RaspberryGroundPinSeventhId = new Guid("6933ba79-fe21-4067-9a67-210086bcdcbd");
        public static Guid RaspberryGroundPinEighthId = new Guid("503d64af-771f-468e-906b-eeea1646d9cf");

        #endregion

        #region UART Pins

        public static Guid RaspberryUARTPinFirstId = new Guid("980747a7-aa8d-4074-842f-cba1ad85d28f");
        public static Guid RaspberryUARTPinSecondId = new Guid("f7cd90e2-fb52-4773-99f3-4b1b0961fa1b");

        #endregion

        #region I2C Pins

        public static Guid RaspberryI2CPinFirstId = new Guid("410d1a95-0f49-4a73-a384-11c8953c7d66");
        public static Guid RaspberryI2CPinSecondId = new Guid("cad639ee-9749-4e8a-87b0-d5a112316bc1");
        public static Guid RaspberryI2CPinThirdId = new Guid("e9ac262c-4c98-4027-a184-7a8c584ef956");
        public static Guid RaspberryI2CPinFourthId = new Guid("31656042-6006-462f-8f51-58308db28aee");

        #endregion

        #region GPIO Pins

        public static Guid RaspberryGPIOPinFirstId = new Guid("9c00dd8d-c8ce-4985-ab2e-d7358013cba5");
        public static Guid RaspberryGPIOPinSecondId = new Guid("98843e93-85f9-4a24-8f37-65a869bb329e");
        public static Guid RaspberryGPIOPinThirdId = new Guid("1daf1971-077f-4d70-a0e7-3bc2afe6021f");
        public static Guid RaspberryGPIOPinFourthId = new Guid("4dc1b76c-dddb-4952-9ed2-172736552b38");
        public static Guid RaspberryGPIOPinFifthId = new Guid("d72e548b-7919-4377-8501-fc0da4b81b1e");
        public static Guid RaspberryGPIOPinSixthId = new Guid("c86d366d-d736-45da-a951-e87cebfc86ba");
        public static Guid RaspberryGPIOPinSeventhId = new Guid("59d191a1-b0e0-4ea1-82e9-98ad42b9e895");
        public static Guid RaspberryGPIOPinEighthId = new Guid("7cdd34e0-0961-4242-919b-36c3f5fb7964");
        public static Guid RaspberryGPIOPinNinthId = new Guid("098c8c78-332e-48cf-8b53-20a8221181ec");
        public static Guid RaspberryGPIOPinTenthId = new Guid("de372710-23a4-40b1-b1dc-db42cbfb306b");
        public static Guid RaspberryGPIOPinEleventhId = new Guid("6fd1e7ae-2aca-4e23-bb31-3143b7ae8de5");

        #endregion

        #region SPI Pins

        public static Guid RaspberrySPIPinFirstId = new Guid("d52c4744-876b-42b4-be40-d0be0c37d323");
        public static Guid RaspberrySPIPinSecondId = new Guid("74f82f77-4bc7-4036-8704-fc278efcc651");
        public static Guid RaspberrySPIPinThirdId = new Guid("5cf80ef0-5c53-438e-91b6-ee7ff6b192e4");
        public static Guid RaspberrySPIPinFourthId = new Guid("f5a7c7c7-50fe-458b-889a-098c86c0b116");
        public static Guid RaspberrySPIPinFifthId = new Guid("5784402b-b537-49a0-95e4-14e42af47854");
        public static Guid RaspberrySPIPinSixthId = new Guid("eb1f9be4-d1b8-4851-9d2d-48f4923f27f5");
        public static Guid RaspberrySPIPinSeventhId = new Guid("4b470ab3-7dc9-45f8-b86c-141fea863158");
        public static Guid RaspberrySPIPinEighthId = new Guid("72de2155-fd19-4575-bf5e-678f5ab69e78");
        public static Guid RaspberrySPIPinNinthId = new Guid("1a81b9b0-f0ac-44ae-ad4b-a108cd6a7305");
        public static Guid RaspberrySPIPinTenthId = new Guid("0584e4eb-e2a6-4150-964a-992a92216944");
        public static Guid RaspberrySPIPinEleventhId = new Guid("e324d7dc-85df-40ec-88f7-a012925998e7");

        #endregion

        #endregion

        #region Relay Pins

        public static Guid RelayPlusPinId = new Guid("61ade986-5d4d-4ae0-b313-2a9a41b01a3d");
        public static Guid RelayMinusPinId = new Guid("5c856ffb-6289-4c88-b345-8eb8a65d62e5");
        public static Guid RelaySignalPinId = new Guid("63fdc5b8-d5f8-464f-b126-5bf3d339ca6e");

        #endregion

        #endregion

        #region Properties

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int Number { get; set; }

        public Guid PinTypeId { get; set; }
        public PinTypeEntity PinType { get; set; }
        public Guid? PinConnectionId { get; set; }
        public PinConnectionEntity PinConnection { get; set; }
        public Guid? DeviceId { get; set; }
        public DeviceEntity Device { get; set; }
        public Guid? ModuleId { get; set; }
        public ModuleEntity Module { get; set; }
        public Guid? ElementId { get; set; }
        public ElementEntity Element { get; set; }

        #endregion

        #region Methods

        #region Data Seed

        public static List<PinEntity> Seed()
        {   
            var entities = new List<PinEntity>();
            entities.AddRange(PinData());

            return entities;
        }

        
        private static List<PinEntity> PinData(){
            var actionName = "PinEntityDataSeed";
            var dataEntities = new List<PinEntity>();

            #region Raspbery Pins Data

            var raspberyPinEntities = new List<PinEntity>
            {
                new PinEntity {
                    Id = RaspberryThreeVoltPowerPinFirstId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.PowerPinTypeId,
                    Name = "3.3V",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryThreeVoltPowerPinSecondId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.PowerPinTypeId,
                    Name = "3.3V",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryFiveVoltPowerPinFirstnId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.PowerPinTypeId,
                    Name = "5V",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryFiveVoltPowerPinSecondId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.PowerPinTypeId,
                    Name = "5V",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGroundPinFirstId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GroundPinTypeId,
                    Name = "Ground",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGroundPinSecondId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GroundPinTypeId,
                    Name = "Ground",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGroundPinThirdId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GroundPinTypeId,
                    Name = "Ground",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGroundPinFourthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GroundPinTypeId,
                    Name = "Ground",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGroundPinFifthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GroundPinTypeId,
                    Name = "Ground",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGroundPinSixthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GroundPinTypeId,
                    Name = "Ground",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGroundPinSeventhId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GroundPinTypeId,
                    Name = "Ground",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGroundPinEighthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GroundPinTypeId,
                    Name = "Ground",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryUARTPinFirstId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.UARTPinTypeId,
                    Name = "GPIO 14 (UART TX)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryUARTPinSecondId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.UARTPinTypeId,
                    Name = "GPIO 15 (UART RX)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryI2CPinFirstId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.I2CPinTypeId,
                    Name = "GPIO 2 Serial Data (I2C)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryI2CPinSecondId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.I2CPinTypeId,
                    Name = "GPIO 3 Serial Clock (I2C)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryI2CPinThirdId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.I2CPinTypeId,
                    Name = "GPIO 0 EEPROM Serial DATA (I2C)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryI2CPinFourthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.I2CPinTypeId,
                    Name = "GPIO 1 EEPROM Serial Clock (I2C)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinFirstId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 4",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinSecondId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 27",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinThirdId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 22",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinFourthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 23",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinFifthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 24",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinSixthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 25",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinSeventhId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 5",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinEighthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 6",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinNinthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 12 (PWM)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinTenthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 13 (PWM)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberryGPIOPinEleventhId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.GPIOPinTypeId,
                    Name = "GPIO 26",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinFirstId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "PIO 17 Chip Enable-CE1 (SPI1)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinSecondId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "PIO 18 Chip Enable-CE0 (SPI1) [PWM]",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinThirdId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "GPIO 10 MOSI (SPI 0)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinFourthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "GPIO 09 MISO (SPI 0)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinFifthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "GPIO 11 SCLK (SPI 0)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinSixthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "GPIO 8 Chip Enable-CE0 (SPI0)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinSeventhId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "GPIO 7 Chip Enable-CE1 (SPI0)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinEighthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "[PWM] GPIO 19 MISO (SPI 1)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinNinthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "GPIO 16 Chip Enable-CE2 (SPI 1)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinTenthId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "GPIO 20 MISO (SPI 1)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RaspberrySPIPinEleventhId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    PinTypeId = PinTypeEntity.SPIPinTypeId,
                    Name = "GPIO 21 SCLK (SPI 1)",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
            };

            #endregion

            #region Relay Module Pins Data

            var relayModulePinEntities = new List<PinEntity>
            {
                new PinEntity {
                    Id = RelayPlusPinId,
                    ModuleId = ModuleEntity.RaspberyRelayModuleId,
                    PinTypeId = PinTypeEntity.PlusPinTypeId,
                    Name = "+",
                    Description = "Plus",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RelayMinusPinId,
                    ModuleId = ModuleEntity.RaspberyRelayModuleId,
                    PinTypeId = PinTypeEntity.MinusPinTypeId,
                    Name = "-",
                    Description = "Minus",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinEntity {
                    Id = RelaySignalPinId,
                    ModuleId = ModuleEntity.RaspberyRelayModuleId,
                    PinTypeId = PinTypeEntity.SignalPinTypeId,
                    Name = "S",
                    Description = "Signal",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
            };

            #endregion

            dataEntities.AddRange(raspberyPinEntities);
            dataEntities.AddRange(relayModulePinEntities);

            return dataEntities;
        }

        #endregion

        #endregion
    
    }
}
