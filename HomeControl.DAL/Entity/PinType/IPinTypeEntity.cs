using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface IPinTypeEntity : IEntityBaseModel
    {
        
        #region Properties
        
        string Name { get; set; }
        string Description { get; set; }

        ICollection<PinEntity> Pins { get; set; }

        #endregion

    }
}
