using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;
using HomeControl.DAL.Entity.Identity;

namespace HomeControl.DAL.Entity
{
    [Table("PinType")]
    public class PinTypeEntity : EntityBaseModel, IPinTypeEntity
    {

        #region Fields

        public static Guid PlusPinTypeId = new Guid("1a29a788-ad02-4d54-af61-52b972a6dd8a"); 
        public static Guid MinusPinTypeId = new Guid("ad5d451e-c3f1-4d23-971f-a3cc1e27d9a9"); 
        public static Guid SignalPinTypeId = new Guid("93d98542-06ac-4254-84d7-f47666955cad"); 
        public static Guid PowerPinTypeId = new Guid("b9f11336-0424-416e-becd-dee984a515e9"); 
        public static Guid GroundPinTypeId = new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"); 
        public static Guid GPIOPinTypeId = new Guid("84558164-6fd7-44d4-b164-7a31b961fab7");
        public static Guid I2CPinTypeId = new Guid("e9d304b9-dac6-4657-9db6-4f0ffaf393a4"); 
        public static Guid UARTPinTypeId = new Guid("5219f966-3c7d-4e0d-8b68-56cad71911b4"); 
        public static Guid SPIPinTypeId = new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"); 


        #endregion

        #region Properties

        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<PinEntity> Pins { get; set; }

        #endregion

        #region Methods

        #region Data Seed

        public static List<PinTypeEntity> Seed()
        {   
            var entities = new List<PinTypeEntity>();
            entities.AddRange(PinTypeData());

            return entities;
        }

        private static List<PinTypeEntity> PinTypeData(){
            var actionName = "PinTypeEntityDataSeed";
            var dataEntities = new List<PinTypeEntity>
            {
                new PinTypeEntity {
                    Id = PlusPinTypeId,
                    Name = "Plus",
                    Description = "Plus Pin", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinTypeEntity {
                    Id = MinusPinTypeId,
                    Name = "Minus",
                    Description = "Minus Pin", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinTypeEntity {
                    Id = SignalPinTypeId,
                    Name = "Signal",
                    Description = "Signal Pin", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinTypeEntity {
                    Id = PowerPinTypeId,
                    Name = "Power",
                    Description = "Power Pin", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinTypeEntity {
                    Id = GroundPinTypeId,
                    Name = "Ground",
                    Description = "Ground Pin", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinTypeEntity {
                    Id = GPIOPinTypeId,
                    Name = "GPIO",
                    Description = "GPIO Pin", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinTypeEntity {
                    Id = I2CPinTypeId,
                    Name = "I2C Pin",
                    Description = "relay-module", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinTypeEntity {
                    Id = UARTPinTypeId,
                    Name = "UART Pin",
                    Description = "relay-module", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new PinTypeEntity {
                    Id = SPIPinTypeId,
                    Name = "SPI",
                    Description = "SPI Pin", 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
            };

            return dataEntities;
        }

        #endregion

        #endregion
    
    }
}
