using System;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface ISwitchStateEntity : ILookupBaseModel
    {
        
        #region Properties
        
        int Value { get; set; }
        string Alias { get; set; }

        Guid SwitchId { get; set; }
        SwitchEntity Switch { get; set; }
        Guid ElementId { get; set; }
        ElementEntity Element { get; set; }

        #endregion

    }
}
