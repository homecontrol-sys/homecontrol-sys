using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;
using HomeControl.DAL.Entity.Identity;

namespace HomeControl.DAL.Entity
{
    [Table("SwitchState")]
    public class SwitchStateEntity : LookupBaseModel, ISwitchStateEntity
    {

        #region Fields

        public static Guid ToggleSwitchStateOnId = new Guid("cd2a3593-1d31-45fc-820c-26f989f3e60d");
        public static Guid ToggleSwitchStateOffId = new Guid("f9093de9-69e7-4f23-b8cf-b73e7e2f732b");

        #endregion

        #region Properties
        
        [Required]
        [DefaultValue(0)]
        public int Value { get; set; }
        public string Alias { get; set; }
        
        public Guid SwitchId { get; set; }
        public SwitchEntity Switch { get; set; }
        public Guid ElementId { get; set; }
        public ElementEntity Element { get; set; }

        #endregion

        #region Methods

        #region Data Seed

        public static List<SwitchStateEntity> Seed()
        {   
            var entities = new List<SwitchStateEntity>();
            entities.AddRange(ElementStateData());

            return entities;
        }

        private static List<SwitchStateEntity> ElementStateData(){
            var actionName = "SwitchStateEntityDataSeed";
            var dataEntities = new List<SwitchStateEntity>
            {           
                new SwitchStateEntity {
                    Id = ToggleSwitchStateOffId,
                    SwitchId = SwitchEntity.ToggleSwitchId,
                    Name = "Off",
                    Description = "Sets element or device state to off",
                    Alias = "off",
                    Abrv = "off",
                    Value = 0,
                    SortOrder = 1,
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
                new SwitchStateEntity {
                    Id = ToggleSwitchStateOnId,
                    SwitchId = SwitchEntity.ToggleSwitchId,
                    Name = "On",
                    Description = "Sets element or device state to on",
                    Alias = "on",
                    Abrv = "on",
                    Value = 1,
                    SortOrder = 2,
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
            };

            return dataEntities;
        }

        #endregion

        #endregion

    }
}
