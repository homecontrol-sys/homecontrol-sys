using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;
using HomeControl.DAL.Entity.Identity;

namespace HomeControl.DAL.Entity
{
    [Table("System")]
    public class SystemEntity : EntityBaseModel, ISystemEntity
    {

        #region Fields

        public static Guid CoreSystemId = new Guid("5e308ab7-f736-4178-80d6-bc9f0bc0ed33");
        public static Guid LightSystemId = new Guid("78e445c9-c646-49be-9e12-69426eff1a4e");

        #endregion

        #region Properties

        public string Name { get; set; }
        public string Description { get; set; }
        [DefaultValue(false)]
        public bool IsMain { get; set; }
        
        [DefaultValue(null)]
        public Guid? NetworkConfigurationId { get; set; }
        public NetworkConfigurationEntity NetworkConfiguration { get; set; }
        public ICollection<ElementEntity> Elements { get; set; }
        public ICollection<SystemDeviceEntity> SystemDevices { get; set; }

        #endregion

        #region Methods

        #region Data Seed

        public static List<ISystemEntity> Seed()
        {   
            var systemEntities = new List<ISystemEntity>();
            
            systemEntities.AddRange(SystemData());
            // systemEntities.AddRange(SystemTestData());

            return systemEntities;
        }

        private static List<ISystemEntity> SystemData(){
            var actionName = "SystemEntityDataSeed";
            var systemDataEntities = new List<ISystemEntity>
            {
                new SystemEntity { 
                    Id = CoreSystemId,
                    IsMain = true, 
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = actionName,
                    Name = "",
                    Description = "",
                },

                new SystemEntity { 
                    Id = LightSystemId,
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = actionName,
                    Name = "Light System",
                    Description = "Light System Description",
                },
            };

            return systemDataEntities;
        }

        private static List<ISystemEntity> SystemTestData(){
            var actionName = "SystemEntityTestDataSeed";
            var systemTestEntities = new List<ISystemEntity>();

            for (var i = 0; i < 10 ; i++){
                systemTestEntities.Add(
                    new SystemEntity { 
                        Id = Guid.NewGuid(),
                        IsMain = false, 
                        DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                        DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                        Action = actionName,
                        Name = $"SystemTest {i}",
                        Description = $"SystemTest {i} Description",
                    });
            }

            return systemTestEntities;
        }

        #endregion

        #endregion

    }
}
