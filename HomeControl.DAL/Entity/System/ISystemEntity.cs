using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface ISystemEntity : IEntityBaseModel
    {

        #region Properties

        string Name { get; set; }
        string Description { get; set; }
        bool IsMain { get; set; }

        Guid? NetworkConfigurationId { get; set; }
        NetworkConfigurationEntity NetworkConfiguration { get; set; }
        ICollection<ElementEntity> Elements { get; set; }
        ICollection<SystemDeviceEntity> SystemDevices { get; set; }

        #endregion

    }
}
