using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;

namespace HomeControl.DAL.Entity
{
    public interface IModuleEntity : IEntityBaseModel
    {
        
        #region Properties
        
        string Name { get; set; }

        Guid DeviceId { get; set; }
        DeviceEntity Device { get; set; }
        ICollection<ElementEntity> Elements { get; set; }
        ICollection<PinEntity> Pins { get; set; }

        #endregion

    }
}
