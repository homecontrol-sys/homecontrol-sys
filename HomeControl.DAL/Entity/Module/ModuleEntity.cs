using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Project.Common.Architecture.DAL;
using HomeControl.DAL.Entity.Identity;

namespace HomeControl.DAL.Entity
{
    [Table("Module")]
    public class ModuleEntity : EntityBaseModel, IModuleEntity
    {

        #region Fields

        public static Guid RaspberyRelayModuleId = new Guid("545e88a8-eec8-4f8d-8f1f-5478fc63644f");

        #endregion

        #region Properties

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public Guid DeviceId { get; set; }
        public DeviceEntity Device { get; set; }
        public ICollection<ElementEntity> Elements { get; set; }
        public ICollection<PinEntity> Pins { get; set; }

        #endregion

        #region Methods

        #region Data Seed

        public static List<ModuleEntity> Seed()
        {   
            var entities = new List<ModuleEntity>();
            entities.AddRange(ModuleData());

            return entities;
        }

        private static List<ModuleEntity> ModuleData(){
            var actionName = "ModuleEntityDataSeed";
            var moduleDataEntities = new List<ModuleEntity>

            {
                new ModuleEntity {
                    Id = RaspberyRelayModuleId,
                    DeviceId = DeviceEntity.RaspberyrPiThreeBPlusId,
                    Name = "Relay",
                    Description = "Relay Module",
                    DateCreated = DateTime.Now, CreatedByUserId = UserEntity.SystemUserId,
                    DateUpdated = DateTime.Now, UpdatedByUserId = UserEntity.SystemUserId,
                    Action = $"{actionName}"
                },
            };

            return moduleDataEntities;
        }

        #endregion

        #endregion
    
    }
}
