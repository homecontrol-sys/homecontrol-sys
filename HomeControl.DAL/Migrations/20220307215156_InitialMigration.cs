﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace HomeControl.DAL.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Device",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Device", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NetworkConfiguration",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SSID = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    IsConnectionVerified = table.Column<bool>(type: "boolean", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NetworkConfiguration", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PinConnection",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    IsTested = table.Column<bool>(type: "boolean", nullable: false),
                    IsValid = table.Column<bool>(type: "boolean", nullable: false),
                    StartPinId = table.Column<Guid>(type: "uuid", nullable: false),
                    EndPinId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PinConnection", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PinType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PinType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Abrv = table.Column<string>(type: "text", nullable: true),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Switch",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Alias = table.Column<string>(type: "text", nullable: false),
                    Dilay = table.Column<int>(type: "integer", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false),
                    Abrv = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    SortOrder = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Switch", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    IsAuthenticated = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordSalt = table.Column<string>(type: "text", nullable: true),
                    PasswordModifiedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LastLoginDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Status = table.Column<short>(type: "smallint", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: true),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Module",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    DeviceId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Module", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Module_Device_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Device",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "System",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    IsMain = table.Column<bool>(type: "boolean", nullable: false),
                    NetworkConfigurationId = table.Column<Guid>(type: "uuid", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System", x => x.Id);
                    table.ForeignKey(
                        name: "FK_System_NetworkConfiguration_NetworkConfigurationId",
                        column: x => x.NetworkConfigurationId,
                        principalTable: "NetworkConfiguration",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaim",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId1 = table.Column<Guid>(type: "uuid", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: true),
                    RoleId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaim_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleClaim_Role_RoleId1",
                        column: x => x.RoleId1,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SwitchState",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Value = table.Column<int>(type: "integer", nullable: false),
                    Alias = table.Column<string>(type: "text", nullable: true),
                    SwitchId = table.Column<Guid>(type: "uuid", nullable: false),
                    ElementId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false),
                    Abrv = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    SortOrder = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SwitchState", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SwitchState_Switch_SwitchId",
                        column: x => x.SwitchId,
                        principalTable: "Switch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserClaim",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId1 = table.Column<Guid>(type: "uuid", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaim_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserClaim_User_UserId1",
                        column: x => x.UserId1,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserLogin",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    ProviderKey = table.Column<string>(type: "text", nullable: false),
                    UserId1 = table.Column<Guid>(type: "uuid", nullable: true),
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: true),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogin", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogin_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserLogin_User_UserId1",
                        column: x => x.UserId1,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    RoleId = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId1 = table.Column<Guid>(type: "uuid", nullable: true),
                    RoleId1 = table.Column<Guid>(type: "uuid", nullable: true),
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRole_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRole_Role_RoleId1",
                        column: x => x.RoleId1,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRole_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRole_User_UserId1",
                        column: x => x.UserId1,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserToken",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    UserId1 = table.Column<Guid>(type: "uuid", nullable: true),
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: true),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserToken", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserToken_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserToken_User_UserId1",
                        column: x => x.UserId1,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SystemDevice",
                columns: table => new
                {
                    SystemId = table.Column<Guid>(type: "uuid", nullable: false),
                    DeviceId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemDevice", x => new { x.SystemId, x.DeviceId });
                    table.ForeignKey(
                        name: "FK_SystemDevice_Device_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Device",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SystemDevice_System_SystemId",
                        column: x => x.SystemId,
                        principalTable: "System",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Element",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Alias = table.Column<string>(type: "text", nullable: true),
                    SystemId = table.Column<Guid>(type: "uuid", nullable: false),
                    DeviceId = table.Column<Guid>(type: "uuid", nullable: true),
                    ModuleId = table.Column<Guid>(type: "uuid", nullable: true),
                    SwitchId = table.Column<Guid>(type: "uuid", nullable: false),
                    SwitchStateId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Element", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Element_Device_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Device",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Element_Module_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Element_Switch_SwitchId",
                        column: x => x.SwitchId,
                        principalTable: "Switch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Element_SwitchState_SwitchStateId",
                        column: x => x.SwitchStateId,
                        principalTable: "SwitchState",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Element_System_SystemId",
                        column: x => x.SystemId,
                        principalTable: "System",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pin",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Number = table.Column<int>(type: "integer", nullable: false),
                    PinTypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    PinConnectionId = table.Column<Guid>(type: "uuid", nullable: true),
                    DeviceId = table.Column<Guid>(type: "uuid", nullable: true),
                    ModuleId = table.Column<Guid>(type: "uuid", nullable: true),
                    ElementId = table.Column<Guid>(type: "uuid", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateUpdated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedByUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Action = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pin", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pin_Device_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Device",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pin_Element_ElementId",
                        column: x => x.ElementId,
                        principalTable: "Element",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pin_Module_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pin_PinConnection_PinConnectionId",
                        column: x => x.PinConnectionId,
                        principalTable: "PinConnection",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pin_PinType_PinTypeId",
                        column: x => x.PinTypeId,
                        principalTable: "PinType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Device",
                columns: new[] { "Id", "Action", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "Name", "UpdatedByUserId" },
                values: new object[,]
                {
                    { new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), "DeviceEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 357, DateTimeKind.Local).AddTicks(5063), new DateTime(2022, 3, 7, 22, 51, 51, 369, DateTimeKind.Local).AddTicks(6292), "Main System Device", "Raspberry Pi", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("706b4881-d0a0-4154-9b0d-5f09b8617d04"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(1974), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2089), "Raspberry Pi Device0", "Raspberry Pi0", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("412201cd-5999-4478-92a5-5fd8dedb39c8"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2216), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2238), "Raspberry Pi Device1", "Raspberry Pi1", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("3eb70962-a9b2-43be-b5ae-04f3b74ff38b"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2334), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2353), "Raspberry Pi Device2", "Raspberry Pi2", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("83898cde-ed28-4ac9-8314-6aaef73fa8c8"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2406), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2426), "Raspberry Pi Device3", "Raspberry Pi3", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("ee2cf6d4-ea60-445b-bc9f-4e8cbde01fce"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2477), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2497), "Raspberry Pi Device4", "Raspberry Pi4", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("6337de0c-c86f-417d-8614-8fefa676114d"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2590), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2609), "Raspberry Pi Device5", "Raspberry Pi5", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("bb496e9f-b9a3-43f4-a1f2-bf2b1d130a7f"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2662), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2698), "Raspberry Pi Device6", "Raspberry Pi6", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("f260ad9d-499a-42c8-b531-9df9f10955fb"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2778), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2798), "Raspberry Pi Device7", "Raspberry Pi7", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("230fb3bf-c711-4607-b143-111789886d4e"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2866), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2885), "Raspberry Pi Device8", "Raspberry Pi8", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("a00606d2-4554-4027-8e56-0b0d567005a7"), "DeviceEntityTestDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2944), new DateTime(2022, 3, 7, 22, 51, 51, 372, DateTimeKind.Local).AddTicks(2963), "Raspberry Pi Device9", "Raspberry Pi9", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") }
                });

            migrationBuilder.InsertData(
                table: "PinType",
                columns: new[] { "Id", "Action", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "Name", "UpdatedByUserId" },
                values: new object[,]
                {
                    { new Guid("93d98542-06ac-4254-84d7-f47666955cad"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(1949), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(1961), "Signal Pin", "Signal", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("b9f11336-0424-416e-becd-dee984a515e9"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(1976), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(1988), "Power Pin", "Power", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2002), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2013), "Ground Pin", "Ground", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2050), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2060), "GPIO Pin", "GPIO", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("e9d304b9-dac6-4657-9db6-4f0ffaf393a4"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2075), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2085), "relay-module", "I2C Pin", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("1a29a788-ad02-4d54-af61-52b972a6dd8a"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(1588), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(1723), "Plus Pin", "Plus", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2125), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2136), "SPI Pin", "SPI", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("ad5d451e-c3f1-4d23-971f-a3cc1e27d9a9"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(1919), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(1932), "Minus Pin", "Minus", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("5219f966-3c7d-4e0d-8b68-56cad71911b4"), "PinTypeEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2100), new DateTime(2022, 3, 7, 22, 51, 51, 477, DateTimeKind.Local).AddTicks(2111), "relay-module", "UART Pin", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") }
                });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "Abrv", "Action", "ConcurrencyStamp", "CreatedByUserId", "DateCreated", "DateUpdated", "Level", "Name", "NormalizedName", "UpdatedByUserId" },
                values: new object[] { new Guid("7950da96-a257-444f-b344-5cc3c6dc5bd7"), "admin", "RoleEntitySeed", "062967bb-a724-4ae8-80c6-18fc188cff1a", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 483, DateTimeKind.Local).AddTicks(118), new DateTime(2022, 3, 7, 22, 51, 51, 483, DateTimeKind.Local).AddTicks(4881), 1, "Admin", null, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") });

            migrationBuilder.InsertData(
                table: "Switch",
                columns: new[] { "Id", "Abrv", "Action", "Alias", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "Dilay", "Name", "SortOrder", "UpdatedByUserId" },
                values: new object[,]
                {
                    { new Guid("89f697f4-b6b3-4f44-9407-9606470d1f26"), "toggle-switch", "ElementStateEntityDataSeed", "toggle-switch", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 418, DateTimeKind.Local).AddTicks(7336), new DateTime(2022, 3, 7, 22, 51, 51, 418, DateTimeKind.Local).AddTicks(7499), "Sets value true-false", 0, "Toggle Switch", 1, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("d237a75a-d6df-4987-a663-8e81876caa4b"), "one-time-switch", "ElementStateEntityDataSeed", "one-time-switch", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 418, DateTimeKind.Local).AddTicks(7749), new DateTime(2022, 3, 7, 22, 51, 51, 418, DateTimeKind.Local).AddTicks(7775), "Sets value of the switch one time until reset", 0, "One Time Switch", 2, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") }
                });

            migrationBuilder.InsertData(
                table: "System",
                columns: new[] { "Id", "Action", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "IsMain", "Name", "NetworkConfigurationId", "UpdatedByUserId" },
                values: new object[,]
                {
                    { new Guid("78e445c9-c646-49be-9e12-69426eff1a4e"), "SystemEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 479, DateTimeKind.Local).AddTicks(9349), new DateTime(2022, 3, 7, 22, 51, 51, 479, DateTimeKind.Local).AddTicks(9402), "Light System Description", false, "Light System", null, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("5e308ab7-f736-4178-80d6-bc9f0bc0ed33"), "SystemEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 479, DateTimeKind.Local).AddTicks(6058), new DateTime(2022, 3, 7, 22, 51, 51, 479, DateTimeKind.Local).AddTicks(6177), "", true, "", null, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "AccessFailedCount", "Action", "ConcurrencyStamp", "CreatedByUserId", "DateCreated", "DateUpdated", "Email", "EmailConfirmed", "FirstName", "IsAuthenticated", "LastLoginDate", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PasswordModifiedDate", "PasswordSalt", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Status", "TwoFactorEnabled", "UpdatedByUserId", "UserName" },
                values: new object[,]
                {
                    { new Guid("9ebab9b1-70ad-4ac0-b195-b11f64f7a1ce"), 0, "Admin8 UserEntityTestDataSeed", "5b5d7ed2-576f-4f2b-b96b-2afc2b061ada", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 511, DateTimeKind.Local).AddTicks(229), new DateTime(2022, 3, 7, 22, 51, 51, 511, DateTimeKind.Local).AddTicks(300), null, false, null, false, null, null, false, null, null, null, "9d8c92f94dfc83818245501756afcfb5ca850ebd488a9b0bc195f1c026d98306e13a9c86aa423ca1c2e87c9e0f187bd465306930c25b596ff4e23be21b6037b0", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test8" },
                    { new Guid("a00047a0-323b-484d-ae33-b1fae933b9fc"), 0, "Admin7 UserEntityTestDataSeed", "f942a680-8e46-4fee-846c-53af5999979b", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(8848), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(8880), null, false, null, false, null, null, false, null, null, null, "cee1ffdc30e05765a4b478378902339d7ebe426574d04eaf04a4c943cb9b499dcaeb7665fd5c3bd39bcd92945442a3687d901c406a2d8eb956d54ec757d33a76", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test7" },
                    { new Guid("2bf839f0-09b6-4c4a-a1ce-180e9098a50f"), 0, "Admin6 UserEntityTestDataSeed", "371f2a33-6b6a-4530-be67-8e7186bb90f8", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(8163), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(8235), null, false, null, false, null, null, false, null, null, null, "30634fc2dd28e412a684771875fd805747fb3bd9760a085efb554a0a4233223a4d98f6da2336179ca33d1170e6e27b18be4588978c35115a43aef6ddc226c808", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test6" },
                    { new Guid("832b22fc-6351-4719-81e0-8310c6b677e4"), 0, "Admin5 UserEntityTestDataSeed", "af654d18-82f4-42c7-88c7-22f5b52be141", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(6767), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(6836), null, false, null, false, null, null, false, null, null, null, "64c26ffe3b35c65dfb93a8fd9a91828c57ed76d3809d06b03e17128125b48e5d01b37bb605a0a0305eff8341fbd56096664597f5cd091bf036e4ca31b304a9cc", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test5" },
                    { new Guid("d49de7c0-7cfb-48ac-ba9b-c9770cee6585"), 0, "Admin3 UserEntityTestDataSeed", "1d591ca6-0ab9-4342-8ca7-6eb0aa092a65", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(5230), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(5321), null, false, null, false, null, null, false, null, null, null, "cb872de2b8d2509c54344435ce9cb43b4faa27f97d486ff4de35af03e4919fb4ec53267caf8def06ef177d69fe0abab3c12fbdc2f267d895fd07c36a62bff4bf", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test3" },
                    { new Guid("d1011a3d-d8a7-4ca9-93de-68debfdd349d"), 0, "Admin2 UserEntityTestDataSeed", "03ec4532-7125-423f-856b-0a111fcdaa07", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(3513), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(3580), null, false, null, false, null, null, false, null, null, null, "6d201beeefb589b08ef0672dac82353d0cbd9ad99e1642c83a1601f3d647bcca003257b5e8f31bdc1d73fbec84fb085c79d6e2677b7ff927e823a54e789140d9", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test2" },
                    { new Guid("3c892c8d-5ba1-4f2c-83cb-e5cd2208b27c"), 0, "Admin1 UserEntityTestDataSeed", "38fa5ef5-b890-4ae3-acb2-941fda2b3a44", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(2645), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(2683), null, false, null, false, null, null, false, null, null, null, "b16ed7d24b3ecbd4164dcdad374e08c0ab7518aa07f9d3683f34c2b3c67a15830268cb4a56c1ff6f54c8e54a795f5b87c08668b51f82d0093f7baee7d2981181", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test1" },
                    { new Guid("f2eebd92-b098-4aa2-b9be-87aa4d52f11d"), 0, "Admin0 UserEntityTestDataSeed", "d6c6740c-6c0c-4713-91f9-0df78ccdbe23", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(1419), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(1579), null, false, null, false, null, null, false, null, null, null, "a4589ae194a3eeeb99409b2287cc8ce5ae2fec1929935f2d0efc58606eb2575d3d036b7b116d360ce9325387df9014c2c5ea5273c8086de4d3454b634a4232a4", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test0" },
                    { new Guid("5e5aa47c-dd83-4523-a514-ce092cb9c30f"), 0, "Admin UserEntityDataSeed", "608fcfea-f0d5-45d8-a36f-b3f9f8311bf5", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 507, DateTimeKind.Local).AddTicks(2484), new DateTime(2022, 3, 7, 22, 51, 51, 507, DateTimeKind.Local).AddTicks(2604), null, false, null, false, null, null, false, null, null, null, "c6001d5b2ac3df314204a8f9d7a00e1503c9aba0fd4538645de4bf4cc7e2555cfe9ff9d0236bf327ed3e907849a98df4d330c4bea551017d465b4c1d9b80bcb0", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "admin" },
                    { new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), 0, "System UserEntityDataSeed", "c3021bad-dcfc-4015-9a1d-18f1bf2a8159", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 505, DateTimeKind.Local).AddTicks(652), new DateTime(2022, 3, 7, 22, 51, 51, 505, DateTimeKind.Local).AddTicks(9040), null, false, null, false, null, null, false, null, null, null, "c6001d5b2ac3df314204a8f9d7a00e1503c9aba0fd4538645de4bf4cc7e2555cfe9ff9d0236bf327ed3e907849a98df4d330c4bea551017d465b4c1d9b80bcb0", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "system" },
                    { new Guid("780386ab-7e74-4c4d-a5ae-e255f8b0d568"), 0, "Admin9 UserEntityTestDataSeed", "c86100b1-32b9-4431-af6d-ef0e638c9a2d", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 511, DateTimeKind.Local).AddTicks(1194), new DateTime(2022, 3, 7, 22, 51, 51, 511, DateTimeKind.Local).AddTicks(1261), null, false, null, false, null, null, false, null, null, null, "0b3d1be846a4812b7043157e6e95d720341bff4dc1e437dd26ba96cec8735c9c74ebcf770cb0fe45dd3e4d04cd59b026240d540db03abf10b1f6e2f4dafcadce", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test9" },
                    { new Guid("f14865fe-75fe-4347-8bb0-115b64871c62"), 0, "Admin4 UserEntityTestDataSeed", "cc1c16c0-9ca4-4323-a4a5-222f23cd424b", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(6056), new DateTime(2022, 3, 7, 22, 51, 51, 510, DateTimeKind.Local).AddTicks(6087), null, false, null, false, null, null, false, null, null, null, "2257aab44b42813142aa8ac4767116ad5bd41e94a79aa0672cc962128ed4809f50ed38d35ba945a80799976c9efa9b686f28d18036134bc2bb0ac2de96ec6280", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, null, (short)0, false, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "test4" }
                });

            migrationBuilder.InsertData(
                table: "Module",
                columns: new[] { "Id", "Action", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "DeviceId", "Name", "UpdatedByUserId" },
                values: new object[] { new Guid("545e88a8-eec8-4f8d-8f1f-5478fc63644f"), "ModuleEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 429, DateTimeKind.Local).AddTicks(6045), new DateTime(2022, 3, 7, 22, 51, 51, 429, DateTimeKind.Local).AddTicks(6201), "Relay Module", new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), "Relay", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") });

            migrationBuilder.InsertData(
                table: "Pin",
                columns: new[] { "Id", "Action", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "DeviceId", "ElementId", "ModuleId", "Name", "Number", "PinConnectionId", "PinTypeId", "UpdatedByUserId" },
                values: new object[,]
                {
                    { new Guid("6fd1e7ae-2aca-4e23-bb31-3143b7ae8de5"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1129), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1140), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 26", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("410d1a95-0f49-4a73-a384-11c8953c7d66"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(735), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(746), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 2 Serial Data (I2C)", 0, null, new Guid("e9d304b9-dac6-4657-9db6-4f0ffaf393a4"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("cad639ee-9749-4e8a-87b0-d5a112316bc1"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(763), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(774), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 3 Serial Clock (I2C)", 0, null, new Guid("e9d304b9-dac6-4657-9db6-4f0ffaf393a4"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("e9ac262c-4c98-4027-a184-7a8c584ef956"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(789), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(802), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 0 EEPROM Serial DATA (I2C)", 0, null, new Guid("e9d304b9-dac6-4657-9db6-4f0ffaf393a4"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("31656042-6006-462f-8f51-58308db28aee"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(826), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(837), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 1 EEPROM Serial Clock (I2C)", 0, null, new Guid("e9d304b9-dac6-4657-9db6-4f0ffaf393a4"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("980747a7-aa8d-4074-842f-cba1ad85d28f"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(679), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(690), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 14 (UART TX)", 0, null, new Guid("5219f966-3c7d-4e0d-8b68-56cad71911b4"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("f7cd90e2-fb52-4773-99f3-4b1b0961fa1b"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(707), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(718), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 15 (UART RX)", 0, null, new Guid("5219f966-3c7d-4e0d-8b68-56cad71911b4"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("d52c4744-876b-42b4-be40-d0be0c37d323"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1157), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1168), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "PIO 17 Chip Enable-CE1 (SPI1)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("de372710-23a4-40b1-b1dc-db42cbfb306b"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1102), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1113), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 13 (PWM)", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("74f82f77-4bc7-4036-8704-fc278efcc651"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1184), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1195), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "PIO 18 Chip Enable-CE0 (SPI1) [PWM]", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("f5a7c7c7-50fe-458b-889a-098c86c0b116"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1239), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1250), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 09 MISO (SPI 0)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("5784402b-b537-49a0-95e4-14e42af47854"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1271), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1284), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 11 SCLK (SPI 0)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("eb1f9be4-d1b8-4851-9d2d-48f4923f27f5"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1301), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1312), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 8 Chip Enable-CE0 (SPI0)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("4b470ab3-7dc9-45f8-b86c-141fea863158"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1328), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1339), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 7 Chip Enable-CE1 (SPI0)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("72de2155-fd19-4575-bf5e-678f5ab69e78"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1554), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1567), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "[PWM] GPIO 19 MISO (SPI 1)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("1a81b9b0-f0ac-44ae-ad4b-a108cd6a7305"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1585), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1596), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 16 Chip Enable-CE2 (SPI 1)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("0584e4eb-e2a6-4150-964a-992a92216944"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1613), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1624), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 20 MISO (SPI 1)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("e324d7dc-85df-40ec-88f7-a012925998e7"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1641), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1652), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 21 SCLK (SPI 1)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("5cf80ef0-5c53-438e-91b6-ee7ff6b192e4"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1211), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1223), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 10 MOSI (SPI 0)", 0, null, new Guid("e7345581-ff24-4bf6-8731-dfeb07ec86c2"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("098c8c78-332e-48cf-8b53-20a8221181ec"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1075), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1086), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 12 (PWM)", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("7cdd34e0-0961-4242-919b-36c3f5fb7964"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1048), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1059), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 6", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("50fcc20c-dd8d-4bc8-a6e1-95b1314c68a5"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(267), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(278), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "Ground", 0, null, new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("4f51bd3f-c0b0-4a29-a616-28a2ce50eb5e"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(104), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(117), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "5V", 0, null, new Guid("b9f11336-0424-416e-becd-dee984a515e9"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("9de68590-81dc-4f92-ad63-9439e0d7ad21"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(133), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(144), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "5V", 0, null, new Guid("b9f11336-0424-416e-becd-dee984a515e9"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("268e656b-0a6d-47fb-b433-75590ee90d47"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(161), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(173), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "Ground", 0, null, new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("624ea535-418c-4cf9-94ab-689156a8cf63"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(212), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(223), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "Ground", 0, null, new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("434b6742-ce21-4969-b6c3-bb04c9ddb8af"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(239), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(250), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "Ground", 0, null, new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("59d191a1-b0e0-4ea1-82e9-98ad42b9e895"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1020), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1031), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 5", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("192e4e09-cbaf-42e7-ad08-80091e7ccaa9"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(555), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(568), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "Ground", 0, null, new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("9ee170e9-1de1-4073-943d-6c82b42b3361"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(73), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(85), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "3.3V", 0, null, new Guid("b9f11336-0424-416e-becd-dee984a515e9"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("0cfeffad-f35f-4c3c-a018-d97f458f58e9"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(595), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(606), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "Ground", 0, null, new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("503d64af-771f-468e-906b-eeea1646d9cf"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(652), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(663), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "Ground", 0, null, new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("9c00dd8d-c8ce-4985-ab2e-d7358013cba5"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(853), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(864), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 4", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("98843e93-85f9-4a24-8f37-65a869bb329e"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(880), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(891), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 27", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("1daf1971-077f-4d70-a0e7-3bc2afe6021f"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(907), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(918), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 22", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("4dc1b76c-dddb-4952-9ed2-172736552b38"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(935), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(946), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 23", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("d72e548b-7919-4377-8501-fc0da4b81b1e"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(964), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(975), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 24", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("c86d366d-d736-45da-a951-e87cebfc86ba"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(993), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(1004), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "GPIO 25", 0, null, new Guid("84558164-6fd7-44d4-b164-7a31b961fab7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("6933ba79-fe21-4067-9a67-210086bcdcbd"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(623), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(634), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "Ground", 0, null, new Guid("92a6bfe9-733a-48d7-af7e-e5510498cc5c"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("d44b093a-9b00-4bf1-952e-3ed182ce2733"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 469, DateTimeKind.Local).AddTicks(9741), new DateTime(2022, 3, 7, 22, 51, 51, 469, DateTimeKind.Local).AddTicks(9888), null, new Guid("1cd6d865-c652-4fc1-9476-d38d7303556b"), null, null, "3.3V", 0, null, new Guid("b9f11336-0424-416e-becd-dee984a515e9"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") }
                });

            migrationBuilder.InsertData(
                table: "SwitchState",
                columns: new[] { "Id", "Abrv", "Action", "Alias", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "ElementId", "Name", "SortOrder", "SwitchId", "UpdatedByUserId", "Value" },
                values: new object[,]
                {
                    { new Guid("f9093de9-69e7-4f23-b8cf-b73e7e2f732b"), "off", "SwitchStateEntityDataSeed", "off", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 425, DateTimeKind.Local).AddTicks(7189), new DateTime(2022, 3, 7, 22, 51, 51, 425, DateTimeKind.Local).AddTicks(7452), "Sets element or device state to off", new Guid("00000000-0000-0000-0000-000000000000"), "Off", 1, new Guid("89f697f4-b6b3-4f44-9407-9606470d1f26"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), 0 },
                    { new Guid("cd2a3593-1d31-45fc-820c-26f989f3e60d"), "on", "SwitchStateEntityDataSeed", "on", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 425, DateTimeKind.Local).AddTicks(7683), new DateTime(2022, 3, 7, 22, 51, 51, 425, DateTimeKind.Local).AddTicks(7704), "Sets element or device state to on", new Guid("00000000-0000-0000-0000-000000000000"), "On", 2, new Guid("89f697f4-b6b3-4f44-9407-9606470d1f26"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), 1 }
                });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId", "Action", "CreatedByUserId", "DateCreated", "DateUpdated", "Id", "RoleId1", "UpdatedByUserId", "UserId1" },
                values: new object[,]
                {
                    { new Guid("7950da96-a257-444f-b344-5cc3c6dc5bd7"), new Guid("5e5aa47c-dd83-4523-a514-ce092cb9c30f"), "Admin UserRoleEntitySeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 516, DateTimeKind.Local).AddTicks(2985), new DateTime(2022, 3, 7, 22, 51, 51, 516, DateTimeKind.Local).AddTicks(3069), new Guid("00000000-0000-0000-0000-000000000000"), null, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), null },
                    { new Guid("7950da96-a257-444f-b344-5cc3c6dc5bd7"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), "System UserRoleEntitySeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 515, DateTimeKind.Local).AddTicks(1481), new DateTime(2022, 3, 7, 22, 51, 51, 515, DateTimeKind.Local).AddTicks(6427), new Guid("00000000-0000-0000-0000-000000000000"), null, new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), null }
                });

            migrationBuilder.InsertData(
                table: "Element",
                columns: new[] { "Id", "Action", "Alias", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "DeviceId", "ModuleId", "Name", "SwitchId", "SwitchStateId", "SystemId", "UpdatedByUserId" },
                values: new object[] { new Guid("e8474c0c-40e5-4c40-9f6b-25ceb7c7e456"), "ElementEntityDataSeed", "light-bulb", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 382, DateTimeKind.Local).AddTicks(838), new DateTime(2022, 3, 7, 22, 51, 51, 382, DateTimeKind.Local).AddTicks(1000), "Light bulb description", null, null, "Light Bulb", new Guid("89f697f4-b6b3-4f44-9407-9606470d1f26"), new Guid("f9093de9-69e7-4f23-b8cf-b73e7e2f732b"), new Guid("78e445c9-c646-49be-9e12-69426eff1a4e"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") });

            migrationBuilder.InsertData(
                table: "Pin",
                columns: new[] { "Id", "Action", "CreatedByUserId", "DateCreated", "DateUpdated", "Description", "DeviceId", "ElementId", "ModuleId", "Name", "Number", "PinConnectionId", "PinTypeId", "UpdatedByUserId" },
                values: new object[,]
                {
                    { new Guid("61ade986-5d4d-4ae0-b313-2a9a41b01a3d"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(4992), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(5054), "Plus", null, null, new Guid("545e88a8-eec8-4f8d-8f1f-5478fc63644f"), "+", 0, null, new Guid("1a29a788-ad02-4d54-af61-52b972a6dd8a"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("5c856ffb-6289-4c88-b345-8eb8a65d62e5"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(5106), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(5118), "Minus", null, null, new Guid("545e88a8-eec8-4f8d-8f1f-5478fc63644f"), "-", 0, null, new Guid("ad5d451e-c3f1-4d23-971f-a3cc1e27d9a9"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") },
                    { new Guid("63fdc5b8-d5f8-464f-b126-5bf3d339ca6e"), "PinEntityDataSeed", new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8"), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(5138), new DateTime(2022, 3, 7, 22, 51, 51, 470, DateTimeKind.Local).AddTicks(5150), "Signal", null, null, new Guid("545e88a8-eec8-4f8d-8f1f-5478fc63644f"), "S", 0, null, new Guid("93d98542-06ac-4254-84d7-f47666955cad"), new Guid("4db89f0a-2051-41c2-adb8-e95f27b8b5e8") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Element_DeviceId",
                table: "Element",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_Element_ModuleId",
                table: "Element",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_Element_SwitchId",
                table: "Element",
                column: "SwitchId");

            migrationBuilder.CreateIndex(
                name: "IX_Element_SwitchStateId",
                table: "Element",
                column: "SwitchStateId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Element_SystemId",
                table: "Element",
                column: "SystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Module_DeviceId",
                table: "Module",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_Pin_DeviceId",
                table: "Pin",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_Pin_ElementId",
                table: "Pin",
                column: "ElementId");

            migrationBuilder.CreateIndex(
                name: "IX_Pin_ModuleId",
                table: "Pin",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_Pin_PinConnectionId",
                table: "Pin",
                column: "PinConnectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Pin_PinTypeId",
                table: "Pin",
                column: "PinTypeId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Role",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaim_RoleId",
                table: "RoleClaim",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaim_RoleId1",
                table: "RoleClaim",
                column: "RoleId1");

            migrationBuilder.CreateIndex(
                name: "IX_SwitchState_SwitchId",
                table: "SwitchState",
                column: "SwitchId");

            migrationBuilder.CreateIndex(
                name: "IX_System_NetworkConfigurationId",
                table: "System",
                column: "NetworkConfigurationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SystemDevice_DeviceId",
                table: "SystemDevice",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "User",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "User",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_UserId",
                table: "UserClaim",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_UserId1",
                table: "UserClaim",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogin_UserId",
                table: "UserLogin",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogin_UserId1",
                table: "UserLogin",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_RoleId",
                table: "UserRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_RoleId1",
                table: "UserRole",
                column: "RoleId1");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_UserId1",
                table: "UserRole",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_UserToken_UserId1",
                table: "UserToken",
                column: "UserId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pin");

            migrationBuilder.DropTable(
                name: "RoleClaim");

            migrationBuilder.DropTable(
                name: "SystemDevice");

            migrationBuilder.DropTable(
                name: "UserClaim");

            migrationBuilder.DropTable(
                name: "UserLogin");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "UserToken");

            migrationBuilder.DropTable(
                name: "Element");

            migrationBuilder.DropTable(
                name: "PinConnection");

            migrationBuilder.DropTable(
                name: "PinType");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Module");

            migrationBuilder.DropTable(
                name: "SwitchState");

            migrationBuilder.DropTable(
                name: "System");

            migrationBuilder.DropTable(
                name: "Device");

            migrationBuilder.DropTable(
                name: "Switch");

            migrationBuilder.DropTable(
                name: "NetworkConfiguration");
        }
    }
}
