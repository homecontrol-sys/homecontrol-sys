﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace HomeControl.DAL
{
    public class HomeControlDbContextFactory : IDesignTimeDbContextFactory<HomeControlDbContext>
    {
        public HomeControlDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<HomeControlDbContext>();
            string connectionString = "Server=database;Port=5432;Database=homecontrol_db;UserID=admin;Password=admin;";
            optionsBuilder.UseNpgsql(connectionString);

            return new HomeControlDbContext(optionsBuilder.Options);
        }

    }
}
