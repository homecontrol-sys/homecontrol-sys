using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HomeControl.DAL
{
    public static partial class DataAccessLayerProvider
    {
        public static IServiceCollection ProvideDataAccessLayer(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<HomeControlDbContext>(options =>
                options.UseNpgsql(config.GetConnectionString("HomeControlDB")));

            return services;
        }
    }
}
