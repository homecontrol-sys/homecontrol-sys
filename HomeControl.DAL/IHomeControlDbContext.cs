using API.Project.Common.Architecture.DAL.EntityFramework;
using HomeControl.DAL.Entity;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.DAL
{
    public interface IHomeControlDbContext : IDbContextBase
    {
        
        #region Properties

        DbSet<DeviceEntity> Devices { get; set; }
        DbSet<ElementEntity> Elements { get; set; }
        DbSet<SwitchEntity> Switches { get; set; }
        DbSet<SwitchStateEntity> SwitchStates { get; set; }
        DbSet<ModuleEntity> Modules { get; set; }
        DbSet<NetworkConfigurationEntity> NetworkConfigurations { get; set; }
        DbSet<PinEntity> Pins { get; set; }
        DbSet<PinConnectionEntity> PinConnections { get; set; }
        DbSet<PinTypeEntity> PinTypes { get; set; }
        DbSet<SystemEntity> Systems { get; set; }
        DbSet<SystemDeviceEntity> SystemDevices { get; set; }

        #endregion

    }
}
