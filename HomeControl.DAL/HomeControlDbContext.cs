using System;
using System.Threading.Tasks;
using HomeControl.DAL._Extension;
using HomeControl.DAL.Entity;
using HomeControl.DAL.Entity.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Polly;

namespace HomeControl.DAL
{
    public class HomeControlDbContext : IdentityDbContext<UserEntity, RoleEntity, Guid, 
    UserClaimEntity, UserRoleEntity, UserLoginEntity, RoleClaimEntity, UserTokenEntity>, 
    IHomeControlDbContext
    {

        #region Constructor
        
        public HomeControlDbContext(DbContextOptions options) : base(options) {}

        #endregion

        #region Properties     

        public DbSet<DeviceEntity> Devices { get; set; }
        public DbSet<ElementEntity> Elements { get; set; }
        public DbSet<SwitchEntity> Switches { get; set; }
        public DbSet<SwitchStateEntity> SwitchStates { get; set; }
        public DbSet<ModuleEntity> Modules { get; set; }
        public DbSet<NetworkConfigurationEntity> NetworkConfigurations { get; set; }
        public DbSet<PinEntity> Pins { get; set; }
        public DbSet<PinConnectionEntity> PinConnections { get; set; }
        public DbSet<PinTypeEntity> PinTypes { get; set; }
        public DbSet<SystemEntity> Systems { get; set; }
        public DbSet<SystemDeviceEntity> SystemDevices { get; set; }

        #endregion
      
        #region Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            #region Microsoft Identity

            //Identity, keys of Identity tables are mapped in OnModelCreating method of IdentityDbContext
            base.OnModelCreating(modelBuilder);

            #endregion

            modelBuilder.SetTableAndColumnNames();
            modelBuilder.DecribeRelationships();
            modelBuilder.SeedData();
        }

        public void MigrateDB()
        {
            Policy
                .Handle<Exception>()
                .WaitAndRetry(10, r => TimeSpan.FromSeconds(10))
                .Execute(() => Database.Migrate());
        }

        public override int SaveChanges()
        {     
            return base.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }

        #endregion
    }
}
