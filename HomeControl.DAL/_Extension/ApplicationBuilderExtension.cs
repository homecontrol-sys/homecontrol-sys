﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace HomeControl.DAL._Extension
{
    public static class ApplicationBuilderExtension 
    {

        public static void AutoMigrateDB(this IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetService<HomeControlDbContext>().MigrateDB();
            }
        }

    }
}
