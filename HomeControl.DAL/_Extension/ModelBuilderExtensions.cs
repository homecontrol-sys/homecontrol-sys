﻿using System;
using HomeControl.DAL.Entity;
using HomeControl.DAL.Entity.Identity;
using Microsoft.EntityFrameworkCore;

namespace HomeControl.DAL._Extension
{
    public static class ModelBuilderExtension
    {

        #region TABLE AND COLUMS 

        public static void SetTableAndColumnNames(this ModelBuilder modelBuilder)
        {

            #region IDENTITY entities

            #region User

            modelBuilder.Entity<UserEntity>(u =>
            {
                //change identity table name 'AspNetUsers' to 'User'
                u.ToTable("User");
            });

            
            modelBuilder.Entity<UserClaimEntity>(uc =>
            {
                //change identity table name 'AspNetUserClaims' to 'UserClaim'
                uc.ToTable("UserClaim");
            });

            modelBuilder.Entity<UserRoleEntity>(ur =>
            {
                //change identity table name 'AspNetUserRoles' to 'UserRole'
                ur.ToTable("UserRole");
            });

            modelBuilder.Entity<UserTokenEntity>(ut =>
            {
                //change identity table name 'AspNetUserTokens' to 'UserToken'
                ut.ToTable("UserToken");
            });

            modelBuilder.Entity<UserLoginEntity>(ul =>
            {
                //change identity table name 'AspNetUserLogins' to 'UserLogin'
                ul.ToTable("UserLogin");
            });


            #endregion

            #region Role

            modelBuilder.Entity<RoleEntity>(u =>
            {
                //change identity table name 'AspNetRoles' to 'Role'
                u.ToTable("Role");
            });

            modelBuilder.Entity<RoleClaimEntity>(u =>
            {
                //change identity table name 'AspNetRoleCliams' to 'RoleClaim'
                u.ToTable("RoleClaim");
            });

            #endregion

            #endregion

        }

        #endregion

        #region RELATIONS

        public static void DecribeRelationships(this ModelBuilder modelBuilder)
        {

            #region System Relations

           //System -> Device - many-to-many
            modelBuilder.Entity<SystemDeviceEntity>().HasKey(sd => new { sd.SystemId, sd.DeviceId });
            modelBuilder.Entity<SystemDeviceEntity>()
                .HasOne<SystemEntity>(sd => sd.System)
                .WithMany(s => s.SystemDevices)
                .HasForeignKey(sd => sd.SystemId);
            modelBuilder.Entity<SystemDeviceEntity>()
                .HasOne<DeviceEntity>(sd => sd.Device)
                .WithMany(d => d.SystemDevices)
                .HasForeignKey(sd => sd.DeviceId);
            
            //System -> Elements - one-to-many
            modelBuilder.Entity<SystemEntity>()
                .HasMany(c => c.Elements)
                .WithOne(e => e.System);

            //System -> NetworkConfiguration - one-to-one
            modelBuilder.Entity<SystemEntity>()
                .HasOne<NetworkConfigurationEntity>(s => s.NetworkConfiguration)
                .WithOne(nc => nc.System)
                .HasForeignKey<SystemEntity>(s => s.NetworkConfigurationId);

            #endregion

            #region Device Relations

            //Device -> Modules - one-to-many
            modelBuilder.Entity<DeviceEntity>()
                .HasMany(c => c.Modules)
                .WithOne(e => e.Device);

            //Device -> Pins - one-to-many
            modelBuilder.Entity<DeviceEntity>()
                .HasMany(c => c.Pins)
                .WithOne(e => e.Device);
            
            //Device -> Elements - one-to-many
            modelBuilder.Entity<DeviceEntity>()
                .HasMany(c => c.Elements)
                .WithOne(e => e.Device);

            #endregion

            #region Module Relations

            //Module -> Pins - one-to-many
            modelBuilder.Entity<ModuleEntity>()
                .HasMany(c => c.Pins)
                .WithOne(e => e.Module);

            //Module -> Element - one-to-many
            modelBuilder.Entity<ModuleEntity>()
                .HasMany(c => c.Elements)
                .WithOne(e => e.Module);

            #endregion

            #region Element Relations

            //Element -> SwitchState - one-to-one
            modelBuilder.Entity<ElementEntity>()
                .HasOne<SwitchStateEntity>(e => e.SwitchState)
                .WithOne(sss => sss.Element)
                .HasForeignKey<ElementEntity>(e => e.SwitchStateId)
                .IsRequired();
            

            #endregion

            #region State Switch Relations

            //Switch -> Element - one-to-many
            modelBuilder.Entity<SwitchEntity>()
                .HasMany(c => c.Elements)
                .WithOne(e => e.Switch)
                .IsRequired();

            //Switch -> SwitchState - one-to-many
            modelBuilder.Entity<SwitchEntity>()
                .HasMany(c => c.SwitchStates)
                .WithOne(e => e.Switch)
                .IsRequired();

            #endregion

            #region Pin Type Relations

            //PinType -> Pin - one-to-many
            modelBuilder.Entity<PinTypeEntity>()
                .HasMany(c => c.Pins)
                .WithOne(e => e.PinType)
                .IsRequired();

            #endregion

            #region Pin Type Relations

            //PinConnection -> Pin - one-to-many
            modelBuilder.Entity<PinConnectionEntity>()
                .HasMany(c => c.Pins)
                .WithOne(e => e.PinConnection);

            #endregion

            #region Identity Relations

            //     #region User Relations

            //     modelBuilder.Entity<UserEntity>(u =>
            //     {

            //         // Each User can have many UserClaims
            //         u.HasMany(u => u.Claims)
            //             .WithOne(u => u.User)
            //             .HasForeignKey(uc => uc.UserId)
            //             .IsRequired();

            //         // Each User can have many UserLogins
            //         u.HasMany(u => u.Logins)
            //             .WithOne(u => u.User)
            //             .HasForeignKey(ul => ul.UserId)
            //             .IsRequired();

            //         // Each User can have many UserTokens
            //         u.HasMany(u => u.Tokens)
            //             .WithOne(u => u.User)
            //             .HasForeignKey(ut => ut.UserId)
            //             .IsRequired();

            //         // Each User can have many entries in the UserRole join table
            //         u.HasMany(u => u.UserRoles)
            //             .WithOne(u => u.User)
            //             .HasForeignKey(ur => ur.UserId)
            //             .IsRequired();
            //     });

            //     #endregion

            //     #region Role Relations

            //     modelBuilder.Entity<RoleEntity>(r =>
            //     {
            //         // Each Role can have many entries in the UserRole join table
            //         r.HasMany(r => r.UserRoles)
            //             .WithOne(r => r.Role)
            //             .HasForeignKey(ur => ur.RoleId)
            //             .IsRequired();

            //         // Each Role can have many associated RoleClaims
            //         r.HasMany(r => r.RoleClaims)
            //             .WithOne(r => r.Role)
            //             .HasForeignKey(rc => rc.RoleId)
            //             .IsRequired();
            //     });

            //     #endregion

            #endregion

        }

        #endregion 

        #region SEEDS

        public static void SeedData(this ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<DeviceEntity>().HasData(DeviceEntity.Seed());
            modelBuilder.Entity<ElementEntity>().HasData(ElementEntity.Seed());
            modelBuilder.Entity<SwitchEntity>().HasData(SwitchEntity.Seed());
            modelBuilder.Entity<SwitchStateEntity>().HasData(SwitchStateEntity.Seed());
            modelBuilder.Entity<ModuleEntity>().HasData(ModuleEntity.Seed());
            modelBuilder.Entity<PinEntity>().HasData(PinEntity.Seed());
            modelBuilder.Entity<PinConnectionEntity>().HasData(PinConnectionEntity.Seed());
            modelBuilder.Entity<PinTypeEntity>().HasData(PinTypeEntity.Seed());
            modelBuilder.Entity<SystemEntity>().HasData(SystemEntity.Seed());

            #region Identity

            modelBuilder.Entity<RoleEntity>().HasData(RoleEntity.Seed());
            modelBuilder.Entity<UserEntity>().HasData(UserEntity.Seed());
            modelBuilder.Entity<UserRoleEntity>().HasData(UserRoleEntity.Seed());

            #endregion

        }

        #endregion
        
    }
}
