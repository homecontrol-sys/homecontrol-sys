using System.IO;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace API.Project.Common.Modules.Security._Extension
{
    public static class StringExtension
    {

        #region String VALIDATION

        #region Email
        
        public static bool IsValidEmail(this string email)
        {
            try {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch {
                return false;
            }
        }

        #endregion

        #endregion
    
        #region String HASHING

            #region SHA hashing

                #region SHA256

                    #region String to Hash

                    /// <summary>
                    /// Encrypts a string using the SHA256 (Secure Hash Algorithm) algorithm.
                    /// Details: http://www.itl.nist.gov/fipspubs/fip180-1.htm
                    /// This works in the same manner as MD5, providing however 256bit encryption.
                    /// </summary>
                    /// <param name="data">A string containing the data to encrypt.</param>
                    /// <returns>A string containing the string, encrypted with the SHA256 algorithm.</returns>
                    public static string SHA256Hash(this string data)
                    {
                        SHA256 sha = new SHA256Managed();
                        byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(data));
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (byte b in hash)
                        {
                            stringBuilder.AppendFormat("{0:x2}", b);
                        }
                        return stringBuilder.ToString();
                    }

                    #endregion

                    #region Validate

                    /// <summary>
                    /// encrypt input text using MD5 and compare it with
                    /// the stored encrypted text
                    /// </summary>
                    /// <param name="inputData">input text you will enterd to encrypt it</param>
                    /// <param name="storedHashData">the encrypted text stored on file or database ... etc</param>
                    /// <returns>true or false depending on input validation</returns>
                    public static bool ValidateSHA256Hash(this string inputData, string storedHashData)
                    {
                        //hash input text and save it string variable
                        string getHashInputData = SHA256Hash(inputData);
                        bool validationResult = (string.Compare(getHashInputData, storedHashData) == 0) ? true : false;
                        return validationResult;
                    }

                    #endregion

                #endregion

                #region SHA384

                    #region To String

                    /// <summary>
                    /// Encrypts a string using the SHA256 (Secure Hash Algorithm) algorithm.
                    /// Details: http://www.itl.nist.gov/fipspubs/fip180-1.htm
                    /// This works in the same manner as MD5, providing however 256bit encryption.
                    /// </summary>
                    /// <param name="data">A string containing the data to encrypt.</param>
                    /// <returns>A string containing the string, encrypted with the SHA256 algorithm.</returns>
                    public static string SHA384Hash(this string data)
                    {
                        SHA384 sha = new SHA384Managed();
                        byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(data));
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (byte b in hash)
                        {
                            stringBuilder.AppendFormat("{0:x2}", b);
                        }
                        return stringBuilder.ToString();
                    }

                    #endregion

                    #region Validate

                    /// <summary>
                    /// encrypt input text using MD5 and compare it with
                    /// the stored encrypted text
                    /// </summary>
                    /// <param name="inputData">input text you will enterd to encrypt it</param>
                    /// <param name="storedHashData">the encrypted text stored on file or database ... etc</param>
                    /// <returns>true or false depending on input validation</returns>
                    public static bool ValidateSHA384Hash(this string inputData, string storedHashData)
                    {
                        //hash input text and save it string variable
                        string getHashInputData = SHA384Hash(inputData);
                        bool validationResult = (string.Compare(getHashInputData, storedHashData) == 0) ? true : false;

                        return validationResult;
                    }

                    #endregion

                #endregion

                #region SHA512

                    #region To String

                    /// <summary>
                    /// Encrypts a string using the SHA256 (Secure Hash Algorithm) algorithm.
                    /// Details: http://www.itl.nist.gov/fipspubs/fip180-1.htm
                    /// This works in the same manner as MD5, providing however 256bit encryption.
                    /// </summary>
                    /// <param name="data">A string containing the data to encrypt.</param>
                    /// <returns>A string containing the string, encrypted with the SHA256 algorithm.</returns>
                    public static string SHA512Hash(this string data)
                    {
                        SHA512 sha = new SHA512Managed();
                        byte[] hash = sha.ComputeHash(Encoding.ASCII.GetBytes(data));
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (byte b in hash)
                        {
                            stringBuilder.AppendFormat("{0:x2}", b);
                        }
                        return stringBuilder.ToString();
                    }

                    #endregion

                    #region Validate

                    /// <summary>
                    /// encrypt input text using MD5 and compare it with
                    /// the stored encrypted text
                    /// </summary>
                    /// <param name="inputData">input text you will enterd to encrypt it</param>
                    /// <param name="storedHashData">the encrypted text stored on file or database ... etc</param>
                    /// <returns>true or false depending on input validation</returns>
                    public static bool ValidateSHA512Hash(this string inputData, string storedHashData)
                    {
                        //hash input text and save it string variable
                        string getHashInputData = SHA512Hash(inputData);
                        bool validationResult = (string.Compare(getHashInputData, storedHashData) == 0) ? true : false;
                        return validationResult;
                    }

                    #endregion

                #endregion

            #endregion
        
            #region MD5 hashing

                #region String 

                    #region To Hash

                    /// <summary>
                    /// Encrypts a string using the MD5 hash encryption algorithm.
                    /// Message Digest is 128-bit and is commonly used to verify data, by checking
                    /// the 'MD5 checksum' of the data. Information on MD5 can be found at:
                    ///
                    /// http://www.faqs.org/rfcs/rfc1321.html
                    /// </summary>
                    /// <param name="data">A string containing the data to encrypt.</param>
                    /// <returns>A string containing the string, encrypted with the MD5 hash.</returns>
                    public static string ToMD5Hash(string data)
                    {
                        MD5 md5 = new MD5CryptoServiceProvider();
                        byte[] hash = md5.ComputeHash(Encoding.ASCII.GetBytes(data));
                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (byte b in hash)
                        {
                            stringBuilder.AppendFormat("{0:x2}", b);
                        }

                        return stringBuilder.ToString();
                    }

                    #endregion

                    #region Validate
                    
                    /// <summary>
                    /// encrypt input text using MD5 and compare it with
                    /// the stored encrypted text
                    /// </summary>
                    /// <param name="inputData">input text you will enterd to encrypt it</param>
                    /// <param name="storedHashData">the encrypted text stored on file or database ... etc</param>
                    /// <returns>true or false depending on input validation</returns>
                    public static bool ValidateMD5Hash(string inputData, string storedHashData)
                    {
                        //hash input text and save it string variable
                        string getHashInputData = ToMD5Hash(inputData);
                        bool validationResult = (string.Compare(getHashInputData, storedHashData) == 0) ? true : false;

                        return validationResult;
                    }

                    #endregion

                #endregion

                #region File

                /// <summary>
                /// Computes an MD5 hash for the provided file.
                /// </summary>
                /// <param name="filename">The full path to the file</param>
                /// <returns>A hexadecimal encoded MD5 hash for the file.</returns>
                public static string MD5FileHash(string filename)
                {
                    using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                    {
                        using (MD5 md5 = new MD5CryptoServiceProvider())
                        {
                            byte[] hash = md5.ComputeHash(stream);
                            StringBuilder builder = new StringBuilder();
                            foreach (byte b in hash)
                            {
                                builder.AppendFormat("{0:x2}", b);
                            }
                            return builder.ToString();
                        }
                    }
                }

                #endregion

            #endregion

        #endregion

    }
}
