# Dependencies

## JWT Tokens
<i>Json Web Tokens package</i>
> dotnet add package System.IdentityModel.Tokens.Jwt

## JWT Barer Tokens
<i>For build Bearer tokens</i>
> dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer
