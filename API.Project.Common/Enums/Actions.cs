namespace API.Project.Common.Enums
{

    public enum ActionType {
        CRUD,
        Request,
        Response,
    }

    public enum ResourceAction {
        CREATE,
        READ,
        UPDATE,
        DELETE,
    }

}