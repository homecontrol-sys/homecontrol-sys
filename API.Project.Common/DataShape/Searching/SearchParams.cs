namespace API.Project.Common.Modules.DataShape
{

    public class SearchParams : ISearchParams 
    {

        #region Properties

        public string SearchQuery { get; set; }

        #endregion

    }

}