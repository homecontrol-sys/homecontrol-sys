namespace API.Project.Common.Modules.DataShape
{

    public interface ISearchParams 
    {
        
        #region Properties

        string SearchQuery { get; set; }

        #endregion

    }

}
