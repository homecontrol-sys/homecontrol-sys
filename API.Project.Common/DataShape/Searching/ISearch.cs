
namespace API.Project.Common.Modules.Base.DataShape
{

    public interface ISearch<TResource>
    {
        TResource SearchBy { get; set; }
    
    }

}