using System;
using System.Collections.Generic;

namespace API.Project.Common.Modules.Base.DataShape
{

    public interface IFilter<TResource>
    {

        TResource Filter {get; set;}
        // IEnumerable<Guid> Ids { get; set; }
    
    }

}