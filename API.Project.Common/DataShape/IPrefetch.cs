using System.Collections.Generic;

namespace API.Project.Common.Modules.Base.DataShape
{

    public interface IPrefetch<TRelations>
    {

        IEnumerable<TRelations> Prefetch { get; set; }
    
    }

}