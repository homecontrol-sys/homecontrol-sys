using System.Collections;

namespace API.Project.Common.Modules.Base.DataShape
{

    public interface IResourceParams<TRelations, TFilterResource, TSearchResource> 
    : IPrefetch<TRelations>, IFilter<TFilterResource>, ISearch<TSearchResource>
    {

    }

}