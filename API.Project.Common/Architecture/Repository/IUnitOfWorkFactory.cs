﻿using Microsoft.EntityFrameworkCore;

namespace API.Project.Common.Architecture.Repository
{
    public interface IUnitOfWorkFactory
    {
        
        #region Methods

        IUnitOfWork Create(DbContext dbContext);

        #endregion
        
    }
}
