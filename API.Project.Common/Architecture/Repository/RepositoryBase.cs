using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using API.Project.Common.Architecture.DAL;
using API.Project.Common.Enums;
using Microsoft.EntityFrameworkCore;

namespace API.Project.Common.Architecture.Repository
{
    public class RepositoryBase : IRepositoryBase 
    {

        #region Fields

        private DbContext _dbContext;

        #endregion

        #region Constructor
        
        public RepositoryBase(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #endregion

        #region Methods

        #region CRUD

        #region Create
    
        public async virtual Task<Guid> Create<TEntity>(TEntity entity) where TEntity : EntityBaseModel
        {
            if (entity == null) return Guid.Empty;  

            if (entity is IJurnalismBaseModel){
                HandleJournalismData(entity as IJurnalismBaseModel, ResourceAction.CREATE, Guid.NewGuid());
            }

            await _dbContext.Set<TEntity>().AddAsync(entity);
            bool isSuccess = await SaveDatabaseChangesAsync();

            return isSuccess ? entity.Id : Guid.Empty;
        }


        public async virtual Task<bool> Create<TEntity>(IEnumerable<TEntity> entities) where TEntity : EntityBaseModel
        {
            if (!(entities.Count() > 0)) return false;

            foreach (var entity in entities)
            {
                bool isSuccess = (await Create(entity)) != Guid.Empty;

                if(!isSuccess) return false;
            }

            return true;
        }

        #endregion

        #region Read 

        public async virtual Task<IEnumerable<TEntity>> Read<TEntity>() where TEntity : EntityBaseModel
        {
            var entities = await _dbContext.Set<TEntity>().ToListAsync();

            return entities;
        }

        public async virtual Task<TEntity> Read<TEntity>(Guid id) where TEntity : EntityBaseModel
        {
            var entity = await _dbContext.Set<TEntity>().FindAsync(id);

            return entity;
        }

        public virtual async Task<IEnumerable<TEntity>> Read<TEntity>(
                Expression<Func<TEntity, bool>> filter = null,
                Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                params Expression<Func<TEntity, object>>[] includeProperties
            )  where TEntity : EntityBaseModel
        {
            IQueryable<TEntity> query = _dbContext.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        #endregion

        #region Update 

        public virtual async Task<bool> Update<TEntity>(TEntity entity) where TEntity : EntityBaseModel
        {
            _dbContext.Set<TEntity>().Update(entity);
            
            return await SaveDatabaseChangesAsync();
        }

        public virtual async Task<bool> Update<TEntity>(IEnumerable<TEntity> entities) where TEntity : EntityBaseModel
        {
            if (!(entities.Count() > 0)) return false;

            foreach (var entity in entities)
            {
                _dbContext.Set<TEntity>().Update(entity);
            }

            return await SaveDatabaseChangesAsync();
        }

        #endregion

        #region Delete 

        public async virtual Task<bool> Delete<TEntity>(Guid id) where TEntity : EntityBaseModel
        {

            var entity = await _dbContext.Set<TEntity>().FindAsync(id);

            if (entity == null) return false;

            _dbContext.Set<TEntity>().Remove(entity);

            return true;
        }

        public async virtual Task<bool> Delete<TEntity>(IEnumerable<Guid> ids) where TEntity : EntityBaseModel
        {
            if (!(ids.Count() > 0)) return false;

            foreach (var id in ids)
            {
                bool result = await Delete<TEntity>(id);

                if (!result) return false;
            }

            return true;
        }

        #endregion      

        #endregion

        #region Others

        public async Task<IEnumerable<TEntity>> FindAsync<TEntity>
        (Expression<Func<TEntity, bool>> expression) where TEntity : EntityBaseModel
        {
            return await _dbContext.Set<TEntity>().Where(expression).ToListAsync();
        }

        public async Task<TEntity> SingleOrDefaultAsync<TEntity>
        (Expression<Func<TEntity, bool>> expression) where TEntity : EntityBaseModel
        {
            return await _dbContext.Set<TEntity>().SingleOrDefaultAsync(expression);
        }

        public IQueryable<TEntity> CreateQuery<TEntity>() where TEntity : EntityBaseModel
        {
            var queryableDbSet = _dbContext.Set<TEntity>() as IQueryable<TEntity>;;
            return queryableDbSet;
        }

        #endregion

        #region Private Methods

        private async Task<bool> SaveDatabaseChangesAsync()
        {
            var entries = _dbContext.ChangeTracker
                .Entries()
                .Where(e => e.Entity is EntityBaseModel 
                    && (e.State == EntityState.Added || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((EntityBaseModel)entityEntry.Entity).DateUpdated = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((EntityBaseModel)entityEntry.Entity).DateCreated = DateTime.Now;
                }
            }

            bool isChangeSuccess = await _dbContext.SaveChangesAsync() > 0; 
            return isChangeSuccess;
        }

        private void HandleJournalismData(IJurnalismBaseModel entity, ResourceAction action, Guid actionIssuer)
        {
            var actionName = $"{action}";
            var currentTime = DateTime.Now;

            switch (action)
            {
                case ResourceAction.CREATE:
                    entity.DateCreated = currentTime;
                    entity.CreatedByUserId = actionIssuer;
                    entity.DateUpdated = currentTime;
                    entity.UpdatedByUserId = actionIssuer;
                    entity.Action = actionName;
                    break;
                case ResourceAction.UPDATE:
                    entity.DateUpdated = currentTime;
                    entity.UpdatedByUserId = actionIssuer;
                    entity.Action = actionName;
                    break;
                default:
                    entity.DateUpdated = currentTime;
                    entity.UpdatedByUserId = actionIssuer;
                    entity.Action = actionName;
                    break;
            }
        }

        #endregion

        #endregion
    }
}