﻿using API.Project.Common.Architecture.DAL.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace API.Project.Common.Architecture.Repository
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        
        #region Methods

        public IUnitOfWork Create(DbContext dbContext)
        {
            return new UnitOfWork(dbContext);
        }

        #endregion
    
    }
}
