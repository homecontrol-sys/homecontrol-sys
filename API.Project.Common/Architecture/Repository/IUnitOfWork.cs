﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace API.Project.Common.Architecture.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        
        #region Methods

        Task BeginTransactionAsync(CancellationToken cancellationToken = default);
        Task CommitTransactionAsync(CancellationToken cancellationToken = default);
        Task RollbackTransactionAsync(CancellationToken cancellationToken = default);
        Task<int> CompleteAsync(CancellationToken cancellationToken = default);

        #endregion
        
    }
}
