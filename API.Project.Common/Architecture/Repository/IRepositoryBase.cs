using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using API.Project.Common.Architecture.DAL;

namespace API.Project.Common.Architecture.Repository
{
    public interface IRepositoryBase
    {
        #region Methods

        #region CRUD

        #region Create
    
        Task<Guid> Create<TEntity>(TEntity entity) where TEntity : EntityBaseModel;
        Task<bool> Create<TEntity>(IEnumerable<TEntity> entities) where TEntity : EntityBaseModel;

        #endregion

        #region Read 

        Task<TEntity> Read<TEntity>(Guid id) where TEntity : EntityBaseModel;
        Task<IEnumerable<TEntity>> Read<TEntity>() where TEntity : EntityBaseModel;
        Task<IEnumerable<TEntity>> Read<TEntity>(Expression<Func<TEntity, bool>> filter = null,
                Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                params Expression<Func<TEntity, object>>[] includeProperties)
                where TEntity : EntityBaseModel;

        #endregion

        #region Update 

        Task<bool> Update<TEntity>(TEntity entity) where TEntity : EntityBaseModel;
        Task<bool> Update<TEntity>(IEnumerable<TEntity> entities) where TEntity : EntityBaseModel;

        #endregion

        #region Delete 

        Task<bool> Delete<TEntity>(Guid id) where TEntity : EntityBaseModel;

        Task<bool> Delete<TEntity>(IEnumerable<Guid> ids) where TEntity : EntityBaseModel;

        #endregion

        #endregion 

        #region Others

        Task<IEnumerable<TEntity>> FindAsync<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : EntityBaseModel;
        Task<TEntity> SingleOrDefaultAsync<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : EntityBaseModel;

        #endregion   

        #endregion
    }
}