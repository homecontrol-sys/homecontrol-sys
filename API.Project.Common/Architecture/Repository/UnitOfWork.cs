﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace API.Project.Common.Architecture.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        
        #region Fields

        private readonly DbContext _dbContext;
        private IDbContextTransaction _transaction;
 
        #endregion

        #region Constructor

        public UnitOfWork(DbContext dbContext)
        {
            if (dbContext != null)
            {
                _dbContext = dbContext;
            }
            else
            {
                throw new ArgumentNullException("DbContext");
            }
        }

        #endregion

        #region Methods

        public async Task BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            _transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);
        }

        public async Task<int> CompleteAsync(CancellationToken cancellationToken = default)
        {
            return await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task CommitTransactionAsync(CancellationToken cancellationToken = default)
        {
            await _transaction.CommitAsync(cancellationToken);
        }
        
        public async Task RollbackTransactionAsync(CancellationToken cancellationToken = default)
        {
            await _transaction.RollbackAsync(cancellationToken);
        }

        public void Dispose()
        {
            _transaction?.Dispose();
            _dbContext.Dispose();
        }

        #endregion
    
    }
}
