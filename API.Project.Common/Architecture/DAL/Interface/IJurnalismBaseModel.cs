using System;

namespace API.Project.Common.Architecture.DAL
{
    public interface IJurnalismBaseModel
    {
        
        DateTime DateCreated { get; set; }
        Guid CreatedByUserId { get; set; }
        DateTime DateUpdated { get; set; }
        Guid UpdatedByUserId { get; set; }
        string Action { get; set; }
        
    }
}
