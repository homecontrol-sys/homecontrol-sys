namespace API.Project.Common.Architecture.DAL
{
    public interface ILookupBaseModel : IEntityBaseModel
    {

        #region Properties

        string Abrv { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        int SortOrder { get; set; }
        
        #endregion

    }
}
