﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Project.Common.Architecture.DAL
{
    public interface IEntityBaseModel : IJurnalismBaseModel
    {

        #region Record Identifier

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        Guid Id { get; set; }

        #endregion

    }
}
