﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Project.Common.Architecture.DAL
{
    public abstract class EntityBaseModel : JurnalismBaseModel, IEntityBaseModel
    {

        #region Identifier

		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    	public Guid Id { get; set; }

        #endregion

  	}
} 
