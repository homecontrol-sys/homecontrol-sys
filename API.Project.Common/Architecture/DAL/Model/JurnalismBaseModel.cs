using System;
using System.ComponentModel.DataAnnotations;

namespace API.Project.Common.Architecture.DAL
{
    public abstract class JurnalismBaseModel : IJurnalismBaseModel
    { 

        [Required]
        public DateTime DateCreated { get; set; } 
        [Required]      
        public Guid CreatedByUserId { get; set; }
        [Required]
        public DateTime DateUpdated { get; set; }
        [Required]
        public Guid UpdatedByUserId { get; set; }
        [Required]
        public string Action { get; set; }
    
    }
}
