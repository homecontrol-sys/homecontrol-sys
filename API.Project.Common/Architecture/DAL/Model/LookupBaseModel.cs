using System.ComponentModel.DataAnnotations;

namespace API.Project.Common.Architecture.DAL
{
    public abstract class LookupBaseModel : EntityBaseModel, ILookupBaseModel
    {
      
        [Required]
        public string Abrv { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int SortOrder { get; set; }

    }
}
