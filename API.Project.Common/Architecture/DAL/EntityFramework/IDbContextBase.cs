using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace API.Project.Common.Architecture.DAL.EntityFramework
{
    public interface IDbContextBase : IDisposable
    {

        #region Methods

        void MigrateDB();
        int SaveChanges();

        EntityEntry Entry(object entity);
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        #endregion

    }
}
