using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Polly;

namespace API.Project.Common.Architecture.DAL.EntityFramework
{
    public abstract class DbContextBase : DbContext, IDbContextBase
    {

        #region Constructor

        public DbContextBase() : base()
        {
            Database.EnsureCreated();
        }

        #endregion

        #region Methods

        public void MigrateDB()
        {
            Policy
                .Handle<Exception>()
                .WaitAndRetry(10, r => TimeSpan.FromSeconds(10))
                .Execute(() => Database.Migrate());
        }
        
        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }

        #endregion

    }
}
