using System.Net;

namespace API.Project.Common.Architecture.REST
{
    public interface IResponseMessage
    {

        #region Properties

        string Action { get; set; }
        string Text { get; set; }
        HttpStatusCode StatusCode { get; set; }
       // string GenerateMessage(APIActions action, string resource);

        #endregion

    }
}
