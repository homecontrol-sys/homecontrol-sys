using System.Net;

namespace API.Project.Common.Architecture.REST
{
    public interface IResponseBaseModel<TResponseObject>
    where TResponseObject : class
    {

        #region Properties

        TResponseObject Data { get; set; }

        IResponseMessage Message { get; set; }


        #endregion

        #region Methods

        string GenerateMessager();

        #endregion

    }
}