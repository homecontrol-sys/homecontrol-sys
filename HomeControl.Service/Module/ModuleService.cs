using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{

    public class ModuleService : IModuleService
    {

        #region  Fields

        private readonly IMapper _mapper;
        private readonly IModuleRepository _moduleRepository;

        #endregion

        #region Constructor

        public ModuleService(IMapper mapper, IModuleRepository moduleRepository)
        {
            this._mapper = mapper;
            this._moduleRepository = moduleRepository;
        }

        #endregion

        #region Methods

        #region Create

        public async Task<Guid> Create(ModuleDomain domain)
        {
            var entity = _mapper.Map<ModuleEntity>(domain);
            var entityId = await _moduleRepository.Create<ModuleEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<ModuleDomain> Read(Guid id)
        {
            var entity = await _moduleRepository.Read<ModuleEntity>(id);
            var domain = _mapper.Map<ModuleDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<ModuleDomain>> Read()
        {
            var entities = await _moduleRepository.Read<ModuleEntity>();
            var domains = _mapper.Map<IEnumerable<ModuleDomain>>(entities); 

            return domains;
        }

        public async Task<IEnumerable<ModuleDomain>> Find(IEnumerable<string> prefetch = null, ModuleFilter filter = null, ModuleSearch search = null)
        {
            var entities = await _moduleRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<ModuleDomain>>(entities);

            return domains;
        }

        #endregion

        #region Update

        // public async Task<bool> Update(ModuleDomain Module)
        // {
        //     var entity = _mapper.Map<ModuleEntity>(Module);
        //     bool result = await _ModuleRepository.Update(entity); 

        //     return result;
        // }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _moduleRepository.Delete<ModuleEntity>(id); 
            
            return result;
        }

        #endregion

        #endregion

    }

}