using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface IModuleService
    {

        Task<Guid> Create(ModuleDomain domain);
        Task<ModuleDomain> Read(Guid id);
        Task<IEnumerable<ModuleDomain>> Read();
        Task<IEnumerable<ModuleDomain>> Find(IEnumerable<string> prefetch = null, ModuleFilter filter = null, ModuleSearch search = null);

    }

}