﻿using System.Data;
using System.Threading.Tasks;
using HomeControl.DAL.Entity.Identity;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.Service.Identity._Utils
{
    public class IdentityUserValidator : IUserValidator<UserEntity>
    {
        private readonly IDbConnection _db;

        public IdentityUserValidator(IDbConnection db)
        {
            _db = db;
        }

        public Task<IdentityResult> ValidateAsync(UserManager<UserEntity> manager, UserEntity user)
        {
            // var knownCompanyMember = _db.QuerySingleOrDefault<CompanyMember>(
            //     @"SELECT * FROM dbo.CompanyMembers WHERE MemberEmail = @Email",
            //     new {Email = user.Email});
            // if (knownCompanyMember != null)
            // {
            //     return Task.FromResult(IdentityResult.Success);
            // }

            // return Task.FromResult(IdentityResult.Failed(new IdentityError
            // {
            //     Code = "UnknownMember",
            //     Description = "Provided username is not a valid company member."
            // }));
            return null;
        }
    }
}
