﻿using System.Threading.Tasks;
using HomeControl.DAL.Entity.Identity;
using Microsoft.AspNetCore.Identity;

namespace HomeControl.Service.Identity._Utils
{
    public class IdentityPasswordValidator : IPasswordValidator<UserEntity>
    {
        public Task<IdentityResult> ValidateAsync(UserManager<UserEntity> manager, UserEntity user, string password)
        {
            if (password.ToLowerInvariant().Contains("glob"))
            {
                return Task.FromResult(IdentityResult.Failed(new IdentityError
                {
                    Code = "GlobomanticsVariant",
                    Description = "Variants of Globomantics cannot be used in a password."
                }));
            }

            return Task.FromResult(IdentityResult.Success);
        }

    }
}
