﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.DAL.Entity.Identity;
using HomeControl.Repository.Identity._Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace HomeControl.Service.Identity._Utils
{
    public class IdentityUserManager : UserManager<UserEntity>
    {
        public IdentityUserManager(IUserStore<UserEntity> store,
        IOptions<IdentityOptions> optionsAccessor,
        IPasswordHasher<UserEntity> passwordHasher,
        IEnumerable<IUserValidator<UserEntity>> userValidators,
        IEnumerable<IPasswordValidator<UserEntity>> passwordValidators,
        ILookupNormalizer keyNormalizer,
        IdentityErrorDescriber errors,
        IServiceProvider services,
        ILogger<UserManager<UserEntity>> logger)
        : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }

        public override async Task<IdentityResult> AccessFailedAsync(UserEntity user)
        {
            ThrowIfDisposed();
            var store = GetUserLockoutStore();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            // If this puts the user over the threshold for lockout,
            // lock them out and reset the access failed count
            var count = await store.IncrementAccessFailedCountAsync(user, CancellationToken);
            if (count < Options.Lockout.MaxFailedAccessAttempts || !user.LockoutEnabled)
            {
                return await UpdateUserAsync(user);
            }
            Logger.LogWarning(12, "User is locked out.");
            await store.SetLockoutEndDateAsync(user, 
                DateTimeOffset.UtcNow.Add(Options.Lockout.DefaultLockoutTimeSpan),
                CancellationToken);
            await store.ResetAccessFailedCountAsync(user, CancellationToken);
            return await UpdateUserAsync(user);
        }

        private IUserLockoutStore<UserEntity> GetUserLockoutStore()
        {
            if (!(Store is IUserLockoutStore<UserEntity> cast))
            {
                throw new NotSupportedException("User store does not implement IUserLockoutStore.");
            }
            return cast;
        }

    }
}
