using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity.Identity;
using HomeControl.Domain.Identity;
using HomeControl.Repository;
using HomeControl.Repository.Identity;

namespace HomeControl.Service
{
    public class UserService : IUserService
    {

        #region Fields

        private readonly IMapper _mapper;
        public readonly IUserRepository _userRepository;
        public readonly IUserClaimRepository _userClaimRepository;
        public readonly IUserRoleRepository _userRoleRepository;
        public readonly IUserTokenRepository _userTokenRepository;
        

        #endregion

        #region Constructor

        public UserService(IMapper mapper, 
        IUserRepository userRepository, IUserClaimRepository userClaimRepository, 
        IUserRoleRepository userRoleRepository, IUserTokenRepository userTokenRepository)
        {
            this._mapper = mapper;
            this._userRepository = userRepository;
            this._userClaimRepository = userClaimRepository;
            this._userRoleRepository = userRoleRepository;
            this._userTokenRepository = userTokenRepository;
        }

        #endregion

        #region Methods

        #region Temp

        public async Task<UserDomain> FindByUserNameOrEmail(string userName, string eMail, string password)
        {
            var entity = await _userRepository.FindByUserNameOrEmail(userName, eMail, password);
            var domain = _mapper.Map<UserDomain>(entity);

            return domain; 
        }

        #endregion

        #region User
            
        public async Task<UserDomain> FindUser(UserFilter filter)
        {
            var entity = await _userRepository.FindUser(filter);
            var domain = _mapper.Map<UserDomain>(entity);

            return domain;
        }

        #region CRUD

        #region Create

        // public async Task<UserDomain> Create(UserDomain user)
        // {
        //     var newEntity = _mapper.Map<UserEntity>(user);
        //     newEntity = await _userRepository.Create(newEntity);
        //     var domain = _mapper.Map<UserDomain>(newEntity);
            
        //     return domain;
        // }

        // public async Task<bool> CreateUser(UserDomain user)
        // {
        //     var entity = _mapper.Map<UserEntity>(user);
        //     var isSuccess = await _userRepository.Create(entity) != null; 
            
        //     return isSuccess;
        // }

        #endregion

        #region Read
    
        // public async Task<UserDomain> ReadUser(Guid id)
        // {
        //     var entity = await _userRepository.Read(id);
        //     var domain = _mapper.Map<UserDomain>(entity);

        //     return domain;
        // }

        // public async Task<IEnumerable<UserDomain>> ReadUsers()
        // {
        //     var entities = await _userRepository.Read();
        //     var domains = _mapper.Map<IEnumerable<UserDomain>>(entities);

        //     return domains;
        // }

        #endregion

        #region Update

        // public async Task<bool> UpdateUser(UserDomain user)
        // {
        //     // var entity = _mapper.Map<UserEntity>(user);
        //     // bool result = await _userRepository.Update(entity); 
            
        //     // return result;
        // }

        #endregion

        #region Delete
        
        // public async Task<bool> DeleteUser(Guid id)
        // {
        //     var result = await _userRepository.Delete(id); 
            
        //     return result;
        // }

        #endregion

        #endregion

        #endregion  

        // #region UserClaim

        //     #region CRUD

        //     #region Create

        //     public async Task<bool> CreateUserClaim(UserClaimDomain userClaim)
        //     {
        //         var entity = _mapper.Map<UserClaimEntity>(userClaim);
        //         bool result = await _userClaimRepository.Create(entity) != Guid.Empty;

        //         return result;
        //     }

        //     #endregion

        //     #region Read
        
        //     public async Task<UserClaimDomain> ReadUserClaim(Guid userId, Guid userClaimId)
        //     {
        //         var entity = await _userClaimRepository.ReadUserClaim(userId, userClaimId);
        //         var domain = _mapper.Map<UserClaimDomain>(entity);

        //         return domain;
        //     }

        //     public async Task<IEnumerable<UserClaimDomain>> ReadUserClaims(Guid userId)
        //     {
        //         var entities = await _userClaimRepository.ReadUserClaims(userId);
        //         var domains = _mapper.Map<IEnumerable<UserClaimDomain>>(entities);

        //         return domains;
        //     }

        //     #endregion

        //     #region Update

        //     public async Task<bool> UpdateUserClaim(UserClaimDomain userClaim)
        //     {
        //         var entity = _mapper.Map<UserClaimEntity>(userClaim);
        //         bool result = await _userClaimRepository.Update(entity); 
                
        //         return result;
        //     }

        //     #endregion

        //     #region Delete
            
        //     public async Task<bool> DeleteUserClaim(Guid id)
        //     {
        //         var result = await _userClaimRepository.Delete(id); 
                
        //         return result;
        //     }

        //     #endregion

        //     #endregion

        // #endregion  

        // #region UserRole

        //     #region CRUD

        //     #region Create

        //     public async Task<bool> CreateUserRole(UserRoleDomain userRole)
        //     {
        //         var entity = _mapper.Map<UserRoleEntity>(userRole);
        //         bool result = await _userRoleRepository.Create(entity) != Guid.Empty;

        //         return result;
        //     }

        //     #endregion

        //     #region Read
        
        //     public async Task<UserRoleDomain> ReadUserRole(Guid userRoleId)
        //     {
        //         var entity = await _userRoleRepository.Read(userRoleId);
        //         var domain = _mapper.Map<UserRoleDomain>(entity);

        //         return domain;
        //     }

        //     public async Task<IEnumerable<UserRoleDomain>> ReadUserRoles(Guid userId)
        //     {
        //         var entities = await _userRoleRepository.ReadUserRoles(userId);
        //         var domains = _mapper.Map<IEnumerable<UserRoleDomain>>(entities);

        //         return domains;
        //     }

        //     #endregion

        //     #region Update

        //     public async Task<bool> UpdateUserRole(UserRoleDomain userRole)
        //     {
        //         var entity = _mapper.Map<UserRoleEntity>(userRole);
        //         bool result = await _userRoleRepository.Update(entity); 
                
        //         return result;
        //     }

        //     #endregion

        //     #region Delete
            
        //     public async Task<bool> DeleteUserRole(Guid userRoleId)
        //     {
        //         var result = await _userRoleRepository.Delete(userRoleId); 
                
        //         return result;
        //     }

        //     #endregion

        //     #endregion

        // #endregion  

        // #region UserToken

        //     #region CRUD

        //     #region Create

        //     public async Task<bool> CreateUserToken(UserTokenDomain userToken)
        //     {
        //         var entity = _mapper.Map<UserTokenEntity>(userToken);
        //         bool result = await _userTokenRepository.Create(entity) != Guid.Empty;

        //         return result;
        //     }

        //     #endregion

        //     #region Read
        
        //     public async Task<UserTokenDomain> ReadUserToken(Guid userTokenId)
        //     {
        //         var entity = await _userTokenRepository.Read(userTokenId);
        //         var domain = _mapper.Map<UserTokenDomain>(entity);

        //         return domain;
        //     }

        //     public async Task<IEnumerable<UserTokenDomain>> ReadUserTokens()
        //     {
        //         var entities = await _userTokenRepository.Read();
        //         var domains = _mapper.Map<IEnumerable<UserTokenDomain>>(entities);

        //         return domains;
        //     }

        //     #endregion

        //     #region Update

        //     public async Task<bool> UpdateUserToken(UserTokenDomain userToken)
        //     {
        //         var entity = _mapper.Map<UserTokenEntity>(userToken);
        //         bool result = await _userTokenRepository.Update(entity); 
                
        //         return result;
        //     }

        //     #endregion

        //     #region Delete
            
        //     public async Task<bool> DeleteUserToken(Guid userTokenId)
        //     {
        //         var result = await _userTokenRepository.Delete(userTokenId); 
                
        //         return result;
        //     }

        // #endregion

        // #endregion

        // #endregion


        #endregion

    }

}