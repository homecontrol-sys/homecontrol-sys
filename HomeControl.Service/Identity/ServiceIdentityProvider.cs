using System;
using System.Net.Http;
using System.Text;
using HomeControl.DAL;
using HomeControl.DAL.Entity.Identity;
using HomeControl.Domain.Identity;
using HomeControl.Service.Identity._Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Polly;

namespace HomeControl.Service.Identity
{
    public static class ServiceIdentityProvider
    {

        #region Public

        public static IServiceCollection ProvideIdentityLayer(this IServiceCollection services, IConfiguration config)
        {
            services.AddCors();
            services.AddJwtToken(config.GetSection("JwtToken"));
            services.AddAppIdentityUtils();
            services.AddAppIdentity();
            services.AddIdentityServices();
            return services;
        }

        #endregion

        #region Private

        private static IServiceCollection AddIdentityServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<IRoleService, RoleService>();

            return services;
        }

        private static IServiceCollection AddAppIdentityUtils(this IServiceCollection services)
        {
            services.AddTransient<IConfigureOptions<IdentityOptions>, AppIdentityOptions>();
            services.AddScoped<IPasswordHasher<UserEntity>, IdentityPasswordHasher>();

            return services;
        }

        private static IdentityBuilder AddAppIdentity(this IServiceCollection services) {
            return services
                .AddIdentity<UserEntity, RoleEntity>()
                .AddEntityFrameworkStores<HomeControlDbContext>()
                .AddUserManager<IdentityUserManager>();
                // .AddSignInManager<SignInManager<UserEntity>>()
                // .AddTokenProvider<DataProtectorTokenProvider<UserEntity>>(TokenOptions.DefaultProvider)
                // .AddTokenProvider<EmailTokenProvider<UserEntity>>(TokenOptions.DefaultEmailProvider)
                // .AddTokenProvider<AuthenticatorTokenProvider<UserEntity>>(TokenOptions.DefaultAuthenticatorProvider)
                // .AddPasswordValidator<IdentityPasswordValidator>()
                // .AddUserValidator<IdentityUserValidator>();
        }

        private static IServiceCollection AddJwtToken(this IServiceCollection services, IConfigurationSection jwtTokenConfig)
        {
            JwtSettings settings = new JwtSettings(jwtTokenConfig);
            services.AddSingleton<JwtSettings>(settings);
            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            })
                .AddJwtBearer("JwtBearer", options => {
                    options.TokenValidationParameters = 
                    new TokenValidationParameters {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.Key)),
                        ValidateIssuer = true,
                        ValidIssuer = settings.Issuer,
                        ValidateAudience = true,
                        ValidAudience = settings.Audience,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromMinutes(settings.MinutesToExpiration)
                    };
                });

          return services;
        }

        #endregion   

    }
}
