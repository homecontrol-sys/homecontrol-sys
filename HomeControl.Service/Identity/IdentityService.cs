using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using API.Project.Common.Modules.Security._Extension;
using AutoMapper;
using HomeControl.Domain.Identity;
using HomeControl.Repository;
using HomeControl.Repository.Identity;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;

namespace HomeControl.Service.Identity
{
    
    public class IdentityService : IIdentityService
    {
        private readonly IMapper _mapper;
        private readonly JwtSettings _jwtSettings;
        private readonly IUserRepository _userRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IUserClaimRepository _userClaimRepository;

        public IdentityService(IMapper mapper, JwtSettings jwtSettings, IUserRepository userRepository,
        IUserRoleRepository userRoleRepository, IUserClaimRepository userClaimRepository)
        {
            this._mapper = mapper;
            this._jwtSettings = jwtSettings;
            this._userRepository = userRepository;
            this._userRoleRepository = userRoleRepository;
            this._userClaimRepository = userClaimRepository;
        }

        #region Public

        // public async Task<AuthObject> Login(AuthModel credentials)
        // {
        //     var identity = await Authenticate(credentials);
        //     bool isValid = identity != null; 

        //     if (isValid){
        //         var authObject = await Authorize(identity);
        //         return authObject;
        //     }
        //     else {
        //         return null;
        //     }
        // }

        #endregion

        #region Private

        private async Task<UserDomain> Authenticate(AuthModel credentials)
        {
            bool isLogingInWithEmail = credentials.UserNameOrEMail.IsValidEmail();
            string eMail = isLogingInWithEmail ? credentials.UserNameOrEMail : null;
            string userName = !isLogingInWithEmail ? credentials.UserNameOrEMail : null;       
            var userEntity = await _userRepository.FindByUserNameOrEmail(userName, eMail, credentials.Password);
            var user = _mapper.Map<UserDomain>(userEntity);

            return user;
        }

        // private async Task<AuthObject> Authorize(UserDomain user)
        // {
        //     var userRoleEntities = await _userRoleRepository.ReadUserRoles(user.Id);
        //     user.UserRoles = _mapper.Map<IEnumerable<UserRoleDomain>>(userRoleEntities);
        //     var userClaimEntities = await _userClaimRepository.ReadUserClaims(user.Id);
        //     user.UserClaims = _mapper.Map<IEnumerable<UserClaimDomain>>(userClaimEntities);

        //     var authObject = _mapper.Map<AuthObject>(user); 

        //     return authObject;
        // }

        private string CreateJwtToken(IList<UserClaimDomain> userClaims, string userName) {
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));

            // Create standard JwtClaims
            List<Claim> jwtClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            // Create custom Claims
            foreach(var claim in userClaims)
            {
                // jwtClaims.Add(new Claim(claim.ClaimType, claim.Value))

            }
            return "";
        }

        #endregion

    }

}
