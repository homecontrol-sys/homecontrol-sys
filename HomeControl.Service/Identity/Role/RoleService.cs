using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity.Identity;
using HomeControl.Domain.Identity;
using HomeControl.Repository.Identity;

namespace HomeControl.Service.Identity
{

    public class RoleService : IRoleService
    {

        #region Fields

        private readonly IMapper _mapper;
        private readonly IRoleRepository _roleRepository;
        // public readonly IRoleClaimRepository _roleClaimRepository;

        #endregion

        #region Constructor

        public RoleService(IMapper mapper, IRoleRepository roleRepository)
        {
            this._mapper = mapper;
            this._roleRepository = roleRepository;
            // this._roleClaimRepository = roleClaimRepository;
        }

        #endregion

        #region Methods

        #region Role

            #region CRUD

                #region Create

                // public async Task<Guid> CreateRole(RoleDomain role)
                // {
                //     var entity = _mapper.Map<RoleEntity>(role);
                //     return await _roleRepository.Create(entity);
                // }


                #endregion

                #region Read

                // public async Task<RoleDomain> ReadRole(Guid id)
                // {
                //     var entity = await _roleRepository.Read<RoleEntity>(id); 
                //     var domain = _mapper.Map<RoleDomain>(entity);

                //     return domain; 
                // }

                // public async Task<IEnumerable<RoleDomain>> ReadRoles()
                // {
                //     var entities = await _roleRepository.Read<RoleEntity>(); 
                //     var domains = _mapper.Map<IEnumerable<RoleDomain>>(entities);

                //     return domains;
                // }

                #endregion

                #region Update

                // public async Task<bool> UpdateRole(RoleDomain Role)
                // {
                //     var entity = _mapper.Map<RoleEntity>(Role);
                //     var isSuccess = (await _roleRepository.Update(entity)) != null; 

                //     return isSuccess;
                // }

                #endregion

                #region Delete

                // public async Task<bool> DeleteRole(Guid id)
                // {
                //     bool result = await _roleRepository.Delete<RoleEntity>(id); 

                //     return result;
                // }

                #endregion

            #endregion

        #endregion
        
        #region RoleClaim

             #region CRUD

                #region Create

                // public async Task<bool> CreateRoleClaim(RoleClaimDomain roleClaim)
                // {
                //     var entity = _mapper.Map<RoleClaimEntity>(roleClaim);
                //     bool result = await _roleClaimRepository.Create(entity);

                //     return result;
                // }


                // #endregion

                // #region Read

                // public async Task<RoleClaimDomain> ReadRoleClaim(Guid id)
                // {
                //     var entity = await _roleClaimRepository.Read(id); 
                //     var domain = _mapper.Map<RoleClaimDomain>(entity);

                //     return domain; 
                // }

                // public async Task<IEnumerable<RoleClaimDomain>> ReadRoleClaims()
                // {
                //     var entities = await _roleClaimRepository.Read(); 
                //     var domains = _mapper.Map<IEnumerable<RoleClaimDomain>>(entities);

                //     return domains;
                // }

                // #endregion

                // #region Update

                // public async Task<bool> UpdateRoleClaim(RoleClaimDomain Role)
                // {
                //     var entity = _mapper.Map<RoleClaimEntity>(Role);
                //     bool result = await _roleClaimRepository.Update(entity); 

                //     return result;
                // }

                // #endregion

                // #region Delete

                // public async Task<bool> DeleteRoleClaim(Guid id)
                // {
                //     bool result = await _roleClaimRepository.Delete(id); 

                //     return result;
                // }

                #endregion

            #endregion

        #endregion

        #endregion
   
   }

}
