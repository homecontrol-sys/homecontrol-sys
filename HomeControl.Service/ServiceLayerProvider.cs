using HomeControl.Service.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HomeControl.Service
{

    public static class ServiceLayerProvider
    {
        public static IServiceCollection ProvideServiceLayer(this IServiceCollection services, IConfiguration config)
        {
            
            #region Singletons

            services.AddSingleton<ICommandService, CommandService>();

            #endregion

            #region Scoped

            services.AddScoped<IDeviceService, DeviceService>();
            services.AddScoped<IElementService, ElementService>();
            services.AddScoped<ISwitchStateService, SwitchStateService>();
            services.AddScoped<ISwitchService, SwitchService>();
            services.AddScoped<IModuleService, ModuleService>();
            services.AddScoped<IPinService, PinService>();
            services.AddScoped<IPinConnectionService, PinConnectionService>();
            services.AddScoped<IPinTypeService, PinTypeService>();
            services.AddScoped<ISystemService, SystemService>();

            #endregion

            #region Identity

            services.ProvideIdentityLayer(config);
            
            #endregion

            return services;
        }
    }

}