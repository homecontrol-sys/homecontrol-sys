using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{

    public class DeviceService : IDeviceService
    {

        #region Fields

        private readonly IMapper _mapper;  
        private readonly IDeviceRepository _deviceRepository;
        private readonly ISystemRepository _systemRepository;


        #endregion


        #region Constructor

        public DeviceService(IMapper mapper, 
        IDeviceRepository deviceRepository)
        {
            this._mapper = mapper;
            this._deviceRepository = deviceRepository;
        }

        #endregion

        #region Methods

        #region Create

        public async Task<Guid> Create(DeviceDomain domain)
        {
            var entity = _mapper.Map<DeviceEntity>(domain);
            var entityId = await _deviceRepository.Create<DeviceEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<DeviceDomain> Read(Guid id)
        {
            var entity = await _deviceRepository.Read<DeviceEntity>(id); 
            var domain = _mapper.Map<DeviceDomain>(entity);

            return domain; 
        }

        public async Task<IEnumerable<DeviceDomain>> Read()
        {
            var entities = await _deviceRepository.Read<DeviceEntity>(); 
            var domains = _mapper.Map<IEnumerable<DeviceDomain>>(entities);

            return domains;
        }

        public async Task<IEnumerable<DeviceDomain>> Find(IEnumerable<string> prefetch = null, DeviceFilter filter = null, DeviceSearch search = null)
        {
            var entities = await _deviceRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<DeviceDomain>>(entities);

            return domains;
        }

        #endregion

        #region Update

        // public async Task<bool> Update(DeviceDomain device)
        // {
        //     var entity = _mapper.Map<DeviceEntity>(device);
        //     bool result = await _deviceRepository.Update(entity); 

        //     return result;
        // }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _deviceRepository.Delete<DeviceEntity>(id); 

            return result;
        }

        #endregion

        #endregion
   
   }

}
