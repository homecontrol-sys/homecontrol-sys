using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface IDeviceService
    {

        Task<Guid> Create(DeviceDomain domain);
        Task<DeviceDomain> Read(Guid id);
        Task<IEnumerable<DeviceDomain>> Read();
        Task<IEnumerable<DeviceDomain>> Find(IEnumerable<string> prefetch = null, DeviceFilter filter = null, DeviceSearch search = null);

    }

}