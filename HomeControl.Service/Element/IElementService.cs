using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface IElementService
    {
        
        Task<Guid> Create(ElementDomain domain);
        Task<ElementDomain> Read(Guid id);
        Task<IEnumerable<ElementDomain>> Read();
        Task Update(ElementDomain domain);
        Task<IEnumerable<ElementDomain>> Find(IEnumerable<string> prefetch = null, ElementFilter filter = null, ElementSearch search = null);
        Task<bool> SwitchElementState(Guid elementId, Guid newElementSwitchStateId);

    }

}