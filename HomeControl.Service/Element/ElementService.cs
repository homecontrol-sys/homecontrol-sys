using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{
    
    public class ElementService : IElementService
    {

        #region Fields

        private readonly IMapper _mapper;
        private readonly IElementRepository _elementRepository;
        private readonly ISwitchService _switchService;
        private readonly ICommandService _commandService;

        #endregion

        #region Constructor

        public ElementService(IMapper mapper, IElementRepository elementRepository, ISwitchService switchService, ICommandService commandService)
        {
            _mapper = mapper;
            _elementRepository = elementRepository;
            _commandService = commandService;
            _switchService = switchService;
        }

        #endregion
    
        #region Methods

        #region CRUD

        #region Create

        public async Task<Guid> Create(ElementDomain domain)
        {
            var entity = _mapper.Map<ElementEntity>(domain);
            var entityId = await _elementRepository.Create<ElementEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<ElementDomain> Read(Guid id)
        {
            var entity = await _elementRepository.Read<ElementEntity>(id);
            var domain = _mapper.Map<ElementDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<ElementDomain>> Read()
        {
            var entities = await _elementRepository.Read<ElementEntity>(); 
            var domains = _mapper.Map<IEnumerable<ElementDomain>>(entities);

            return domains;
        }

        public async Task<IEnumerable<ElementDomain>> Find(IEnumerable<string> prefetch = null, ElementFilter filter = null, ElementSearch search = null)
        {
            var entities = await _elementRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<ElementDomain>>(entities);

            return domains;
        }

        #endregion

        #region Update

        public async Task Update(ElementDomain element)
        {
            var entity = _mapper.Map<ElementEntity>(element);
            await _elementRepository.Update(entity);
        }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _elementRepository.Delete<ElementEntity>(id); 

            return result;
        }

        #endregion

        #endregion

         public async Task<bool> SwitchElementState(Guid elementId, Guid newElementSwitchStateId){
            var element = await _elementRepository.Read<ElementEntity>(elementId);

            if (element == null) return false;
            if (newElementSwitchStateId == element.SwitchStateId) return false;

            string[] prefetch = new[] { "SwitchStates" };
            var targetSwitch = await _switchService.Find(prefetch);
            var newElementSwitchState = targetSwitch.SelectMany(s => s.SwitchStates.Where(ss => ss.Id == newElementSwitchStateId)).SingleOrDefault();
            if(newElementSwitchState == null) return false;
      
            _commandService.SetPinValue(newElementSwitchState.Value);
            element.SwitchStateId = newElementSwitchStateId;
            await _elementRepository.Update<ElementEntity>(element);

            return true;
         }

        #endregion

    }

}