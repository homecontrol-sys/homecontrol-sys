using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface ISwitchService
    {

        Task<Guid> Create(SwitchDomain domain);
        Task<SwitchDomain> Read(Guid id);
        Task<IEnumerable<SwitchDomain>> Read();
        Task<IEnumerable<SwitchDomain>> Find(IEnumerable<string> prefetch = null, SwitchFilter filter = null, SwitchSearch search = null);
        Task<bool> SwitchStateExistsOnSwitch(Guid switchId, Guid switchStateId);

    }

}