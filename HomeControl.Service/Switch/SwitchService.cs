using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{
    
    public class SwitchService : ISwitchService
    {

        #region Fields

        private readonly IMapper _mapper;
        private readonly ISwitchRepository _switchRepository;

        #endregion

        #region Constructor

        public SwitchService(IMapper mapper, ISwitchRepository switchRepository)
        {
            this._mapper = mapper;
            this._switchRepository = switchRepository;
        }

        #endregion
    
        #region Methods

        #region Create

        public async Task<Guid> Create(SwitchDomain domain)
        {
            var entity = _mapper.Map<SwitchEntity>(domain);
            var entityId = await _switchRepository.Create<SwitchEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<SwitchDomain> Read(Guid id)
        {
            var entity = await _switchRepository.Read<SwitchEntity>(id);
            var domain = _mapper.Map<SwitchDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<SwitchDomain>> Read()
        {
            var entities = await _switchRepository.Read<SwitchEntity>(); 
            var domains = _mapper.Map<IEnumerable<SwitchDomain>>(entities);

            return domains;
        }

        public async Task<IEnumerable<SwitchDomain>> Find(IEnumerable<string> prefetch = null, SwitchFilter filter = null, SwitchSearch search = null)
        {
            var entities = await _switchRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<SwitchDomain>>(entities);

            return domains;
        }

        public async Task<bool> SwitchStateExistsOnSwitch(Guid switchId, Guid switchStateId)
        {
            return await _switchRepository.SwitchStateExistsOnSwitch(switchId, switchStateId);
        }

        #endregion

        #region Update

        // public async Task<bool> Update(SwitchDomain switch)
        // {
        //     var entity = _mapper.Map<SwitchEntity>(switch);
        //     // bool result = await base.Update(entity);

        //     return result;
        // }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _switchRepository.Delete<SwitchEntity>(id); 

            return result;
        }

        #endregion

        #endregion

    }

}