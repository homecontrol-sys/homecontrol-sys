namespace HomeControl.Service.Command
{

    public enum PossiblePinValue
    {
        LOW = 0,
        HIGH = 1,
    }
}