using System;
using System.Threading.Tasks;

namespace HomeControl.Service
{

    public interface ICommandService
    {

        #region Methods
        
        bool Execute(string command);
        bool ExecuteVoiceCommand();
        void SetPinValue(int pinValue);

        #endregion

    }

}