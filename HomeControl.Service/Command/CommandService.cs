using System;
using System.Device.Gpio;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HomeControl.Domain;
using HomeControl.Service.Command;

namespace HomeControl.Service
{

    public class CommandService : ICommandService, IDisposable
    {

        #region Fields

        private const int relay = 27;
        private GpioController _gpioController = new GpioController();
        private bool disposedValue = false;
        private object _locker = new object();

        #endregion

        #region Properties
        
        public bool IsLedOn { get; private set; }
        
        #endregion

        #region Constructor

        public CommandService()
        {
            InitializePins();
        }

        #endregion

        #region Methods

        public void InitializePins()
        {
            try 
            {
                _gpioController.OpenPin(relay, PinMode.Output);
            }
            catch (IOException e)
            {
                if (e.Source != null)
                    Console.WriteLine("IOException source: {0}", e.Source);
                throw;
            }
        }

        public bool Execute(string command)
        {
            lock (_locker)
            {              
                if(command.Contains("light on")){
                    _gpioController.Write(relay, PinValue.High);
                }
                else if(command.Contains("light off")){
                    _gpioController.Write(relay, PinValue.Low);
                }
            }
            return true;
        }
        
        public bool ExecuteVoiceCommand()
        {
            return false;
        }

        public void SetPinValue(int pinValue)
        {
            switch(pinValue)
            {
                case (int)PossiblePinValue.LOW:
                    _gpioController.Write(relay, PinValue.Low);
                    break;

                case (int)PossiblePinValue.HIGH:
                    _gpioController.Write(relay, PinValue.High);
                    break;
                
                default:
                    _gpioController.Write(relay, PinValue.Low);
                    break;
            }
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _gpioController.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
   
   }

}
