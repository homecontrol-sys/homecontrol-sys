using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{

    public class PinConnectionService : IPinConnectionService
    {

        #region  Fields

        private readonly IMapper _mapper;
        private readonly IPinConnectionRepository _pinConnectionRepository;

        #endregion

        #region Constructor

        public PinConnectionService(IMapper mapper, IPinConnectionRepository pinConnectionRepository)
        {
            this._mapper = mapper;
            this._pinConnectionRepository = pinConnectionRepository;
        }

        #endregion

        #region Methods

        #region Create

        public async Task<Guid> Create(PinConnectionDomain domain)
        {
            var entity = _mapper.Map<PinConnectionEntity>(domain);
            var entityId = await _pinConnectionRepository.Create<PinConnectionEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<PinConnectionDomain> Read(Guid id)
        {
            var entity = await _pinConnectionRepository.Read<PinConnectionEntity>(id);
            var domain = _mapper.Map<PinConnectionDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<PinConnectionDomain>> Read()
        {
            var entities = await _pinConnectionRepository.Read<PinConnectionEntity>();
            var domains = _mapper.Map<IEnumerable<PinConnectionDomain>>(entities); 

            return domains;
        }

        public async Task<IEnumerable<PinConnectionDomain>> Find(IEnumerable<string> prefetch = null, PinConnectionFilter filter = null, PinConnectionSearch search = null)
        {
            var entities = await _pinConnectionRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<PinConnectionDomain>>(entities);

            return domains;
        }

        #endregion

        #region Update

        // public async Task<bool> Update(PinConnectionDomain PinConnection)
        // {
        //     var entity = _mapper.Map<PinConnectionEntity>(PinConnection);
        //     bool result = await _PinConnectionRepository.Update(entity); 

        //     return result;
        // }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _pinConnectionRepository.Delete<PinConnectionEntity>(id); 
            
            return result;
        }

        #endregion

        #endregion

    }

}