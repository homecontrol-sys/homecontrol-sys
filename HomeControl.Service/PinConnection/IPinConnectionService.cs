using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface IPinConnectionService
    {

        Task<Guid> Create(PinConnectionDomain domain);
        Task<PinConnectionDomain> Read(Guid id);
        Task<IEnumerable<PinConnectionDomain>> Read();
        Task<IEnumerable<PinConnectionDomain>> Find(IEnumerable<string> prefetch = null, PinConnectionFilter filter = null, PinConnectionSearch search = null);

    }

}