using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{
    
    public class SwitchStateService : ISwitchStateService
    {

        #region Fields

        private readonly IMapper _mapper;
        private readonly ISwitchStateRepository _switchStateRepository;

        #endregion

        #region Constructor

        public SwitchStateService(IMapper mapper, ISwitchStateRepository switchStateRepository)
        {
            this._mapper = mapper;
            this._switchStateRepository = switchStateRepository;
        }

        #endregion
    
        #region Methods

        #region Create

        public async Task<Guid> Create(SwitchStateDomain domain)
        {
            var entity = _mapper.Map<SwitchStateEntity>(domain);
            var entityId = await _switchStateRepository.Create<SwitchStateEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<SwitchStateDomain> Read(Guid id)
        {
            var entity = await _switchStateRepository.Read<SwitchStateEntity>(id);
            var domain = _mapper.Map<SwitchStateDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<SwitchStateDomain>> Read()
        {
            var entities = await _switchStateRepository.Read<SwitchStateEntity>(); 
            var domains = _mapper.Map<IEnumerable<SwitchStateDomain>>(entities);

            return domains;
        }

        public async Task<IEnumerable<SwitchStateDomain>> Find(IEnumerable<string> prefetch = null, SwitchStateFilter filter = null, SwitchStateSearch search = null)
        {
            var entities = await _switchStateRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<SwitchStateDomain>>(entities);

            return domains;
        }

        #endregion

        #region Update

        // public async Task<bool> Update(SwitchStateDomain switchState)
        // {
        //     var entity = _mapper.Map<SwitchStateEntity>(switchState);
        //     // bool result = await base.Update(entity);

        //     return result;
        // }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _switchStateRepository.Delete<SwitchStateEntity>(id); 

            return result;
        }

        #endregion

        #endregion

    }

}