using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface ISwitchStateService
    {
        
        Task<Guid> Create(SwitchStateDomain domain);
        Task<SwitchStateDomain> Read(Guid id);
        Task<IEnumerable<SwitchStateDomain>> Read();
        Task<IEnumerable<SwitchStateDomain>> Find(IEnumerable<string> prefetch = null, SwitchStateFilter filter = null, SwitchStateSearch search = null);

    }

}