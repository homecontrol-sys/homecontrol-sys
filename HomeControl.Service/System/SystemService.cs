using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Project.Common.Architecture.Repository;
using AutoMapper;
using HomeControl.DAL;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{

    public class SystemService : ISystemService
    {

        #region  Fields

        private readonly IMapper _mapper;
        private readonly HomeControlDbContext _homeControlDbContext;
        private readonly ISystemRepository _systemRepository;
        private readonly INetworkConfigurationRepository _networkConfigurationRepository;

        #endregion

        #region Constructor

        public SystemService(IMapper mapper, HomeControlDbContext homeControlDbContext, 
            ISystemRepository systemRepository, INetworkConfigurationRepository networkConfigurationRepository)
        {
            _mapper = mapper;
            _homeControlDbContext = homeControlDbContext;
            _systemRepository = systemRepository;
            _networkConfigurationRepository = networkConfigurationRepository;
        }

        #endregion

        #region CRUD

        #region Create

        public async Task<Guid> Create(SystemDomain domain)
        {
            var entity = _mapper.Map<SystemEntity>(domain);
            var entityId = await _systemRepository.Create<SystemEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<SystemDomain> Read(Guid id)
        {
            var entity = await _systemRepository.Read<SystemEntity>(id);
            var domain = _mapper.Map<SystemDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<SystemDomain>> Read()
        {
            var entities = await _systemRepository.Read<SystemEntity>();
            var domains = _mapper.Map<IEnumerable<SystemDomain>>(entities); 

            return domains;
        }

        public async Task<IEnumerable<SystemDomain>> FindDomains(IEnumerable<string> prefetch = null, SystemFilter filter = null, SystemSearch search = null)
        {
            var entities = await _systemRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<SystemDomain>>(entities);

            return domains;
        }

        public async Task<IEnumerable<SystemEntity>> FindEntities(IEnumerable<string> prefetch = null, SystemFilter filter = null, SystemSearch search = null)
        {
            var entities = await _systemRepository.Find(prefetch, filter, search);

            return entities;
        }

        public async Task<SystemDomain> FindMainSystemDomain()
        {
            var filter = new SystemFilter { IsMain = true };
            var domain = (await FindDomains(null, filter, null)).FirstOrDefault();

            return domain;
        }

        public async Task<SystemEntity> FindMainSystemEntity()
        {
            var filter = new SystemFilter { IsMain = true };
            var entity = (await FindEntities(null, filter, null)).FirstOrDefault();

            return entity;
        }

        #endregion

        #region Update

        public void Update(SystemDomain system)
        {
            var entity = _mapper.Map<SystemEntity>(system);
            _systemRepository.Update(entity); 
        }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _systemRepository.Delete<SystemEntity>(id); 
            
            return result;
        }

        #endregion

        #endregion

        #region CONFIGURATION
        
        public async Task<bool> IsMainSystemConfigured() 
        {
            var mainSystem = await FindMainSystemDomain();

            if (mainSystem == null) return false;
            
            var networkConfigurationId = mainSystem?.NetworkConfigurationId; 
            bool hasNetworkConfigured = networkConfigurationId != Guid.Empty && networkConfigurationId != null;   
            
            var isConfigured = hasNetworkConfigured;

            return isConfigured;
        }
        
        public async Task<bool> ConfigureMainSystem(MainSystemConfiguration config)
        {
            var mainSystemEntity = await FindMainSystemEntity();

            if (mainSystemEntity == null) return false;
            using (var unitOfWork = new UnitOfWork(_homeControlDbContext))
            {
                try
                {
                    await unitOfWork.BeginTransactionAsync();
                    NetworkConfigurationEntity networkConfigEntity = new NetworkConfigurationEntity {
                        SSID = config.SSID,
                        Password = config.Password,
                    };
                    await _networkConfigurationRepository.Create(networkConfigEntity);
                    mainSystemEntity = _mapper.Map<SystemEntity>(mainSystemEntity);
                    mainSystemEntity.Name = config.Name;
                    mainSystemEntity.NetworkConfigurationId = networkConfigEntity.Id;
                    await _systemRepository.Update(mainSystemEntity);
                    await unitOfWork.CommitTransactionAsync();

                    return true;
                }
                catch (Exception)
                {
                    await unitOfWork.RollbackTransactionAsync();

                    return false;
                }
            }
        }
            

        #endregion

    }

}