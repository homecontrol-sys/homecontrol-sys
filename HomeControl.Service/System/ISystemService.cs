using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface ISystemService
    {
        
        #region CRUD

        Task<Guid> Create(SystemDomain domain);
        Task<SystemDomain> Read(Guid id);
        Task<IEnumerable<SystemDomain>> Read();
        Task<IEnumerable<SystemDomain>> FindDomains(IEnumerable<string> prefetch = null, SystemFilter filter = null, SystemSearch search = null);
        Task<IEnumerable<SystemEntity>> FindEntities(IEnumerable<string> prefetch = null, SystemFilter filter = null, SystemSearch search = null);

        #endregion

        #region CONFIGURE

        Task<bool> IsMainSystemConfigured();
        Task<bool> ConfigureMainSystem(MainSystemConfiguration config);

        #endregion

    }

}