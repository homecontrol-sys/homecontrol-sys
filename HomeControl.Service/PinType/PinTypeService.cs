using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{

    public class PinTypeService : IPinTypeService
    {

        #region  Fields

        private readonly IMapper _mapper;
        private readonly IPinTypeRepository _pinTypeRepository;

        #endregion

        #region Constructor

        public PinTypeService(IMapper mapper, IPinTypeRepository pinTypeRepository)
        {
            this._mapper = mapper;
            this._pinTypeRepository = pinTypeRepository;
        }

        #endregion

        #region Methods

        #region Create

        public async Task<Guid> Create(PinTypeDomain domain)
        {
            var entity = _mapper.Map<PinTypeEntity>(domain);
            var entityId = await _pinTypeRepository.Create<PinTypeEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<PinTypeDomain> Read(Guid id)
        {
            var entity = await _pinTypeRepository.Read<PinTypeEntity>(id);
            var domain = _mapper.Map<PinTypeDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<PinTypeDomain>> Read()
        {
            var entities = await _pinTypeRepository.Read<PinTypeEntity>();
            var domains = _mapper.Map<IEnumerable<PinTypeDomain>>(entities); 

            return domains;
        }

        public async Task<IEnumerable<PinTypeDomain>> Find(IEnumerable<string> prefetch = null, PinTypeFilter filter = null, PinTypeSearch search = null)
        {
            var entities = await _pinTypeRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<PinTypeDomain>>(entities);

            return domains;
        }

        #endregion

        #region Update

        // public async Task<bool> Update(PinTypeDomain PinType)
        // {
        //     var entity = _mapper.Map<PinTypeEntity>(PinType);
        //     bool result = await _PinTypeRepository.Update(entity); 

        //     return result;
        // }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _pinTypeRepository.Delete<PinTypeEntity>(id); 
            
            return result;
        }

        #endregion

        #endregion

    }

}