using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface IPinTypeService
    {

        Task<Guid> Create(PinTypeDomain domain);
        Task<PinTypeDomain> Read(Guid id);
        Task<IEnumerable<PinTypeDomain>> Read();
        Task<IEnumerable<PinTypeDomain>> Find(IEnumerable<string> prefetch = null, PinTypeFilter filter = null, PinTypeSearch search = null);

    }

}