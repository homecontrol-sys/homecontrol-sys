using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface IPinService
    {

        Task<Guid> Create(PinDomain domain);
        Task<PinDomain> Read(Guid id);
        Task<IEnumerable<PinDomain>> Read();
        Task<IEnumerable<PinDomain>> Find(IEnumerable<string> prefetch = null, PinFilter filter = null, PinSearch search = null);

    }

}