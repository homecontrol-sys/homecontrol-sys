using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{

    public class PinService : IPinService
    {

        #region  Fields

        private readonly IMapper _mapper;
        private readonly IPinRepository _pinRepository;

        #endregion

        #region Constructor

        public PinService(IMapper mapper, IPinRepository pinRepository)
        {
            this._mapper = mapper;
            this._pinRepository = pinRepository;
        }

        #endregion

        #region Methods

        #region Create

        public async Task<Guid> Create(PinDomain domain)
        {
            var entity = _mapper.Map<PinEntity>(domain);
            var entityId = await _pinRepository.Create<PinEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<PinDomain> Read(Guid id)
        {
            var entity = await _pinRepository.Read<PinEntity>(id);
            var domain = _mapper.Map<PinDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<PinDomain>> Read()
        {
            var entities = await _pinRepository.Read<PinEntity>();
            var domains = _mapper.Map<IEnumerable<PinDomain>>(entities); 

            return domains;
        }

        public async Task<IEnumerable<PinDomain>> Find(IEnumerable<string> prefetch = null, PinFilter filter = null, PinSearch search = null)
        {
            var entities = await _pinRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<PinDomain>>(entities);

            return domains;
        }

        #endregion

        #region Update

        // public async Task<bool> Update(PinDomain Pin)
        // {
        //     var entity = _mapper.Map<PinEntity>(Pin);
        //     bool result = await _PinRepository.Update(entity); 

        //     return result;
        // }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _pinRepository.Delete<PinEntity>(id); 
            
            return result;
        }

        #endregion

        #endregion

    }

}