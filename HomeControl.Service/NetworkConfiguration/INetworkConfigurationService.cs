using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HomeControl.Domain;

namespace HomeControl.Service
{

    public interface INetworkConfigurationService
    {
        
        Task<Guid> Create(NetworkConfigurationDomain domain);
        Task<NetworkConfigurationDomain> Read(Guid id);
        Task<IEnumerable<NetworkConfigurationDomain>> Read();
        Task<IEnumerable<NetworkConfigurationDomain>> Find(
            IEnumerable<string> prefetch = null, NetworkConfigurationFilter filter = null, NetworkConfigurationSearch search = null);

    }

}