using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;
using HomeControl.Repository;

namespace HomeControl.Service
{
    
    public class NetworkConfigurationService : INetworkConfigurationService
    {

        #region Fields

        private readonly IMapper _mapper;
        private readonly INetworkConfigurationRepository _networkConfigurationRepository;

        #endregion

        #region Constructor

        public NetworkConfigurationService(IMapper mapper, INetworkConfigurationRepository networkConfigurationRepository)
        {
            this._mapper = mapper;
            this._networkConfigurationRepository = networkConfigurationRepository;
        }

        #endregion
    
        #region Methods

        #region Create

        public async Task<Guid> Create(NetworkConfigurationDomain domain)
        {
            var entity = _mapper.Map<NetworkConfigurationEntity>(domain);
            var entityId = await _networkConfigurationRepository.Create<NetworkConfigurationEntity>(entity);

            return entityId;
        }

        #endregion

        #region Read

        public async Task<NetworkConfigurationDomain> Read(Guid id)
        {
            var entity = await _networkConfigurationRepository.Read<NetworkConfigurationEntity>(id);
            var domain = _mapper.Map<NetworkConfigurationDomain>(entity);

            return domain;
        }

        public async Task<IEnumerable<NetworkConfigurationDomain>> Read()
        {
            var entities = await _networkConfigurationRepository.Read<NetworkConfigurationEntity>(); 
            var domains = _mapper.Map<IEnumerable<NetworkConfigurationDomain>>(entities);

            return domains;
        }

        public async Task<IEnumerable<NetworkConfigurationDomain>> Find(IEnumerable<string> prefetch = null, NetworkConfigurationFilter filter = null, NetworkConfigurationSearch search = null)
        {
            var entities = await _networkConfigurationRepository.Find(prefetch, filter, search);
            var domains = _mapper.Map<IEnumerable<NetworkConfigurationDomain>>(entities);

            return domains;
        }

        #endregion

        #region Update

        // public async Task<bool> Update(NetworkConfigurationDomain networkConfiguration)
        // {
        //     var entity = _mapper.Map<NetworkConfigurationEntity>(networkConfiguration);
        //     // bool result = await base.Update(entity);

        //     return result;
        // }

        #endregion

        #region Delete

        public async Task<bool> Delete(Guid id)
        {
            bool result = await _networkConfigurationRepository.Delete<NetworkConfigurationEntity>(id); 

            return result;
        }

        #endregion

        #endregion

    }

}