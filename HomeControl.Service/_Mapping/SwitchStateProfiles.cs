using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class SwitchStateProfiles : Profile
    {
        public SwitchStateProfiles()
        {

            #region Two Way
            
            CreateMap<SwitchStateEntity, SwitchStateDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region To Domain from Entity

            #endregion
              
        }
    }
}
