using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class ModuleProfiles : Profile
    {
        public ModuleProfiles()
        {
            
            #region Two Way
            
            CreateMap<ModuleEntity, ModuleDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region From Entity to Domain

            #endregion
              
        }
    }
}
