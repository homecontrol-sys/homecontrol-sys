using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class ElementProfiles : Profile
    {
        public ElementProfiles()
        {

            #region Two Way
            
            CreateMap<ElementEntity, ElementDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region To Domain from Entity

            #endregion
              
        }
    }
}
