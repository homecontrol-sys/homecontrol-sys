using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class PinProfiles : Profile
    {
        public PinProfiles()
        {
            
            #region Two Way
            
            CreateMap<PinEntity, PinDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region From Entity to Domain

            #endregion
              
        }
    }
}
