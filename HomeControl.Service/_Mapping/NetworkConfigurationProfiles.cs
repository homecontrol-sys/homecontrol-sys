using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class NetworkConfigurationProfiles : Profile
    {
        public NetworkConfigurationProfiles()
        {

            #region Two Way
            
            CreateMap<NetworkConfigurationEntity, NetworkConfigurationDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region To Domain from Entity

            #endregion
              
        }
    }
}
