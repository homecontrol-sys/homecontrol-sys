using AutoMapper;
using HomeControl.DAL.Entity.Identity;
using HomeControl.Domain.Identity;

namespace HomeControl.Service._Mapping
{
    public class RoleProfiles : Profile
    {
        public RoleProfiles()
        {

            #region From Domain to Entity

            #endregion

            #region To Domain from Entity

            #endregion

            #region Two Way
            
            CreateMap<RoleDomain, RoleEntity>().ReverseMap();

            #endregion 
              
        }
    }
}
