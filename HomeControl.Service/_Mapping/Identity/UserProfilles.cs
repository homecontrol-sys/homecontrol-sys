using API.Project.Common.Modules.Security._Extension;
using AutoMapper;
using HomeControl.DAL.Entity.Identity;
using HomeControl.Domain.Identity;

namespace HomeControl.Service._Mapping
{
    public class UserProfiles : Profile
    {
        public UserProfiles()
        {
            
            #region Bussines Models

            CreateMap<AuthModel, UserDomain>()
                .ForMember(dest => dest.UserName, opt =>
                { 
                    opt.Condition(src => !src.UserNameOrEMail.IsValidEmail());
                    opt.MapFrom(src => src.UserNameOrEMail);
                })
                .ForMember(dest => dest.Email, opt =>
                { 
                    opt.Condition(src => src.UserNameOrEMail.IsValidEmail());
                    opt.MapFrom(src => src.UserNameOrEMail);
                });
            CreateMap<UserDomain, AuthModel>();

            CreateMap<UserDomain, AuthObject>().ReverseMap();

            #endregion

            #region From Domain to Entity

            CreateMap<UserRoleDomain, UserRoleEntity>();
            CreateMap<UserClaimDomain, UserClaimEntity>();

            #endregion

            #region From Entity to Domain

            CreateMap<UserEntity, UserDomain>().ReverseMap();
            CreateMap<UserRoleEntity, UserRoleDomain>();
            CreateMap<UserClaimEntity, UserClaimDomain>();

            #endregion
        
        }
    }
}
