using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class PinTypeProfiles : Profile
    {
        public PinTypeProfiles()
        {
            
            #region Two Way
            
            CreateMap<PinTypeEntity, PinTypeDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region From Entity to Domain

            #endregion
              
        }
    }
}
