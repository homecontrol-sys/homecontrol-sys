using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class PinConnectionProfiles : Profile
    {
        public PinConnectionProfiles()
        {
            
            #region Two Way
            
            CreateMap<PinConnectionEntity, PinConnectionDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region From Entity to Domain

            #endregion
              
        }
    }
}
