using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class SystemProfiles : Profile
    {
        public SystemProfiles()
        {           

            #region Domain <-> Entity

            CreateMap<SystemDomain, SystemEntity>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region To Domain from Entity

            #endregion
              
        }
    }
}
