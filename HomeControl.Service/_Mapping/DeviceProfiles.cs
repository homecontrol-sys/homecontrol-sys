using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class DeviceProfiles : Profile
    {
        public DeviceProfiles()
        {
            
            #region Two Way
            
            CreateMap<DeviceEntity, DeviceDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region From Entity to Domain

            #endregion
              
        }
    }
}
