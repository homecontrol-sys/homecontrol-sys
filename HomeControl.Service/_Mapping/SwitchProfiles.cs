using AutoMapper;
using HomeControl.DAL.Entity;
using HomeControl.Domain;

namespace HomeControl.Service._Mapping
{
    public class SwitchProfiles : Profile
    {
        public SwitchProfiles()
        {

            #region Two Way
            
            CreateMap<SwitchEntity, SwitchDomain>().ReverseMap();

            #endregion

            #region From Domain to Entity

            #endregion

            #region To Domain from Entity

            #endregion
              
        }
    }
}
