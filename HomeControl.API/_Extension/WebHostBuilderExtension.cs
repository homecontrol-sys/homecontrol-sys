using Microsoft.AspNetCore.Hosting;
using Serilog;
using System.Reflection;

namespace HomeControl.API._Extension
{
    public static class WebHostBuilderExtension
    {

        public static IWebHostBuilder AddSeqLogging(this IWebHostBuilder hostBuilder)
        {
            return hostBuilder.UseSerilog((hostingContext, loggerConfiguration) =>
            {
                loggerConfiguration
                    .ReadFrom.Configuration(hostingContext.Configuration)
                    .Enrich.FromLogContext()
                    .Enrich.WithProperty("Assembly", Assembly.GetEntryAssembly()?.GetName()?.Name)
                    .WriteTo.Seq("http://logging");
            });
        }

    }
}
