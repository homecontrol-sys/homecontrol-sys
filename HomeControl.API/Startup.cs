using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using HomeControl.DAL;
using HomeControl.Service;
using HomeControl.Repository;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HomeControl.DAL._Extension;
using Serilog;
using System.Linq;
using Microsoft.AspNetCore.HttpOverrides;

namespace HomeControl.API
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            

            services.ProvideDataAccessLayer(Configuration);
            services.ProvideRepositoryLayer();
            services.ProvideServiceLayer(Configuration);

            #region Controller

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore)
                .ConfigureApiBehaviorOptions(setupAction =>
                    setupAction.InvalidModelStateResponseFactory = context =>
                    {
                        //create validation problem details, OR USE FluentValidation (Jeremy Skinner) github,, easier to unit test
                        var problemDetailsFactory = context.HttpContext.RequestServices.GetRequiredService<ProblemDetailsFactory>();
                        var problemDetails = problemDetailsFactory.CreateValidationProblemDetails(
                            context.HttpContext,
                            context.ModelState);
                        // aditional information
                        problemDetails.Detail = "See error field for details";
                        problemDetails.Instance = context.HttpContext.Request.Path;
          
                        //find out which status code to use
                        var actionExecutingContext = context as ActionExecutingContext;          
                        //by default all status codes is returned as 400 Bad Request, add exception for model validation can process creation
                        if ((context.ModelState.ErrorCount > 0) && (actionExecutingContext?.ActionArguments.Count == context.ActionDescriptor.Parameters.Count))
                        {
                            problemDetails.Type = "See error field for details";
                            problemDetails.Status = StatusCodes.Status422UnprocessableEntity;
                            problemDetails.Title = "One or more validation errors occurred.";
                            return new UnprocessableEntityObjectResult(problemDetails)
                            {
                                ContentTypes = { "application/problem+json" }
                            };
                        }

                        // if one or more argument incorrect 
                        problemDetails.Status = StatusCodes.Status400BadRequest;
                        problemDetails.Title = "One or more errors on input occurred.";
                        return new BadRequestObjectResult(problemDetails)
                        {
                            ContentTypes = { "application/problem+json" }
                        };
                    });

           
            #endregion

            #region Features

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "HomeControl.API", Version = "v1" });
                c.ResolveConflictingActions (apiDescriptions => apiDescriptions.First());
                });
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            #region ReverseProxy setup

            var forwardedHeaderOptions = new ForwardedHeadersOptions{
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            };
            forwardedHeaderOptions.KnownNetworks.Clear();
            forwardedHeaderOptions.KnownProxies.Clear();
            app.UseForwardedHeaders(forwardedHeaderOptions);

            #endregion

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "HomeControl.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(options =>
                options.WithOrigins(
                    "http://www-local.homecontrol.com:3001", 
                    "https://www-local.homecontrol.com:44395",
                    "http://localhost:3000"
                )
                .AllowAnyMethod()
                .AllowAnyHeader()
            );

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            #region Features

            app.UseSerilogRequestLogging();
            app.AutoMigrateDB();

            #endregion

        }
    }
}
