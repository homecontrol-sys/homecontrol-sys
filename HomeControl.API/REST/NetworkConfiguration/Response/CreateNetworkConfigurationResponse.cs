using System;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public class CreateNetworkConfigurationResponse : ICreateNetworkConfigurationResponse
    {

        #region Properties

        public Guid Id { get; set; }

        #endregion

    }

}