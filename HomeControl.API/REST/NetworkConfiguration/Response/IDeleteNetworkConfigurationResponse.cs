using System;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public interface IDeleteNetworkConfigurationResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}