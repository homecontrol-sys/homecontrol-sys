using System;
using HomeControl.API.REST.System;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public interface IReadNetworkConfigurationResponse
    {

        #region Properties

        Guid Id { get; set; }
        string SSID { get; set; }
        string Password { get; set; }
        bool IsConnectionVerified { get; set; }

        ReadSystemResponse System { get; set; }

        #endregion

    }

}