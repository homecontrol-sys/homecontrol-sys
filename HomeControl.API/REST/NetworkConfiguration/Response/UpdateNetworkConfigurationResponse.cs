using System;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public class UpdateNetworkConfigurationResponse : IUpdateNetworkConfigurationResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }

        #endregion

    }

}