using System;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public class DeleteNetworkConfigurationResponse : IDeleteNetworkConfigurationResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }

}