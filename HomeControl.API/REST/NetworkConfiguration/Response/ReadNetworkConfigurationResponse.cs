using System;
using HomeControl.API.REST.System;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public class ReadNetworkConfigurationResponse : IReadNetworkConfigurationResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string SSID { get; set; }
        public string Password { get; set; }
        public bool IsConnectionVerified { get; set; }

        public ReadSystemResponse System { get; set; }

        #endregion

    }

}