using System;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public interface ICreateNetworkConfigurationResponse
    {

        #region Properties

        Guid Id { get; set; }

        #endregion

    }

}