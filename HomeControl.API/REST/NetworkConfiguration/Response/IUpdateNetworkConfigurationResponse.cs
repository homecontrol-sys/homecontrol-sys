using System;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public interface IUpdateNetworkConfigurationResponse
    {

        #region Properties

        Guid Id { get; set; }
        string SSID { get; set; }
        string Password { get; set; }

        #endregion

    }

}