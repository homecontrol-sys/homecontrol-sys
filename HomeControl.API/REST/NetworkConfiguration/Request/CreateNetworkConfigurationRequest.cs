using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.NetworkConfiguration
{

    public class CreateNetworkConfigurationRequest : ICreateNetworkConfigurationRequest
    {
        
        #region Properties
        
        [Required]
        public string SSID { get; set; }
        [Required]
        public string Password { get; set; }

        #endregion

    }
    
}