namespace HomeControl.API.REST.NetworkConfiguration
{
    public interface ICreateNetworkConfigurationRequest
    {

        #region Properties

        string SSID { get; set; }
        string Password { get; set; }

        #endregion

    }
}