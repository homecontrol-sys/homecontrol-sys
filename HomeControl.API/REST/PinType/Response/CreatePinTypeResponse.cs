using System;

namespace HomeControl.API.REST.PinType
{

    public class CreatePinTypeResponse : ICreatePinTypeResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
