using System;

namespace HomeControl.API.REST.PinType
{

    public interface IReadPinTypeResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Alias { get; set; }

        #endregion
    
    }

}
