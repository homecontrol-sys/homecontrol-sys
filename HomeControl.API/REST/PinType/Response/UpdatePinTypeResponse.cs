using System;

namespace HomeControl.API.REST.PinType
{

    public class UpdatePinTypeResponse : IUpdatePinTypeResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
