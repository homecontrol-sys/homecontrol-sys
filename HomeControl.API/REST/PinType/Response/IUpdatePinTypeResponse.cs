using System;

namespace HomeControl.API.REST.PinType
{

    public interface IUpdatePinTypeResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
