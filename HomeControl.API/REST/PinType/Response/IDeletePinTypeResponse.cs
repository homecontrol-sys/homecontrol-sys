using System;

namespace HomeControl.API.REST.PinType
{

    public interface IDeletePinTypeResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
