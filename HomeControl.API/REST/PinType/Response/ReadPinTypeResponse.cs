using System;

namespace HomeControl.API.REST.PinType
{

    public class ReadPinTypeResponse : IReadPinTypeResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }

        #endregion

    }

}
