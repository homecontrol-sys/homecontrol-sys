using System;

namespace HomeControl.API.REST.PinType
{

    public interface IDeletePinTypeRequest
    {
    
        #region Properties

        Guid Id { get; set; }

        #endregion

    }

}