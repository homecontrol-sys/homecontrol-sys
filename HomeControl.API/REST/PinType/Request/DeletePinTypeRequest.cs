using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.PinType
{

    public class DeletePinTypeRequest : IDeletePinTypeRequest
    {
        
        #region Properties

        [Required]
        public Guid Id { get; set; }

        #endregion

    }
    
}