using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.PinType
{

    public class UpdatePinTypeRequest : IUpdatePinTypeRequest
    {
        
        #region Properties
        
        [Required]
        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }
    
}