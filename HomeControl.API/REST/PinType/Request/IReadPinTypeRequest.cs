using HomeControl.Domain._Common.DataShape;
using HomeControl.Domain;

namespace HomeControl.API.REST.PinType
{

    public interface IReadPinTypeRequest : IResourceParams<PinTypeFilter, PinTypeSearch>
    {

    }

}