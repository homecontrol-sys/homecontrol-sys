using System;

namespace HomeControl.API.REST.PinType
{

    public interface IUpdatePinTypeRequest
    {
    
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}