using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.PinType
{
    
    public class CreatePinTypeRequest : ICreatePinTypeRequest
    {
        
        #region Properties
        

        [Required]
        public string Name { get; set; }

        #endregion

    }
    
}