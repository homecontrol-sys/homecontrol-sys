namespace HomeControl.API.REST.PinType
{
    public interface ICreatePinTypeRequest
    {

        #region Properties
        
        string Name { get; set; }

        #endregion

    }
}