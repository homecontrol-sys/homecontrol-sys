using System;

namespace HomeControl.API.REST.PinConnection
{

    public class DeletePinConnectionResponse : IDeletePinConnectionResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
