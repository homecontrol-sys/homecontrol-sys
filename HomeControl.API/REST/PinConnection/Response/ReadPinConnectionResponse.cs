using System;

namespace HomeControl.API.REST.PinConnection
{

    public class ReadPinConnectionResponse : IReadPinConnectionResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }

        #endregion

    }

}
