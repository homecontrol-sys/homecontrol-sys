using System;

namespace HomeControl.API.REST.PinConnection
{

    public interface ICreatePinConnectionResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
