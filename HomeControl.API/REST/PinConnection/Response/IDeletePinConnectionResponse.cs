using System;

namespace HomeControl.API.REST.PinConnection
{

    public interface IDeletePinConnectionResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
