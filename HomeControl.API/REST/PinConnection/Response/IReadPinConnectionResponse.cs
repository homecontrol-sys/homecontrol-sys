using System;

namespace HomeControl.API.REST.PinConnection
{

    public interface IReadPinConnectionResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Alias { get; set; }

        #endregion
    
    }

}
