using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.PinConnection
{

    public class UpdatePinConnectionRequest : IUpdatePinConnectionRequest
    {
        
        #region Properties
        
        [Required]
        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }
    
}