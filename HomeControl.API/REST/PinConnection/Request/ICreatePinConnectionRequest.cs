namespace HomeControl.API.REST.PinConnection
{
    public interface ICreatePinConnectionRequest
    {

        #region Properties
        
        string Alias { get; set; }

        string Name { get; set; }

        #endregion

    }
}