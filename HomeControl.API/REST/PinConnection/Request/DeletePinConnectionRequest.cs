using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.PinConnection
{

    public class DeletePinConnectionRequest : IDeletePinConnectionRequest
    {
        
        #region Properties

        [Required]
        public Guid Id { get; set; }

        #endregion

    }
    
}