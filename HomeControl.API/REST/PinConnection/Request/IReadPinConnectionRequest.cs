using HomeControl.Domain._Common.DataShape;
using HomeControl.Domain;

namespace HomeControl.API.REST.PinConnection
{

    public interface IReadPinConnectionRequest : IResourceParams<PinConnectionFilter, PinConnectionSearch>
    {

    }

}