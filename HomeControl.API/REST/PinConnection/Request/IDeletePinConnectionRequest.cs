using System;

namespace HomeControl.API.REST.PinConnection
{

    public interface IDeletePinConnectionRequest
    {
    
        #region Properties

        Guid Id { get; set; }

        #endregion

    }

}