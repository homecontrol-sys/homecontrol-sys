using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.PinConnection
{
    
    public class CreatePinConnectionRequest : ICreatePinConnectionRequest
    {
        
        #region Properties
        
        [Required]
        public string Alias { get; set; }

        [Required]
        public string Name { get; set; }

        #endregion

    }
    
}