using System;

namespace HomeControl.API.REST.PinConnection
{

    public interface IUpdatePinConnectionRequest
    {
    
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}