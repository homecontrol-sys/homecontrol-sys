using System;
using System.Collections.Generic;
using HomeControl.Domain;

namespace HomeControl.API.REST.PinConnection
{

    public class ReadPinConnectionRequest : IReadPinConnectionRequest
    {
        
        public IEnumerable<string> Prefetch { get; set; }
        public PinConnectionFilter Filter { get; set; }
        public PinConnectionSearch SearchBy { get; set; }

        #region Paging 

        #region Fields

        const int _maxPageSize = 20;
        private int _pageSize = 10;

        #endregion

        #region Properties

        public int PageNumber { get; set; } = 1;
        public int PageSize 
        { 
            get => _pageSize;
            set => _pageSize = (value > _maxPageSize) ? _maxPageSize : value;
        }

        #endregion

        #endregion

    }
    
}