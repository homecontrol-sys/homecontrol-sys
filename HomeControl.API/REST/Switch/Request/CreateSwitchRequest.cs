namespace HomeControl.API.REST.Switch
{

    public class CreateSwitchRequest : ICreateSwitchRequest
    {
        
        #region Properties
        
        public string Name { get; set; }

        #endregion

    }
    
}