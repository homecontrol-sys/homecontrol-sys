using HomeControl.Domain._Common.DataShape;
using HomeControl.Domain;

namespace HomeControl.API.REST.Switch
{
    public interface IReadSwitchRequest : IResourceParams<SwitchFilter, SwitchSearch>
    {

    }
}