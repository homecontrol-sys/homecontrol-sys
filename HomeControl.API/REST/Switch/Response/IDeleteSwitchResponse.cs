using System;

namespace HomeControl.API.REST.Switch
{

    public interface IDeleteSwitchResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}