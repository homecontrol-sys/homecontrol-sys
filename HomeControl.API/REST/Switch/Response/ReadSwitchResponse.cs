using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;
using HomeControl.API.REST.Element;
using HomeControl.API.REST.SwitchState;

namespace HomeControl.API.REST.Switch
{

    public class ReadSwitchResponse : LookupBaseModel, IReadSwitchResponse
    {

        #region Properties

        public string Alias { get; set; }
        public int Dilay { get; set; }
        
        public ICollection<ReadElementResponse> Elements { get; set; }
        public ICollection<ReadSwitchStateResponse> SwitchStates { get; set; }

        #endregion

    }

}