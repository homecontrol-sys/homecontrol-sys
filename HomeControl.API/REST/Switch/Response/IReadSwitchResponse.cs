using System;
using System.Collections.Generic;
using API.Project.Common.Architecture.DAL;
using HomeControl.API.REST.Element;
using HomeControl.API.REST.SwitchState;

namespace HomeControl.API.REST.Switch
{

    public interface IReadSwitchResponse : ILookupBaseModel
    {

        #region Properties

        string Alias { get; set; }
        int Dilay { get; set; }
        
        ICollection<ReadElementResponse> Elements { get; set; }
        ICollection<ReadSwitchStateResponse> SwitchStates { get; set; }

        #endregion

    }
}