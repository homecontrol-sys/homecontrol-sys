using System;

namespace HomeControl.API.REST.Switch
{

    public interface ICreateSwitchResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}