using System;

namespace HomeControl.API.REST.Switch
{

    public class UpdateSwitchResponse : IUpdateSwitchResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }

}