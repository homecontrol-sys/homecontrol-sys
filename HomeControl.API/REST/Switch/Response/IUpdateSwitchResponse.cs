using System;

namespace HomeControl.API.REST.Switch
{

    public interface IUpdateSwitchResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}