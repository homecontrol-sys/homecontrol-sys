using System;

namespace HomeControl.API.REST.Switch
{

    public class DeleteSwitchResponse : IDeleteSwitchResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }

}