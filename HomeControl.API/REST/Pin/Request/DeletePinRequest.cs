using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Pin
{

    public class DeletePinRequest : IDeletePinRequest
    {
        
        #region Properties

        [Required]
        public Guid Id { get; set; }

        #endregion

    }
    
}