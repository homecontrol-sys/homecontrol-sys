namespace HomeControl.API.REST.Pin
{
    public interface ICreatePinRequest
    {

        #region Properties
        
        string Name { get; set; }

        #endregion

    }
}