using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Pin
{

    public class UpdatePinRequest : IUpdatePinRequest
    {
        
        #region Properties
        
        [Required]
        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }
    
}