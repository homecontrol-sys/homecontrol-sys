using HomeControl.Domain._Common.DataShape;
using HomeControl.Domain;

namespace HomeControl.API.REST.Pin
{

    public interface IReadPinRequest : IResourceParams<PinFilter, PinSearch>
    {

    }

}