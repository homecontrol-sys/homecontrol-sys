namespace HomeControl.API.REST.Pin
{
    
    public class CreatePinRequest : ICreatePinRequest
    {
        
        #region Properties
        
        public string Name { get; set; }

        #endregion

    }
    
}