using System;

namespace HomeControl.API.REST.Pin
{

    public interface IDeletePinRequest
    {
    
        #region Properties

        Guid Id { get; set; }

        #endregion

    }

}