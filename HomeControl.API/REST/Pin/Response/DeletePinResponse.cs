using System;

namespace HomeControl.API.REST.Pin
{

    public class DeletePinResponse : IDeletePinResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
