using System;

namespace HomeControl.API.REST.Pin
{

    public class CreatePinResponse : ICreatePinResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
