using System;

namespace HomeControl.API.REST.Pin
{

    public interface ICreatePinResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
