using System;

namespace HomeControl.API.REST.Pin
{

    public interface IReadPinResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Alias { get; set; }

        #endregion
    
    }

}
