using System;

namespace HomeControl.API.REST.Pin
{

    public class UpdatePinResponse : IUpdatePinResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
