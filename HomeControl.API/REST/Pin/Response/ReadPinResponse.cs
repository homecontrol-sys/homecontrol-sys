using System;

namespace HomeControl.API.REST.Pin
{

    public class ReadPinResponse : IReadPinResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }

        #endregion

    }

}
