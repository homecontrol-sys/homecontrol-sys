using System;

namespace HomeControl.API.REST.System
{

    public class ConfigureMainSystemResponse : IConfigureMainSystemResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
