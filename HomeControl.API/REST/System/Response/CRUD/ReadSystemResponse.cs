using System;
using HomeControl.API.REST.NetworkConfiguration;

namespace HomeControl.API.REST.System
{

    public class ReadSystemResponse : IReadSystemResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsMain { get; set; }
        
        public Guid? NetworkConfigurationId { get; set; }
        public ReadNetworkConfigurationResponse NetworkConfiguration { get; set; }

        #endregion

    }

}
