using System;

namespace HomeControl.API.REST.System
{

    public class DeleteSystemResponse : IDeleteSystemResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
