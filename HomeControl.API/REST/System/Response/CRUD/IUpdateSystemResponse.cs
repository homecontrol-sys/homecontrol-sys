using System;

namespace HomeControl.API.REST.System
{

    public interface IUpdateSystemResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
