using System;
using HomeControl.API.REST.NetworkConfiguration;

namespace HomeControl.API.REST.System
{

    public interface IReadSystemResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        bool IsMain { get; set; }

        Guid? NetworkConfigurationId { get; set; }
        ReadNetworkConfigurationResponse NetworkConfiguration { get; set; }

        #endregion
    
    }

}
