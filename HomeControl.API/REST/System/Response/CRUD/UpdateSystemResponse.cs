using System;

namespace HomeControl.API.REST.System
{

    public class UpdateSystemResponse : IUpdateSystemResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
