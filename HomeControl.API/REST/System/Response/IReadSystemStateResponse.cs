using System;
using System.Collections.Generic;
using HomeControl.API.REST.Element;
using HomeControl.API.REST.NetworkConfiguration;

namespace HomeControl.API.REST.System
{

    public interface IReadSystemStateResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        bool IsMain { get; set; }

        Guid? NetworkConfigurationId { get; set; }
        ReadNetworkConfigurationResponse NetworkConfiguration { get; set; }
        ICollection<ReadElementResponse> Elements { get; set; }

        #endregion
    
    }

}
