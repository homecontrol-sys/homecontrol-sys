using System;

namespace HomeControl.API.REST.System
{

    public interface IConfigureMainSystemResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
