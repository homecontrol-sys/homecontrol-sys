using System;
using System.Collections.Generic;
using HomeControl.API.REST.Element;
using HomeControl.API.REST.NetworkConfiguration;

namespace HomeControl.API.REST.System
{

    public class ReadSystemStateResponse : IReadSystemStateResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsMain { get; set; }
        
        public Guid? NetworkConfigurationId { get; set; }
        public ReadNetworkConfigurationResponse NetworkConfiguration { get; set; }
        public ICollection<ReadElementResponse> Elements { get; set; }

        #endregion

    }

}
