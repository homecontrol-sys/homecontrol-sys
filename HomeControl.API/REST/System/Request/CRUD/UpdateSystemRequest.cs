using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.System
{

    public class UpdateSystemRequest : IUpdateSystemRequest
    {
        
        #region Properties
        
        [Required]
        public string Name { get; set; }
        public Guid Id { get; set; }

        #endregion

    }
    
}