using HomeControl.Domain._Common.DataShape;
using HomeControl.Domain;

namespace HomeControl.API.REST.System
{

    public interface IReadSystemRequest : IResourceParams<SystemFilter, SystemSearch>
    {
    
    }

}