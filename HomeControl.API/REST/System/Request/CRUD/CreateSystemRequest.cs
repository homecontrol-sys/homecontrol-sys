using System;
using System.ComponentModel.DataAnnotations;
using HomeControl.API.REST._Common;

namespace HomeControl.API.REST.System
{
    
    [TitleMustBeDifferentFromDescription]
    public class CreateSystemRequest : ICreateSystemRequest
    {
        
        #region Properties

        [Required]
        [MaxLength(32)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        #endregion

    }
    
}