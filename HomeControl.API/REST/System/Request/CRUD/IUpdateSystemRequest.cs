using System;

namespace HomeControl.API.REST.System
{

    public interface IUpdateSystemRequest
    {
    
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}