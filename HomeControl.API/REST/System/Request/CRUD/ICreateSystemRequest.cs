using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.System
{
    public interface ICreateSystemRequest
    {

        #region Properties
        
        [Required]
        string Name { get; set; }
        string Description { get; set; }

        #endregion

    }
}