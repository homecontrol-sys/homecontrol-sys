using System;

namespace HomeControl.API.REST.Element
{
    public class SwitchElementStateRequest : ISwitchElementStateRequest
    {
        
        #region Properties

        public Guid ElementId { get; set; }
        public Guid NewElementSwitchStateId { get; set; }
        public string Name { get; set; }

        #endregion

    }
}
