namespace HomeControl.API.REST.Element
{

    public class CreateElementRequest : ICreateElementRequest
    {
        
        #region Properties
        
        public string Name { get; set; }

        #endregion

    }
    
}