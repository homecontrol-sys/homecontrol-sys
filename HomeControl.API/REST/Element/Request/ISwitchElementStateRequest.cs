using System;

namespace HomeControl.API.REST.Element
{
    public interface ISwitchElementStateRequest
    {
        
        #region Properties

        Guid ElementId { get; set; }
        Guid NewElementSwitchStateId { get; set; }
        string Name { get; set; }

        #endregion

    }
}