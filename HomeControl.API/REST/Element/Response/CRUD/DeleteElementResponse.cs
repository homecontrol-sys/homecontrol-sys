using System;

namespace HomeControl.API.REST.Element
{

    public class DeleteElementResponse : IDeleteElementResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }

}