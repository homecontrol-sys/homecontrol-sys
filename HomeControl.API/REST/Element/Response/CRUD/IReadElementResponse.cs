using System;
using System.Collections.Generic;
using HomeControl.API.REST.Switch;
using HomeControl.API.REST.SwitchState;
using HomeControl.API.REST.System;

namespace HomeControl.API.REST.Element
{

    public interface IReadElementResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Alias { get; set; }
        string Name { get; set; }

        ReadSwitchResponse Switch { get; set; }
        ReadSwitchStateResponse SwitchState { get; set; }
        // Guid? DeviceId { get; set; }
        // Guid SystemId { get; set; }
        // ReadSystemRequest System { get; set; }
        // ICollection<ReadSwitchStateRequest> SwitchStates { get; set; }
        // Guid SwitchSwitchId { get; set; }
        // ReadSwitchSwitchRequest SwitchSwitch { get; set;}

        #endregion

    }

}