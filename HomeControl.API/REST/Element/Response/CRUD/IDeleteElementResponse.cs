using System;

namespace HomeControl.API.REST.Element
{

    public interface IDeleteElementResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}