using System;

namespace HomeControl.API.REST.Element
{

    public class CreateElementResponse : ICreateElementResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }

}