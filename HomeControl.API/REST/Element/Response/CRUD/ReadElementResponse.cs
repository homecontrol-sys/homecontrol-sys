using System;
using System.Collections.Generic;
using HomeControl.API.REST.Switch;
using HomeControl.API.REST.SwitchState;
using HomeControl.API.REST.System;

namespace HomeControl.API.REST.Element
{

    public class ReadElementResponse : IReadElementResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }

        public ReadSwitchResponse Switch { get; set; }
        public ReadSwitchStateResponse SwitchState { get; set; }


        // public Guid? DeviceId { get; set; }
        // public Guid SystemId { get; set; }
        // public ReadSystemRequest System { get; set; }
        // public ICollection<ReadSwitchStateRequest> SwitchStates { get; set; }
        // public Guid SwitchSwitchId { get; set; }
        // public ReadSwitchSwitchRequest SwitchSwitch { get; set;}

        #endregion

    }

}