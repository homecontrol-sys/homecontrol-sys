using System.ComponentModel.DataAnnotations;
using HomeControl.API.REST.System;

namespace HomeControl.API.REST._Common
{

    public class TitleMustBeDifferentFromDescription : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var system = (CreateSystemRequest)validationContext.ObjectInstance;
            
            if (system.Name == system.Description)
            {
                return new ValidationResult(
                    "The provided description should be different from the title.",
                    new[] {"CreateSystemRequest"});
            }

            return ValidationResult.Success;
        }
    }

}
