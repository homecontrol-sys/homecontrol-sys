using System;

namespace HomeControl.API.REST.SwitchState
{

    public interface ICreateSwitchStateResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}