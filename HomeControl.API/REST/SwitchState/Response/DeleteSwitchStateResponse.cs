using System;

namespace HomeControl.API.REST.SwitchState
{

    public class DeleteSwitchStateResponse : IDeleteSwitchStateResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }

}