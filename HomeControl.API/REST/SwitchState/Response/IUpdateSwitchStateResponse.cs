using System;

namespace HomeControl.API.REST.SwitchState
{

    public interface IUpdateSwitchStateResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}
