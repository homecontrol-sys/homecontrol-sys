using System.Collections.Generic;
using HomeControl.Domain;

namespace HomeControl.API.REST.SwitchState
{

    public class ReadSwitchStateRequest : IReadSwitchStateRequest
    {
        
        #region Properties
        
        public IEnumerable<string> Prefetch { get; set; }
        public SwitchStateFilter Filter { get; set; }
        public SwitchStateSearch SearchBy { get; set; }

        #region Paging 

        #region Fields

        const int _maxPageSize = 20;
        private int _pageSize = 10;

        #endregion

        #region Properties

        public int PageNumber { get; set; } = 1;
        public int PageSize 
        { 
            get => _pageSize;
            set => _pageSize = (value > _maxPageSize) ? _maxPageSize : value;
        }

        #endregion

        #endregion

        #endregion

    }
    
}
