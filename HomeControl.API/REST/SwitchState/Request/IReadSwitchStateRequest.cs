using HomeControl.Domain._Common.DataShape;
using HomeControl.Domain;

namespace HomeControl.API.REST.SwitchState
{
    public interface IReadSwitchStateRequest : IResourceParams<SwitchStateFilter, SwitchStateSearch>
    {

        #region Properties

        #endregion

    }
}