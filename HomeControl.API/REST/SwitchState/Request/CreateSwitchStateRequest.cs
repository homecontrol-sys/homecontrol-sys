namespace HomeControl.API.REST.SwitchState
{

    public class CreateSwitchStateRequest : ICreateSwitchStateRequest
    {
        
        #region Properties
        
        public string Name { get; set; }

        #endregion

    }
    
}