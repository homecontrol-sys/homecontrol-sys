using System;

namespace HomeControl.API.REST.Identity.User
{

    public interface IDeleteUserRequest
    {
    
        #region Properties
        
        Guid Id { get; set; }
        string FullName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string EMail { get; set; }

        #endregion

    }

}