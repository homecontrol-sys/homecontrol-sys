using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Identity.User
{
    public class CreateUserRequest : ICreateUserRequest
    {
        
        #region Properties
        
        [Required]
        public string UserNameOrEMail { get; set; }
        [Required]
        public string Password { get; set; }

        #endregion

    }
}