using System;

namespace HomeControl.API.REST.Identity.User
{

    public class DeleteUserRequest : IDeleteUserRequest
    {

        #region Properties

        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EMail { get; set; }

        #endregion

    }

}