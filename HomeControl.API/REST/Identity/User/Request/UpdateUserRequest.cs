using System;

namespace HomeControl.API.REST.Identity.User
{

    public class UpdateUserRequest : IUpdateUserRequest
    {

        #region Properties

        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }


        #endregion

    }

}