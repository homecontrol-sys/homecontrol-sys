using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Identity.User
{
    public interface ICreateUserRequest
    {

        #region Properties
        
        [Required]
        string UserNameOrEMail { get; set; }
        [Required]
        string Password { get; set; }

        #endregion

    }
}