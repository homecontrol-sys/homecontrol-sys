using System;

namespace HomeControl.API.REST.Identity.User
{

    public interface IUpdateUserRequest
    {
    
        #region Properties
        
        string UserName { get; set; }
        string Email { get; set; }
        string Password { get; set; }

        #endregion

    }

}