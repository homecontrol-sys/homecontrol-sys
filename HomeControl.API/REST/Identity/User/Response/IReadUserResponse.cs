using System;

namespace HomeControl.API.REST.Identity.User
{

    public interface IReadUserResponse
    {

        #region Properties

        Guid Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string UserName { get; set; }
        string Email { get; set; }
        bool EmailConfirmed { get; set; }

        #endregion
    
    }

}
