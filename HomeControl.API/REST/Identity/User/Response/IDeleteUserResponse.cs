using System;

namespace HomeControl.API.REST.Identity.User
{

    public interface IDeleteUserResponse
    {

        #region Properties

        Guid Id { get; set; }
        string FirstName { get; set; }

        #endregion
    
    }

}
