using System;

namespace HomeControl.API.REST.Identity.User
{

    public class UpdateUserResponse : IUpdateUserResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string FirstName { get; set; }

        #endregion
    
    }

}
