using System;

namespace HomeControl.API.REST.Identity.User
{

    public class ReadUserResponse : IReadUserResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }

        #endregion
    
    }

}
