using System;

namespace HomeControl.API.REST.Identity.User
{

    public interface ICreateUserResponse
    {

        #region Properties

        Guid Id { get; set; }
        string FirstName { get; set; }

        #endregion
    
    }

}
