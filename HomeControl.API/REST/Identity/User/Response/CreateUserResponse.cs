using System;

namespace HomeControl.API.REST.Identity.User
{

    public class CreateUserResponse : ICreateUserResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string FirstName { get; set; }

        #endregion
    
    }

}
