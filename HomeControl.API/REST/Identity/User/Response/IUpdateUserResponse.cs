using System;

namespace HomeControl.API.REST.Identity.User
{

    public interface IUpdateUserResponse
    {

        #region Properties

        Guid Id { get; set; }
        string FirstName { get; set; }

        #endregion
    
    }

}
