using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Identity.Auth
{
    public interface ILoginResponse
    {

        #region Properties
        
        [Required]
        string UserNameOrEMail { get; set; }
        [Required]
        string Password { get; set; }

        #endregion

    }
}