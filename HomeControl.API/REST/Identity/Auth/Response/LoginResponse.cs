using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Identity.Auth
{
    public class LoginResponse : ILoginResponse
    {
        
        #region Properties
        
        [Required]
        public string UserNameOrEMail { get; set; }
        [Required]
        public string Password { get; set; }

        #endregion

    }
}