using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Identity.Auth
{
    public class LoginRequest : ILoginRequest
    {

        #region Properties
        
        [Required]
        public string UserNameOrEMail { get; set; }
        [Required]
        public string Password { get; set; }

        #endregion

    }
}