﻿using AutoMapper;
using HomeControl.API.REST.System;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class SystemProfiles : Profile
    {
        
        public SystemProfiles()
        {

            #region REST To Domain

            #region CRUD

            CreateMap<CreateSystemRequest, SystemDomain>();
            CreateMap<ReadSystemRequest, SystemDomain>();
            CreateMap<UpdateSystemRequest, SystemDomain>();
            CreateMap<DeleteSystemRequest, SystemDomain>();

            #endregion

            #endregion

            #region Domain to REST

            #region CRUD

            CreateMap<SystemDomain, CreateSystemResponse>();
            CreateMap<SystemDomain, ReadSystemResponse>();
            CreateMap<SystemDomain, UpdateSystemResponse>();
            CreateMap<SystemDomain, DeleteSystemResponse>();

            #endregion

            #region STATE

            CreateMap<SystemDomain, ReadSystemStateResponse>();

            #endregion

            #endregion        

        }

    }

}
