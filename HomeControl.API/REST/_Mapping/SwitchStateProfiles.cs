﻿using AutoMapper;
using HomeControl.API.REST.SwitchState;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class SwitchStateProfiles : Profile
    {
        
        public SwitchStateProfiles()
        {

           #region From REST(Request) to Domain

            #region Crud DTOs

            CreateMap<CreateSwitchStateRequest, SwitchStateDomain>();
            CreateMap<ReadSwitchStateRequest, SwitchStateDomain>();
            CreateMap<UpdateSwitchStateRequest, SwitchStateDomain>();
            CreateMap<DeleteSwitchStateRequest, SwitchStateDomain>();

            #endregion

            #endregion

            #region From Domain to REST(Response)

            #region Crud DTOs

            CreateMap<SwitchStateDomain, CreateSwitchStateResponse>();
            CreateMap<SwitchStateDomain, ReadSwitchStateResponse>();
            CreateMap<SwitchStateDomain, UpdateSwitchStateResponse>();
            CreateMap<SwitchStateDomain, DeleteSwitchStateResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
