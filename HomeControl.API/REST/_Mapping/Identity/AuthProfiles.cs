﻿using AutoMapper;
using HomeControl.API.REST.Identity.Auth;
using HomeControl.API.REST.Identity.User;
using HomeControl.Domain.Identity;

namespace HomeControl.API.REST._Mapping
{

    public class AuthProfiles : Profile
    {
        
        public AuthProfiles()
        {

            #region From REST(Request) to Domain

            #region Bussines Models

            CreateMap<LoginRequest, AuthModel>();

            #endregion

            #endregion

        }

    }

}
