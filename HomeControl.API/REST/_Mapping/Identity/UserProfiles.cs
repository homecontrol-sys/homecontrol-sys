﻿using AutoMapper;
using HomeControl.API.REST.Identity.Auth;
using HomeControl.API.REST.Identity.User;
using HomeControl.Domain.Identity;

namespace HomeControl.API.REST._Mapping
{

    public class UserProfiles : Profile
    {
        
        public UserProfiles()
        {

            #region From REST(Request) to Domain

            #region Bussines Models

            CreateMap<LoginRequest, AuthModel>();

            #endregion

            #region CRUD DTOs

            CreateMap<CreateUserRequest, UserDomain>();
            CreateMap<ReadUserRequest, UserDomain>();
            CreateMap<UpdateUserRequest, UserDomain>();
            CreateMap<DeleteUserRequest, UserDomain>();

            #endregion

            #endregion


            #region To REST(Response) from Domain

            #region Bussines Models 

            CreateMap<UserDomain, LoginResponse>();

            #endregion

            #region CRUD DTOs

            CreateMap<UserDomain, CreateUserResponse>();
            CreateMap<UserDomain, ReadUserResponse>();
            CreateMap<UserDomain, UpdateUserResponse>();
            CreateMap<UserDomain, DeleteUserResponse>();

            #endregion

            #endregion
            
        }

    }

}
