﻿using AutoMapper;
using HomeControl.API.REST.Element;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class ElementProfiles : Profile
    {
        
        public ElementProfiles()
        {

           #region From REST(Request) to Domain

            #region Crud DTOs

            CreateMap<CreateElementRequest, ElementDomain>();
            CreateMap<ReadElementRequest, ElementDomain>();
            CreateMap<UpdateElementRequest, ElementDomain>();
            CreateMap<DeleteElementRequest, ElementDomain>();

            #endregion

            #endregion

            #region From Domain to REST(Response)

            #region Crud DTOs

            CreateMap<ElementDomain, CreateElementResponse>();
            CreateMap<ElementDomain, ReadElementResponse>();
            CreateMap<ElementDomain, UpdateElementResponse>();
            CreateMap<ElementDomain, DeleteElementResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
