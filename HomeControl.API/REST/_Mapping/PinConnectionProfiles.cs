﻿using AutoMapper;
using HomeControl.API.REST.PinConnection;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class PinConnectionProfiles : Profile
    {
        
        public PinConnectionProfiles()
        {

            #region From REST(Request) to Domain

            #region PinConnection

            CreateMap<CreatePinConnectionRequest, PinConnectionDomain>();
            CreateMap<ReadPinConnectionRequest, PinConnectionDomain>();
            CreateMap<UpdatePinConnectionRequest, PinConnectionDomain>();
            CreateMap<DeletePinConnectionRequest, PinConnectionDomain>();

            #region PinConnectionCategory


            #endregion

            #endregion

            #endregion

            #region From Domain to REST(Response)
            
            #region PinConnection

            CreateMap<PinConnectionDomain, CreatePinConnectionResponse>();
            CreateMap<PinConnectionDomain, ReadPinConnectionResponse>();
            CreateMap<PinConnectionDomain, UpdatePinConnectionResponse>();
            CreateMap<PinConnectionDomain, DeletePinConnectionResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
