﻿using AutoMapper;
using HomeControl.API.REST.Pin;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class PinProfiles : Profile
    {
        
        public PinProfiles()
        {

            #region From REST(Request) to Domain

            #region Pin

            CreateMap<CreatePinRequest, PinDomain>();
            CreateMap<ReadPinRequest, PinDomain>();
            CreateMap<UpdatePinRequest, PinDomain>();
            CreateMap<DeletePinRequest, PinDomain>();

            #region PinCategory


            #endregion

            #endregion

            #endregion

            #region From Domain to REST(Response)
            
            #region Pin

            CreateMap<PinDomain, CreatePinResponse>();
            CreateMap<PinDomain, ReadPinResponse>();
            CreateMap<PinDomain, UpdatePinResponse>();
            CreateMap<PinDomain, DeletePinResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
