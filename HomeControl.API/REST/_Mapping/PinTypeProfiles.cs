﻿using AutoMapper;
using HomeControl.API.REST.PinType;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class PinTypeProfiles : Profile
    {
        
        public PinTypeProfiles()
        {

            #region From REST(Request) to Domain

            #region PinType

            CreateMap<CreatePinTypeRequest, PinTypeDomain>();
            CreateMap<ReadPinTypeRequest, PinTypeDomain>();
            CreateMap<UpdatePinTypeRequest, PinTypeDomain>();
            CreateMap<DeletePinTypeRequest, PinTypeDomain>();

            #region PinTypeCategory


            #endregion

            #endregion

            #endregion

            #region From Domain to REST(Response)
            
            #region PinType

            CreateMap<PinTypeDomain, CreatePinTypeResponse>();
            CreateMap<PinTypeDomain, ReadPinTypeResponse>();
            CreateMap<PinTypeDomain, UpdatePinTypeResponse>();
            CreateMap<PinTypeDomain, DeletePinTypeResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
