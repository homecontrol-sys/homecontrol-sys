﻿using AutoMapper;
using HomeControl.API.REST.Switch;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class SwitchProfiles : Profile
    {
        
        public SwitchProfiles()
        {

           #region From REST(Request) to Domain

            #region Crud DTOs

            CreateMap<CreateSwitchRequest, SwitchDomain>();
            CreateMap<ReadSwitchRequest, SwitchDomain>();
            CreateMap<UpdateSwitchRequest, SwitchDomain>();
            CreateMap<DeleteSwitchRequest, SwitchDomain>();

            #endregion

            #endregion

            #region From Domain to REST(Response)

            #region Crud DTOs

            CreateMap<SwitchDomain, CreateSwitchResponse>();
            CreateMap<SwitchDomain, ReadSwitchResponse>();
            CreateMap<SwitchDomain, UpdateSwitchResponse>();
            CreateMap<SwitchDomain, DeleteSwitchResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
