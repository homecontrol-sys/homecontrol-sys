﻿using AutoMapper;
using HomeControl.API.REST.Device;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class DeviceProfiles : Profile
    {
        
        public DeviceProfiles()
        {

            #region From REST(Request) to Domain

            #region Crud DTOs

            CreateMap<CreateDeviceRequest, DeviceDomain>();
            CreateMap<ReadDeviceRequest, DeviceDomain>();
            CreateMap<UpdateDeviceRequest, DeviceDomain>();
            CreateMap<DeleteDeviceRequest, DeviceDomain>();

            #endregion

            #endregion

            #region From Domain to REST(Response)

            #region Crud DTOs

            CreateMap<DeviceDomain, CreateDeviceResponse>();
            CreateMap<DeviceDomain, ReadDeviceResponse>();
            CreateMap<DeviceDomain, UpdateDeviceResponse>();
            CreateMap<DeviceDomain, DeleteDeviceResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
