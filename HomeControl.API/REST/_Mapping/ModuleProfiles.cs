﻿using AutoMapper;
using HomeControl.API.REST.Module;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class ModuleProfiles : Profile
    {
        
        public ModuleProfiles()
        {

            #region From REST(Request) to Domain

            #region Module

            CreateMap<CreateModuleRequest, ModuleDomain>();
            CreateMap<ReadModuleRequest, ModuleDomain>();
            CreateMap<UpdateModuleRequest, ModuleDomain>();
            CreateMap<DeleteModuleRequest, ModuleDomain>();

            #region ModuleCategory


            #endregion

            #endregion

            #endregion

            #region From Domain to REST(Response)
            
            #region Module

            CreateMap<ModuleDomain, CreateModuleResponse>();
            CreateMap<ModuleDomain, ReadModuleResponse>();
            CreateMap<ModuleDomain, UpdateModuleResponse>();
            CreateMap<ModuleDomain, DeleteModuleResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
