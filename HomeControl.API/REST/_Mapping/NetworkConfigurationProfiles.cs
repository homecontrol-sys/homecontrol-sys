﻿using AutoMapper;
using HomeControl.API.REST.NetworkConfiguration;
using HomeControl.Domain;

namespace HomeControl.API.REST._Mapping
{

    public class NetworkConfigurationProfiles : Profile
    {
        
        public NetworkConfigurationProfiles()
        {

           #region From REST(Request) to Domain

            #region Crud DTOs

            CreateMap<CreateNetworkConfigurationRequest, NetworkConfigurationDomain>();
            CreateMap<ReadNetworkConfigurationRequest, NetworkConfigurationDomain>();
            CreateMap<UpdateNetworkConfigurationRequest, NetworkConfigurationDomain>();
            CreateMap<DeleteNetworkConfigurationRequest, NetworkConfigurationDomain>();

            #endregion

            #endregion

            #region From Domain to REST(Response)

            #region Crud DTOs

            CreateMap<NetworkConfigurationDomain, CreateNetworkConfigurationResponse>();
            CreateMap<NetworkConfigurationDomain, ReadNetworkConfigurationResponse>();
            CreateMap<NetworkConfigurationDomain, UpdateNetworkConfigurationResponse>();
            CreateMap<NetworkConfigurationDomain, DeleteNetworkConfigurationResponse>();
            
            #endregion
            
            #endregion
        
        }

    }

}
