using System;
using System.ComponentModel.DataAnnotations;
using HomeControl.API.REST._Common;

namespace HomeControl.API.REST.Module
{
    
    public class CreateModuleRequest : ICreateModuleRequest
    {
        
        #region Properties
        
        [Required]
        public string Alias { get; set; }

        [Required]
        public string Name { get; set; }

        #endregion

    }
    
}