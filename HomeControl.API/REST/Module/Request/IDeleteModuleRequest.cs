using System;

namespace HomeControl.API.REST.Module
{

    public interface IDeleteModuleRequest
    {
    
        #region Properties

        Guid Id { get; set; }

        #endregion

    }

}