using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Module
{

    public class UpdateModuleRequest : IUpdateModuleRequest
    {
        
        #region Properties
        
        [Required]
        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }
    
}