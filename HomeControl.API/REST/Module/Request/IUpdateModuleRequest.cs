using System;

namespace HomeControl.API.REST.Module
{

    public interface IUpdateModuleRequest
    {
    
        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}