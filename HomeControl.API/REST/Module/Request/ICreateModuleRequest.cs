namespace HomeControl.API.REST.Module
{
    public interface ICreateModuleRequest
    {

        #region Properties
        
        string Alias { get; set; }

        string Name { get; set; }

        #endregion

    }
}