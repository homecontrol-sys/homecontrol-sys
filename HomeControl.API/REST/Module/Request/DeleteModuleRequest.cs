using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Module
{

    public class DeleteModuleRequest : IDeleteModuleRequest
    {
        
        #region Properties

        [Required]
        public Guid Id { get; set; }

        #endregion

    }
    
}