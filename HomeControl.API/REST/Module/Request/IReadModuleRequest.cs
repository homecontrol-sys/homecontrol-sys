using HomeControl.Domain._Common.DataShape;
using HomeControl.Domain;

namespace HomeControl.API.REST.Module
{

    public interface IReadModuleRequest : IResourceParams<ModuleFilter, ModuleSearch>
    {

    }

}