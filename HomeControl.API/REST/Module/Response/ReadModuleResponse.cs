using System;

namespace HomeControl.API.REST.Module
{

    public class ReadModuleResponse : IReadModuleResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }

        #endregion

    }

}
