using System;

namespace HomeControl.API.REST.Module
{

    public interface IDeleteModuleResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
