using System;

namespace HomeControl.API.REST.Module
{

    public interface IReadModuleResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        string Alias { get; set; }

        #endregion
    
    }

}
