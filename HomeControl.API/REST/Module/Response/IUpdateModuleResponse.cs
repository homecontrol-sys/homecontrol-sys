using System;

namespace HomeControl.API.REST.Module
{

    public interface IUpdateModuleResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion
    
    }

}
