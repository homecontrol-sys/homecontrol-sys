using System;

namespace HomeControl.API.REST.Module
{

    public class UpdateModuleResponse : IUpdateModuleResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
