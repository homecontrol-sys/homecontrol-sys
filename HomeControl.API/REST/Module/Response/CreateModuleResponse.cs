using System;

namespace HomeControl.API.REST.Module
{

    public class CreateModuleResponse : ICreateModuleResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion
    
    }

}
