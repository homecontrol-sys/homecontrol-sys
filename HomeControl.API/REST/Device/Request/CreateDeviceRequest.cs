using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Device
{
    public class CreateDeviceRequest : ICreateDeviceRequest
    {
        
        #region Properties
        
        [Required]
        public string Name { get; set; }
        [Required]
        public Guid SystemId { get; set; }

        #endregion

    }
}