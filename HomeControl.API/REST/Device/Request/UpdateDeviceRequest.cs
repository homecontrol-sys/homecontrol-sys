using System;

namespace HomeControl.API.REST.Device
{
    public class UpdateDeviceRequest : IUpdateDeviceRequest
    {
        
        #region Properties
        
        public string Name { get; set; }
        public Guid Id { get; set; }

        #endregion

    }
}