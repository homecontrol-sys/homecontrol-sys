using System;
using HomeControl.Domain._Common.DataShape;
using HomeControl.Domain;

namespace HomeControl.API.REST.Device
{

    public interface IReadDeviceRequest : IResourceParams<DeviceFilter, DeviceSearch>
    {
    
    }

}