using System;

namespace HomeControl.API.REST.Device
{
    public class DeleteDeviceRequest : IDeleteDeviceRequest
    {
        
        #region Properties
        
        public string Name { get; set; }
        public Guid Id { get; set; }

        #endregion

    }
}