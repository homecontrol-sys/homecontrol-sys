using System;
using System.ComponentModel.DataAnnotations;

namespace HomeControl.API.REST.Device
{
    public interface ICreateDeviceRequest
    {

        #region Properties

        [Required]        
        string Name { get; set; }
        [Required]        
        Guid SystemId { get; set; }

        #endregion

    }
}