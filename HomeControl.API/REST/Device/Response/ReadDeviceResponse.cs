using System;

namespace HomeControl.API.REST.Device
{

    public class ReadDeviceResponse : IReadDeviceResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }
        
        public Guid SystemId { get; set; }

        #endregion

    }

}