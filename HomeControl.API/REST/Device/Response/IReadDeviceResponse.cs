using System;

namespace HomeControl.API.REST.Device
{

    public interface IReadDeviceResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }
        Guid SystemId { get; set; }

        #endregion

    }

}