using System;

namespace HomeControl.API.REST.Device
{

    public interface IUpdateDeviceResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}