using System;

namespace HomeControl.API.REST.Device
{

    public interface ICreateDeviceResponse
    {

        #region Properties

        Guid Id { get; set; }
        string Name { get; set; }

        #endregion

    }

}