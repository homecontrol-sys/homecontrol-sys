using System;

namespace HomeControl.API.REST.Device
{

    public class UpdateDeviceResponse : IUpdateDeviceResponse
    {

        #region Properties

        public Guid Id { get; set; }
        public string Name { get; set; }

        #endregion

    }

}