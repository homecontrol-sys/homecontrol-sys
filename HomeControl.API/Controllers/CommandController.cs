using System.Threading.Tasks;
using AutoMapper;
using HomeControl.API.REST.Command;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/command")]
    public class CommandController : ControllerBase
    {
        #region Fields

        private readonly ICommandService _commandService;
        private readonly ILogger<CommandController> _logger;
        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        public CommandController(ICommandService commandService, ILogger<CommandController> logger, IMapper mapper)
        {
            _commandService = commandService;
            _logger = logger;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        #region POST

        [HttpPost]
        public ActionResult<string> Execute(string command)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            _commandService.Execute(command);
            
            var response = $"'{command}' command executed!";

            return Ok(response);
        }

        [HttpPost("voice", Name=nameof(ExecuteVoiceCommand))]
        public ActionResult<string> ExecuteVoiceCommand(string command)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            _commandService.Execute(command);
            
            var response = $"'{command}' command executed!";

            return Ok(response);
        }

        // [HttpPost("trigger-switch", Name=nameof(ExecuteSwitchTrigger))]
        // public ActionResult<string> ExecuteSwitchTrigger(SwitchStateRequest request)
        // { 
        //     if (!ModelState.IsValid) return BadRequest(ModelState);

        //     _commandService.ExecuteSwitchTrigger(request.ElementId, request.NewElementSwitchStateId);
            
        //     return Ok();
        // }

        #endregion

        #endregion
    }
}