using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.API.REST.Device;
using HomeControl.Domain;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/device")]
    public class DeviceController : ControllerBase
    {

        #region Fields

        private readonly ILogger<DeviceController> _logger;
        private readonly IMapper _mapper;
        private readonly IDeviceService _deviceService;

        #endregion

        #region Constructor

        public DeviceController(ILogger<DeviceController> _logger, IMapper _mapper, IDeviceService _deviceService)
        {
            this._logger = _logger;
            this._mapper = _mapper;
            this._deviceService = _deviceService;
        }

        #endregion

        #region Methods

        #region Device CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreateDeviceResponse>> CreateDevice(CreateDeviceRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<DeviceDomain>(request);          
            var resourceId = await _deviceService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("Device creation failure");
                
            return CreatedAtRoute(routeName: nameof(GetDevice), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadDeviceResponse>>> GetDevices([FromQuery] ReadDeviceRequest request = null)
        {
            IEnumerable<DeviceDomain> domain;
            if(request == null) domain = await _deviceService.Read();
            else {
                domain = await _deviceService.Find(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadDeviceResponse>>(domain);

            return Ok(response);
        }
        
        [HttpGet("{id:guid}", Name=nameof(GetDevice))]
        public async Task<ActionResult<ReadDeviceResponse>> GetDevice(Guid id)
        {
            var domain = await _deviceService.Read(id);

            if (domain == null) return NotFound("Device not found.");

            var response = _mapper.Map<ReadDeviceResponse>(domain);

            return Ok(response);
        }

        #endregion

        #region Update

        // [HttpPatch("{id:guid}")]
        // public async Task<ActionResult<ReadDeviceResponse>> UpdateDevice(Guid id, [FromBody] JsonPatchDocument<DeviceDomain> deviceUpdatesPatch)
        // {
        //     if (!ModelState.IsValid) return BadRequest(ModelState);
        //     else
        //     {
        //         try
        //         {
        //             var domain = await _deviceService.Read(id);

        //             if (domain == null) return NotFound("Device not found.");

        //             deviceUpdatesPatch.ApplyTo(domain);
        //             var updatedDomain = await _deviceService.Update(domain);
        //             var response = _mapper.Map<ReadDeviceResponse>(updatedDomain);

        //             return Ok(response);
        //         }
        //         catch (Exception ex) { return BadRequest(ex); }
        //     }
        // }

        #endregion

        #region Delete

        // [HttpDelete("{id:guid}")]
        // public async Task<ActionResult<ReadDeviceResponse>> DeleteDevice(Guid id)
        // {
        //     var domain = await _deviceService.Delete(id);

        //     if (domain) return NotFound("Device not found.");

        //     return Ok();
        // }

        #endregion

        #endregion

        #endregion

    }

}