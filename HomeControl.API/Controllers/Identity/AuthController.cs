// using System.Threading.Tasks;
// using AutoMapper;
// using Microsoft.AspNetCore.Mvc;
// using Microsoft.Extensions.Logging;
// using Microsoft.AspNetCore.Authentication;
// using Microsoft.AspNetCore.Authentication.Cookies;
// using HomeControl.API.REST.Security;
// using API.Project.Common.Modules.Security._Extension;
// using HomeControl.Service.Identity;
// using HomeControl.Domain.Identity;

// namespace HomeControl.API.Controllers
// {
//     [ApiController]
//     [Route("/identity")]
//     public class AuthController : ControllerBase
//     {
//         #region Fields

//         private readonly ILogger<AuthController> _logger;
//         private readonly IMapper _mapper;
//         private readonly IIdentityService _identityService;
//         private readonly IJwtSettings _jwtSetings;

//         #endregion

//         #region Constructor

//         public AuthController(ILogger<AuthController> logger, IMapper mapper, 
//         IIdentityService identityService, IJwtSettings jwtSetings)
//         {
//             this._logger = logger;
//             this._mapper = mapper;
//             this._identityService = identityService;
//             this._jwtSetings = jwtSetings;
//         }

//         #endregion

//         #region Methods

//             #region User Login

//             [HttpPost]
//             [Route("/login")]
//             public async Task<ActionResult> LoginUser([FromBody]LoginRequest credentials)
//             {
//                 if (!ModelState.IsValid || credentials == null)
//                 {
//                     return new BadRequestObjectResult(new { Message = "Login failed." });
//                 }

//                 var authModel = _mapper.Map<AuthModel>(credentials);
//                 bool isLogingInWithEmail = credentials.UserNameOrEMail.IsValidEmail();
//                 var user = await _identityService.Login(authModel);           

//                 ActionResult result = user == null
//                     ? NotFound(new { Message = $"{(isLogingInWithEmail ? "Email" : "UserName")} or password is incorrect."})
//                     : Ok(new { Message = "You are logged in", User =  user});

//                 return result;
//             }

//             [HttpPost]
//             [Route("/logout")]
//             public async Task<IActionResult> Logout()
//             {
//                 await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
//                 return Ok(new { Message = "You are logged out" });
//             }

//             #endregion

//             #region User Registration

//             // [HttpPost]
//             // [Route("/register")]
//             // public async Task<ActionResult> Register([FromBody]LoginUserRequest request)
//             // { 
//             //     if (!ModelState.IsValid) return BadRequest(ModelState);

//             //     var identityUser = new IdentityUser() { UserName = request.UserNameOrEMail, Email = request.Password };
//             //     var result = await _userManager.CreateAsync(identityUser, request.Password);
//             //     if (!result.Succeeded)
//             //     {
//             //         var dictionary = new ModelStateDictionary();
//             //         foreach (IdentityError error in result.Errors)
//             //         {
//             //             dictionary.AddModelError(error.Code, error.Description);
//             //         }

//             //         return new BadRequestObjectResult(new { Message = "User Registration Failed", Errors = dictionary });
//             //     }

//             //     return Ok(new { Message = "User Reigstration Successful" });
//             // }

//             #endregion
              
//         #endregion
//     }
// }