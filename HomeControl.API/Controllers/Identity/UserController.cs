using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.API.REST.Identity.User;
using HomeControl.Service;
using HomeControl.Service.Identity;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/user")]
    public class UserController : ControllerBase
    {
        #region Fields

        private readonly ILogger<UserController> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public UserController(ILogger<UserController> logger, IMapper mapper, IUserService userService)
        {
            _logger = logger;
            _mapper = mapper;
            _userService = userService;
        }

        #endregion

        #region Methods
        
            #region User CRUD

                #region Read

                // [HttpGet("{id:guid}")]
                // public async Task<ActionResult<ReadUserResponse>> GetUser(Guid id)
                // {
                //     var domain = await _userService.ReadUser(id);

                //     if (domain == null) return NotFound("User not found.");

                //     var response = _mapper.Map<ReadUserResponse>(domain);

                //     return Ok(response);
                // }

                // [HttpGet]
                // public async Task<ActionResult<IEnumerable<ReadUserResponse>>> GetUsers()
                // {
                //     var domain = await _userService.ReadUsers();
                //     var response = _mapper.Map<IEnumerable<ReadUserResponse>>(domain);

                //     return Ok(response);
                // }

                #endregion

                #region Create

                #endregion

                #region Update

                // [HttpPut("{id:guid}")]
                // public async Task<ActionResult<ReadUserResponse>> UpdateUser(Guid id, [FromBody] UpdateUserRequest userUpdates)
                // {
                //     if (!ModelState.IsValid) return BadRequest(ModelState);
                //     else
                //     {
                //         try
                //         {
                //             var domain = await _userService.ReadUser(id);

                //             if (domain == null) return NotFound("User not found.");

                //             var updateRequest = _mapper.Map<UpdateUserRequest>(domain);
                            
                //             var updatedDomain = await _userService.UpdateUser(domain);
                //             var response = _mapper.Map<ReadUserResponse>(updatedDomain);

                //             return Ok(response);
                //         }
                //         catch (Exception ex) { return BadRequest(ex); }
                //     }
                // }

                #endregion 

                // #region Delete

                // [HttpDelete("{id:guid}")]
                // public async Task<ActionResult<ReadUserResponse>> DeleteUser(Guid id)
                // {
                //     var result = await _userService.DeleteUser(id);

                //     if (!result) return NotFound("User not found.");

                //     return Ok(result);
                // }

                // #endregion

            #endregion

        #endregion
    }
}