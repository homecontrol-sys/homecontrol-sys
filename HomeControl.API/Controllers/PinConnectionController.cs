using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HomeControl.API.REST.PinConnection;
using HomeControl.Domain;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/pin-connection")]
    public class PinConnectionController : ControllerBase
    {

        #region Fields

        private readonly ILogger<PinConnectionController> _logger;
        private readonly IMapper _mapper;
        private readonly IPinConnectionService _pinConnectionService;

        #endregion

        #region Constructor

        public PinConnectionController(ILogger<PinConnectionController> logger, IMapper mapper,
        IPinConnectionService pinConnectionService)
        {
            _logger = logger;
            _mapper = mapper;
            _pinConnectionService = pinConnectionService;
        }

        #endregion

        #region Methods

        #region Metadata

        [HttpOptions]
        public ActionResult GetPinConnectionAllowedOptions()
        {
            Response.Headers.Add("Allow", "GET,POST,PUT,OPTIONS");
            
            return Ok();
        }

        #endregion

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreatePinConnectionResponse>> CreatePinConnection(CreatePinConnectionRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<PinConnectionDomain>(request);
            var resourceId = await _pinConnectionService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("PinConnection creation failure");
                
            return CreatedAtRoute(routeName: nameof(GetPinConnection), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadPinConnectionResponse>>> GetPinConnections([FromQuery] ReadPinConnectionRequest request = null)
        {
            IEnumerable<PinConnectionDomain> domain;
            if(request == null) domain = await _pinConnectionService.Read();
            else {
                domain = await _pinConnectionService.Find(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadPinConnectionResponse>>(domain);

            return Ok(response);
        }


        [HttpGet("{id:guid}", Name=nameof(GetPinConnection))]
        public async Task<ActionResult<ReadPinConnectionResponse>> GetPinConnection(Guid id)
        {
            var domain = await _pinConnectionService.Read(id);

            if (domain == null) return NotFound("PinConnection not found.");

            var response = _mapper.Map<ReadPinConnectionResponse>(domain);

            return Ok(response);
        }


        #endregion

                #region Update

                // //FULL UPDATE PUT
                // [HttpPut("pinConnectionCategoryId:guid")]
                // public async Task<IActionResult> UpdatePinConnectionCategory(CreatePinConnectionRequest request)
                // { 
                //     if (!ModelState.IsValid) return BadRequest(ModelState);

                //     var domain = _mapper.Map<PinConnectionDomain>(request);
                //     //Guid resourceId = await _pinConnectionService.Create(domain);
                //     Guid resourceId = Guid.NewGuid();

                //     if (Guid.Empty == resourceId) return BadRequest("PinConnection creation failure");
                    
                //     var response = _mapper.Map<CreatePinConnectionResponse>(domain);

                //     return CreatedAtRoute(routeName: nameof(GetPinConnection), routeValues: new { id = resourceId }, response);
                // }

                // [HttpPatch("{id:guid}")]
                // public async Task<ActionResult<ReadPinConnectionResponse>> UpdatePinConnection(Guid id, [FromBody] JsonPatchDocument<PinConnectionDomain> pinConnectionUpdatesPatch)
                // {
                //     if (!ModelState.IsValid) return BadRequest(ModelState);
                //     else
                //     {
                //         try
                //         {
                //             var domain = await _pinConnectionService.Read(id);

                //             if (domain == null) return NotFound("PinConnection not found.");

                //             pinConnectionUpdatesPatch.ApplyTo(domain);
                //             var updatedDomain = await _pinConnectionService.Update(domain);
                //             var response = _mapper.Map<ReadPinConnectionResponse>(updatedDomain);

                //             return Ok(response);
                //         }
                //         catch (Exception ex) { return BadRequest(ex); }
                //     }
                // }

                #endregion

                #region Delete

                // [HttpDelete("{id:guid}")]
                // public async Task<ActionResult<ReadPinConnectionResponse>> DeletePinConnection(Guid id)
                // {
                //     var domain = await _pinConnectionService.Delete(id);

                //     if (domain) return NotFound("Delete not found.");

                //     return Ok();
                // }

                #endregion

            #endregion

        #endregion

    }
}
