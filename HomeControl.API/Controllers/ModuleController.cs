using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HomeControl.API.REST.Module;
using HomeControl.Domain;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/module")]
    public class ModuleController : ControllerBase
    {

        #region Fields

        private readonly ILogger<ModuleController> _logger;
        private readonly IMapper _mapper;
        private readonly IModuleService _moduleService;

        #endregion

        #region Constructor

        public ModuleController(ILogger<ModuleController> logger, IMapper mapper,
        IModuleService moduleService)
        {
            _logger = logger;
            _mapper = mapper;
            _moduleService = moduleService;
        }

        #endregion

        #region Methods

        #region Metadata

        [HttpOptions]
        public ActionResult GetModuleAllowedOptions()
        {
            Response.Headers.Add("Allow", "GET,POST,PUT,OPTIONS");
            
            return Ok();
        }

        #endregion

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreateModuleResponse>> CreateModule(CreateModuleRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<ModuleDomain>(request);
            var resourceId = await _moduleService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("Module creation failure");
                
            return CreatedAtRoute(routeName: nameof(GetModule), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadModuleResponse>>> GetModules([FromQuery] ReadModuleRequest request = null)
        {
            IEnumerable<ModuleDomain> domain;
            if(request == null) domain = await _moduleService.Read();
            else {
                domain = await _moduleService.Find(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadModuleResponse>>(domain);

            return Ok(response);
        }


        [HttpGet("{id:guid}", Name=nameof(GetModule))]
        public async Task<ActionResult<ReadModuleResponse>> GetModule(Guid id)
        {
            var domain = await _moduleService.Read(id);

            if (domain == null) return NotFound("Module not found.");

            var response = _mapper.Map<ReadModuleResponse>(domain);

            return Ok(response);
        }


        #endregion

                #region Update

                // //FULL UPDATE PUT
                // [HttpPut("moduleCategoryId:guid")]
                // public async Task<IActionResult> UpdateModuleCategory(CreateModuleRequest request)
                // { 
                //     if (!ModelState.IsValid) return BadRequest(ModelState);

                //     var domain = _mapper.Map<ModuleDomain>(request);
                //     //Guid resourceId = await _moduleService.Create(domain);
                //     Guid resourceId = Guid.NewGuid();

                //     if (Guid.Empty == resourceId) return BadRequest("Module creation failure");
                    
                //     var response = _mapper.Map<CreateModuleResponse>(domain);

                //     return CreatedAtRoute(routeName: nameof(GetModule), routeValues: new { id = resourceId }, response);
                // }

                // [HttpPatch("{id:guid}")]
                // public async Task<ActionResult<ReadModuleResponse>> UpdateModule(Guid id, [FromBody] JsonPatchDocument<ModuleDomain> moduleUpdatesPatch)
                // {
                //     if (!ModelState.IsValid) return BadRequest(ModelState);
                //     else
                //     {
                //         try
                //         {
                //             var domain = await _moduleService.Read(id);

                //             if (domain == null) return NotFound("Module not found.");

                //             moduleUpdatesPatch.ApplyTo(domain);
                //             var updatedDomain = await _moduleService.Update(domain);
                //             var response = _mapper.Map<ReadModuleResponse>(updatedDomain);

                //             return Ok(response);
                //         }
                //         catch (Exception ex) { return BadRequest(ex); }
                //     }
                // }

                #endregion

                #region Delete

                // [HttpDelete("{id:guid}")]
                // public async Task<ActionResult<ReadModuleResponse>> DeleteModule(Guid id)
                // {
                //     var domain = await _moduleService.Delete(id);

                //     if (domain) return NotFound("Delete not found.");

                //     return Ok();
                // }

                #endregion

            #endregion

        #endregion

    }
}
