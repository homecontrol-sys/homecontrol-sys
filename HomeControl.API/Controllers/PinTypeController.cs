using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HomeControl.API.REST.PinType;
using HomeControl.Domain;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/pin-type")]
    public class PinTypeController : ControllerBase
    {

        #region Fields

        private readonly ILogger<PinTypeController> _logger;
        private readonly IMapper _mapper;
        private readonly IPinTypeService _pinTypeService;

        #endregion

        #region Constructor

        public PinTypeController(ILogger<PinTypeController> logger, IMapper mapper,
        IPinTypeService pinTypeService)
        {
            _logger = logger;
            _mapper = mapper;
            _pinTypeService = pinTypeService;
        }

        #endregion

        #region Methods

        #region Metadata

        [HttpOptions]
        public ActionResult GetPinTypeAllowedOptions()
        {
            Response.Headers.Add("Allow", "GET,POST,PUT,OPTIONS");
            
            return Ok();
        }

        #endregion

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreatePinTypeResponse>> CreatePinType(CreatePinTypeRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<PinTypeDomain>(request);
            var resourceId = await _pinTypeService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("PinType creation failure");
                
            return CreatedAtRoute(routeName: nameof(GetPinType), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadPinTypeResponse>>> GetPinTypes([FromQuery] ReadPinTypeRequest request = null)
        {
            IEnumerable<PinTypeDomain> domain;
            if(request == null) domain = await _pinTypeService.Read();
            else {
                domain = await _pinTypeService.Find(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadPinTypeResponse>>(domain);

            return Ok(response);
        }


        [HttpGet("{id:guid}", Name=nameof(GetPinType))]
        public async Task<ActionResult<ReadPinTypeResponse>> GetPinType(Guid id)
        {
            var domain = await _pinTypeService.Read(id);

            if (domain == null) return NotFound("PinType not found.");

            var response = _mapper.Map<ReadPinTypeResponse>(domain);

            return Ok(response);
        }


        #endregion

                #region Update

                // //FULL UPDATE PUT
                // [HttpPut("pinTypeCategoryId:guid")]
                // public async Task<IActionResult> UpdatePinTypeCategory(CreatePinTypeRequest request)
                // { 
                //     if (!ModelState.IsValid) return BadRequest(ModelState);

                //     var domain = _mapper.Map<PinTypeDomain>(request);
                //     //Guid resourceId = await _pinTypeService.Create(domain);
                //     Guid resourceId = Guid.NewGuid();

                //     if (Guid.Empty == resourceId) return BadRequest("PinType creation failure");
                    
                //     var response = _mapper.Map<CreatePinTypeResponse>(domain);

                //     return CreatedAtRoute(routeName: nameof(GetPinType), routeValues: new { id = resourceId }, response);
                // }

                // [HttpPatch("{id:guid}")]
                // public async Task<ActionResult<ReadPinTypeResponse>> UpdatePinType(Guid id, [FromBody] JsonPatchDocument<PinTypeDomain> pinTypeUpdatesPatch)
                // {
                //     if (!ModelState.IsValid) return BadRequest(ModelState);
                //     else
                //     {
                //         try
                //         {
                //             var domain = await _pinTypeService.Read(id);

                //             if (domain == null) return NotFound("PinType not found.");

                //             pinTypeUpdatesPatch.ApplyTo(domain);
                //             var updatedDomain = await _pinTypeService.Update(domain);
                //             var response = _mapper.Map<ReadPinTypeResponse>(updatedDomain);

                //             return Ok(response);
                //         }
                //         catch (Exception ex) { return BadRequest(ex); }
                //     }
                // }

                #endregion

                #region Delete

                // [HttpDelete("{id:guid}")]
                // public async Task<ActionResult<ReadPinTypeResponse>> DeletePinType(Guid id)
                // {
                //     var domain = await _pinTypeService.Delete(id);

                //     if (domain) return NotFound("Delete not found.");

                //     return Ok();
                // }

                #endregion

            #endregion

        #endregion

    }
}
