using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.API.REST.NetworkConfiguration;
using HomeControl.Domain;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/network-configuration")]
    public class NetworkConfigurationController : ControllerBase
    {
        
        #region Fields

        private readonly ILogger<NetworkConfigurationController> _logger;
        private readonly IMapper _mapper;
        private readonly INetworkConfigurationService _networkConfigurationService;

        #endregion

        #region Constructor

        public NetworkConfigurationController(ILogger<NetworkConfigurationController> logger, IMapper mapper, INetworkConfigurationService networkConfigurationService)
        {
            _networkConfigurationService = networkConfigurationService;
            _logger = logger;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreateNetworkConfigurationResponse>> CreateNetworkConfiguration(CreateNetworkConfigurationRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<NetworkConfigurationDomain>(request);
            var resourceId = await _networkConfigurationService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("NetworkConfiguration creation failure");
                
            return CreatedAtRoute(routeName: nameof(GetNetworkConfiguration), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadNetworkConfigurationResponse>>> GetNetworkConfigurations([FromQuery] ReadNetworkConfigurationRequest request = null)
        {
            IEnumerable<NetworkConfigurationDomain> domain;
            if(request == null) domain = await _networkConfigurationService.Read();
            else {
                domain = await _networkConfigurationService.Find(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadNetworkConfigurationResponse>>(domain);

            return Ok(response);
        }

        [HttpGet("{id:guid}", Name=nameof(GetNetworkConfiguration))]
        public async Task<ActionResult<ReadNetworkConfigurationResponse>> GetNetworkConfiguration(Guid id)
        {
            var domain = await _networkConfigurationService.Read(id);

            if (domain == null) return NotFound("NetworkConfiguration not found.");

            var response = _mapper.Map<ReadNetworkConfigurationResponse>(domain);

            return Ok(response);
        }

        #endregion

            #region Update

            // [HttpPatch("{id:guid}")]
            // public async Task<ActionResult<ReadNetworkConfigurationResponse>> UpdateNetworkConfiguration(Guid id, [FromBody] JsonPatchDocument<NetworkConfigurationDomain> networkConfigurationUpdatesPatch)
            // {
            //     if (!ModelState.IsValid) return BadRequest(ModelState);
            //     else
            //     {
            //         try
            //         {
            //             var domain = await _networkConfigurationService.Read(id);

            //             if (domain == null) return NotFound("Device not found.");

            //             networkConfigurationUpdatesPatch.ApplyTo(domain);
            //             var updatedDomain = await _networkConfigurationService.Update(domain);
            //             var response = _mapper.Map<ReadNetworkConfigurationResponse>(updatedDomain);

            //             return Ok(response);
            //         }
            //         catch (Exception ex) { return BadRequest(ex); }
            //     }
            // }

            #endregion

            #region Delete

            // [HttpDelete("{id:guid}")]
            // public async Task<ActionResult<ReadNetworkConfigurationResponse>> DeleteNetworkConfiguration(Guid id)
            // {
            //     var domain = await _networkConfigurationService.Delete(id);

            //     if (domain) return NotFound("NetworkConfiguration not found.");

            //     return Ok();
            // }

            #endregion

        #endregion

        #endregion

    }

}
