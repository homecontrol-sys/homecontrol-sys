using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HomeControl.API.REST.System;
using HomeControl.Domain;
using System.Linq;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/system")]
    public class SystemController : ControllerBase
    {

        #region Fields

        private readonly ILogger<SystemController> _logger;
        private readonly IMapper _mapper;
        private readonly ISystemService _systemService;

        #endregion

        #region Constructor

        public SystemController(ILogger<SystemController> logger, IMapper mapper,
        ISystemService systemService)
        {
            _logger = logger;
            _mapper = mapper;
            _systemService = systemService;
        }

        #endregion

        #region Methods

        #region Endpoint METADATA

        [HttpOptions]
        public ActionResult GetSystemAllowedOptions()
        {
            Response.Headers.Add("Allow", "GET,POST,PUT,OPTIONS");
            
            return Ok();
        }

        #endregion

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreateSystemResponse>> CreateSystem(CreateSystemRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<SystemDomain>(request);
            var resourceId = await _systemService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("System creation failure");

            return CreatedAtRoute(routeName: nameof(GetSystem), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadSystemResponse>>> GetSystems([FromQuery] ReadSystemRequest request = null)
        {
            IEnumerable<SystemDomain> domain;
            if(request == null) domain = await _systemService.Read();
            else {
                domain = await _systemService.FindDomains(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadSystemResponse>>(domain);

            return Ok(response);
        }

        [HttpGet("{id:guid}", Name=nameof(GetSystem))]
        public async Task<ActionResult<ReadSystemResponse>> GetSystem(Guid id)
        {
            var domain = await _systemService.Read(id);

            if (domain == null) return NotFound("System not found.");

            var response = _mapper.Map<ReadSystemResponse>(domain);

            return Ok(response);
        }

        #endregion

                #region Update

                // //FULL UPDATE PUT
                // [HttpPut("systemCategoryId:guid")]
                // public async Task<IActionResult> UpdateSystemCategory(CreateSystemRequest request)
                // { 
                //     if (!ModelState.IsValid) return BadRequest(ModelState);

                //     var domain = _mapper.Map<SystemDomain>(request);
                //     //Guid resourceId = await _systemService.Create(domain);
                //     Guid resourceId = Guid.NewGuid();

                //     if (Guid.Empty == resourceId) return BadRequest("System creation failure");
                    
                //     var response = _mapper.Map<CreateSystemResponse>(domain);

                //     return CreatedAtRoute(routeName: nameof(GetSystem), routeValues: new { id = resourceId }, response);
                // }

                // [HttpPatch("{id:guid}")]
                // public async Task<ActionResult<ReadSystemResponse>> UpdateSystem(Guid id, [FromBody] JsonPatchDocument<SystemDomain> systemUpdatesPatch)
                // {
                //     if (!ModelState.IsValid) return BadRequest(ModelState);
                //     else
                //     {
                //         try
                //         {
                //             var domain = await _systemService.Read(id);

                //             if (domain == null) return NotFound("System not found.");

                //             systemUpdatesPatch.ApplyTo(domain);
                //             var updatedDomain = await _systemService.Update(domain);
                //             var response = _mapper.Map<ReadSystemResponse>(updatedDomain);

                //             return Ok(response);
                //         }
                //         catch (Exception ex) { return BadRequest(ex); }
                //     }
                // }

                #endregion

                #region Delete

                // [HttpDelete("{id:guid}")]
                // public async Task<ActionResult<ReadSystemResponse>> DeleteSystem(Guid id)
                // {
                //     var domain = await _systemService.Delete(id);

                //     if (domain) return NotFound("Delete not found.");

                //     return Ok();
                // }

                #endregion

            #endregion

        #region STATE

        [HttpGet("state", Name=nameof(GetSystemState))]
        public async Task<ActionResult<IEnumerable<ReadSystemStateResponse>>> GetSystemState()
        {   
            string[] prefetch = new[] {"Elements", "Switch", "SwitchState", "SwitchStates"};
            var domains = await _systemService.FindDomains(prefetch);
            var response = _mapper.Map<IEnumerable<ReadSystemStateResponse>>(domains);

            return Ok(response);
        }

        #endregion

        #region CONFIGURATION

        [HttpGet("is-main-configured", Name=nameof(CheckIsMainSystemConfigured))]
        public async Task<ActionResult<bool>> CheckIsMainSystemConfigured()
        {
            var filter = new SystemFilter { IsMain = true };
            var response = await _systemService.IsMainSystemConfigured();

            return Ok(response);
        }

        [HttpPost("configure-main", Name=nameof(ConfigureMainSystem))]
        public async Task<ActionResult<CreateSystemResponse>> ConfigureMainSystem(MainSystemConfiguration request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            bool isConfigured = await _systemService.ConfigureMainSystem(request);

            if (isConfigured) return NotFound("System not found.");
            
            return Ok();
        }
       
        #endregion

        #endregion

    }
}
