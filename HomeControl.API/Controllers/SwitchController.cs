using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.API.REST.Switch;
using HomeControl.Domain;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/switch")]
    public class SwitchController : ControllerBase
    {
        
        #region Fields

        private readonly ILogger<SwitchController> _logger;
        private readonly IMapper _mapper;
        private readonly ISwitchService _switchService;

        #endregion

        #region Constructor

        public SwitchController(ILogger<SwitchController> logger, 
            IMapper mapper, ISwitchService switchService)
        {
            _logger = logger;
            _mapper = mapper;
            _switchService = switchService;
        }

        #endregion

        #region Methods

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreateSwitchResponse>> CreateSwitch(CreateSwitchRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<SwitchDomain>(request);
            var resourceId = await _switchService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("Element State Switch creation failure");

            return CreatedAtRoute(routeName: nameof(GetSwitch), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadSwitchResponse>>> GetSwitchess([FromQuery] ReadSwitchRequest request = null)
        {
            IEnumerable<SwitchDomain> domain;
            if(request == null) domain = await _switchService.Read();
            else {
                domain = await _switchService.Find(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadSwitchResponse>>(domain);

            return Ok(response);
        }

        [HttpGet("{id:guid}", Name=nameof(GetSwitch))]
        public async Task<ActionResult<ReadSwitchResponse>> GetSwitch(Guid id)
        {
            var domain = await _switchService.Read(id);

            if (domain == null) return NotFound("Element State Switch not found.");

            var response = _mapper.Map<ReadSwitchResponse>(domain);

            return Ok(response);
        }

        #endregion

        #region Update

        // [HttpPatch("{id:guid}")]
        // public async Task<ActionResult<ReadSwitchResponse>> UpdateSwitch(Guid id, [FromBody] JsonPatchDocument<SwitchDomain> switchUpdatesPatch)
        // {
        //     if (!ModelState.IsValid) return BadRequest(ModelState);
        //     else
        //     {
        //         try
        //         {
        //             var domain = await _switchService.Read(id);

        //             if (domain == null) return NotFound("Device not found.");

        //             switchUpdatesPatch.ApplyTo(domain);
        //             var updatedDomain = await _switchService.Update(domain);
        //             var response = _mapper.Map<ReadSwitchResponse>(updatedDomain);

        //             return Ok(response);
        //         }
        //         catch (Exception ex) { return BadRequest(ex); }
        //     }
        // }

        #endregion

        #region Delete

        // [HttpDelete("{id:guid}")]
        // public async Task<ActionResult<ReadSwitchResponse>> DeleteSwitch(Guid id)
        // {
        //     var domain = await _switchService.Delete(id);

        //     if (domain) return NotFound("Switch not found.");

        //     return Ok();
        // }

        #endregion

        #endregion

        #endregion

    }

}
