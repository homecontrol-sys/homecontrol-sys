using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.API.REST.Element;
using HomeControl.Domain;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/element")]
    public class ElementController : ControllerBase
    {
        
        #region Fields

        private readonly ILogger<ElementController> _logger;
        private readonly IMapper _mapper;
        private readonly IElementService _elementService;

        #endregion

        #region Constructor

        public ElementController(ILogger<ElementController> logger, IMapper mapper, IElementService elementService)
        {
            _elementService = elementService;
            _logger = logger;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreateElementResponse>> CreateElement(CreateElementRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<ElementDomain>(request);
            var resourceId = await _elementService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("Element creation failure");
                
            return CreatedAtRoute(routeName: nameof(GetElement), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadElementResponse>>> GetElements([FromQuery] ReadElementRequest request = null)
        {
            IEnumerable<ElementDomain> domain;
            // if(request == null) domain = await _elementService.Read();
            // else {
            //     domain = await _elementService.Find(request.Prefetch, request.Filter, request.SearchBy);
            // }
            string[] prefetch = new[] {"Switch"};
            domain = await _elementService.Find(prefetch, request.Filter, request.SearchBy);

            var response = _mapper.Map<IEnumerable<ReadElementResponse>>(domain);

            return Ok(response);
        }

        [HttpGet("{id:guid}", Name=nameof(GetElement))]
        public async Task<ActionResult<ReadElementResponse>> GetElement(Guid id)
        {
            var domain = await _elementService.Read(id);

            if (domain == null) return NotFound("Element not found.");

            var response = _mapper.Map<ReadElementResponse>(domain);

            return Ok(response);
        }

        #endregion

            #region Update

            // [HttpPatch("{id:guid}")]
            // public async Task<ActionResult<ReadElementResponse>> UpdateElement(Guid id, [FromBody] JsonPatchDocument<ElementDomain> elementUpdatesPatch)
            // {
            //     if (!ModelState.IsValid) return BadRequest(ModelState);
            //     else
            //     {
            //         try
            //         {
            //             var domain = await _elementService.Read(id);

            //             if (domain == null) return NotFound("Device not found.");

            //             elementUpdatesPatch.ApplyTo(domain);
            //             var updatedDomain = await _elementService.Update(domain);
            //             var response = _mapper.Map<ReadElementResponse>(updatedDomain);

            //             return Ok(response);
            //         }
            //         catch (Exception ex) { return BadRequest(ex); }
            //     }
            // }

            #endregion

            #region Delete

            // [HttpDelete("{id:guid}")]
            // public async Task<ActionResult<ReadElementResponse>> DeleteElement(Guid id)
            // {
            //     var domain = await _elementService.Delete(id);

            //     if (domain) return NotFound("Element not found.");

            //     return Ok();
            // }

            #endregion

        #endregion

        [HttpPost("switch-state", Name=nameof(ExecuteSwitchTrigger))]
        public async Task<ActionResult<string>> ExecuteSwitchTrigger([FromBody]SwitchElementStateRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await _elementService.SwitchElementState(request.ElementId, request.NewElementSwitchStateId);
            
            return Ok();
        }

        #endregion

    }

}
