using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HomeControl.API.REST.Pin;
using HomeControl.Domain;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/pin")]
    public class PinController : ControllerBase
    {

        #region Fields

        private readonly ILogger<PinController> _logger;
        private readonly IMapper _mapper;
        private readonly IPinService _pinService;

        #endregion

        #region Constructor

        public PinController(ILogger<PinController> logger, IMapper mapper,
        IPinService pinService)
        {
            _logger = logger;
            _mapper = mapper;
            _pinService = pinService;
        }

        #endregion

        #region Methods

        #region Metadata

        [HttpOptions]
        public ActionResult GetPinAllowedOptions()
        {
            Response.Headers.Add("Allow", "GET,POST,PUT,OPTIONS");
            
            return Ok();
        }

        #endregion

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreatePinResponse>> CreatePin(CreatePinRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<PinDomain>(request);
            var resourceId = await _pinService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("Pin creation failure");
                
            return CreatedAtRoute(routeName: nameof(GetPin), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadPinResponse>>> GetPins([FromQuery] ReadPinRequest request = null)
        {
            IEnumerable<PinDomain> domain;
            if(request == null) domain = await _pinService.Read();
            else {
                domain = await _pinService.Find(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadPinResponse>>(domain);

            return Ok(response);
        }


        [HttpGet("{id:guid}", Name=nameof(GetPin))]
        public async Task<ActionResult<ReadPinResponse>> GetPin(Guid id)
        {
            var domain = await _pinService.Read(id);

            if (domain == null) return NotFound("Pin not found.");

            var response = _mapper.Map<ReadPinResponse>(domain);

            return Ok(response);
        }


        #endregion

                #region Update

                // //FULL UPDATE PUT
                // [HttpPut("pinCategoryId:guid")]
                // public async Task<IActionResult> UpdatePinCategory(CreatePinRequest request)
                // { 
                //     if (!ModelState.IsValid) return BadRequest(ModelState);

                //     var domain = _mapper.Map<PinDomain>(request);
                //     //Guid resourceId = await _pinService.Create(domain);
                //     Guid resourceId = Guid.NewGuid();

                //     if (Guid.Empty == resourceId) return BadRequest("Pin creation failure");
                    
                //     var response = _mapper.Map<CreatePinResponse>(domain);

                //     return CreatedAtRoute(routeName: nameof(GetPin), routeValues: new { id = resourceId }, response);
                // }

                // [HttpPatch("{id:guid}")]
                // public async Task<ActionResult<ReadPinResponse>> UpdatePin(Guid id, [FromBody] JsonPatchDocument<PinDomain> pinUpdatesPatch)
                // {
                //     if (!ModelState.IsValid) return BadRequest(ModelState);
                //     else
                //     {
                //         try
                //         {
                //             var domain = await _pinService.Read(id);

                //             if (domain == null) return NotFound("Pin not found.");

                //             pinUpdatesPatch.ApplyTo(domain);
                //             var updatedDomain = await _pinService.Update(domain);
                //             var response = _mapper.Map<ReadPinResponse>(updatedDomain);

                //             return Ok(response);
                //         }
                //         catch (Exception ex) { return BadRequest(ex); }
                //     }
                // }

                #endregion

                #region Delete

                // [HttpDelete("{id:guid}")]
                // public async Task<ActionResult<ReadPinResponse>> DeletePin(Guid id)
                // {
                //     var domain = await _pinService.Delete(id);

                //     if (domain) return NotFound("Delete not found.");

                //     return Ok();
                // }

                #endregion

            #endregion

        #endregion

    }
}
