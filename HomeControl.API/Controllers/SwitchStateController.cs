using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HomeControl.API.REST.SwitchState;
using HomeControl.Domain;
using HomeControl.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeControl.API.Controllers
{
    [ApiController]
    [Route("/switch-state")]
    public class SwitchStateController : ControllerBase
    {
        
        #region Fields

        private readonly ILogger<SwitchStateController> _logger;
        private readonly IMapper _mapper;
        private readonly ISwitchStateService _switchStateService;

        #endregion

        #region Constructor

        public SwitchStateController(ILogger<SwitchStateController> logger, IMapper mapper, ISwitchStateService switchStateService)
        {
            _switchStateService = switchStateService;
            _logger = logger;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        #region CRUD

        #region Create

        [HttpPost]
        public async Task<ActionResult<CreateSwitchStateResponse>> CreateSwitchState(CreateSwitchStateRequest request)
        { 
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var domain = _mapper.Map<SwitchStateDomain>(request);
            var resourceId = await _switchStateService.Create(domain);

            if (resourceId == Guid.Empty) return BadRequest("Element State creation failure");
                
            return CreatedAtRoute(routeName: nameof(GetSwitchState), routeValues: new { id = resourceId }, resourceId);
        }

        #endregion

        #region Read

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadSwitchStateResponse>>> GetSwitchStates([FromQuery] ReadSwitchStateRequest request = null)
        {
            IEnumerable<SwitchStateDomain> domain;
            if(request == null) domain = await _switchStateService.Read();
            else {
                domain = await _switchStateService.Find(request.Prefetch, request.Filter, request.SearchBy);
            }

            var response = _mapper.Map<IEnumerable<ReadSwitchStateResponse>>(domain);

            return Ok(response);
        }

        [HttpGet("{id:guid}", Name="GET-Element-State")]
        public async Task<ActionResult<ReadSwitchStateResponse>> GetSwitchState(Guid id)
        {
            var domain = await _switchStateService.Read(id);

            if (domain == null) return NotFound("Element State not found.");

            var response = _mapper.Map<ReadSwitchStateResponse>(domain);

            return Ok(response);
        }

        #endregion

        #region Update

        // [HttpPatch("{id:guid}")]
        // public async Task<ActionResult<ReadSwitchStateResponse>> UpdateSwitchState(Guid id, [FromBody] JsonPatchDocument<SwitchStateDomain> switchStateUpdatesPatch)
        // {
        //     if (!ModelState.IsValid) return BadRequest(ModelState);
        //     else
        //     {
        //         try
        //         {
        //             var domain = await _switchStateService.Read(id);

        //             if (domain == null) return NotFound("Device not found.");

        //             switchStateUpdatesPatch.ApplyTo(domain);
        //             var updatedDomain = await _switchStateService.Update(domain);
        //             var response = _mapper.Map<ReadSwitchStateResponse>(updatedDomain);

        //             return Ok(response);
        //         }
        //         catch (Exception ex) { return BadRequest(ex); }
        //     }
        // }

        #endregion

        #region Delete

        // [HttpDelete("{id:guid}")]
        // public async Task<ActionResult<ReadSwitchStateResponse>> DeleteSwitchState(Guid id)
        // {
        //     var domain = await _switchStateService.Delete(id);

        //     if (domain) return NotFound("SwitchState not found.");

        //     return Ok();
        // }

        #endregion

        #endregion

        #endregion

    }

}
